class Company {
    constructor(){
        this.departments = {}; 
    }

    addEmployee(username, salary, position, department){
        let args = [...arguments];
        
        if(args.includes("") || args.includes(undefined) || args.includes(null)){
            throw new Error('Invalid input!');
        }
        else if(salary < 0){
            throw new Error('Invalid input!');
        }

        if(this.departments.hasOwnProperty(department)){
            this.departments[department].push({username, salary, position: position, department});
        }
        else{
            this.departments[department] = [{username, salary, position: position, department}];
        }
        
        return `New employee is hired. Name: ${username}. Position: ${position}`;
    }

    bestDepartment() {
        let bestDepartmentName = '';
        let maxAverageSalary = 0;
        for (const depart in this.departments) {
           
            let averageSalary = this.departments[depart].reduce((acc, x, i) => {
                acc += Number(x.salary);
                return acc;
            },0);
            averageSalary /= this.departments[depart].length;
            if(maxAverageSalary < averageSalary){
                maxAverageSalary = averageSalary;
                bestDepartmentName = depart;
            }
        }
        let departmentEmployees = this.departments[bestDepartmentName].sort((a,b) => {
           let result = b.salary - a.salary
            if((b.salary - a.salary) === 0){
               result = a.username.localeCompare(b.username);
            }
            return result;
        });

        let employeesString = departmentEmployees.map(element => {
            return `${element.username} ${element.salary} ${element.position}`;
        }).join('\n');
        

       let result = `Best Department is: ${bestDepartmentName}` + '\n' + `Average salary: ${maxAverageSalary.toFixed(2)}` + '\n' + employeesString;

        return result;
    }
}

let c = new Company();
c.addEmployee("Stap", 2000, "engineer", "Construction");
c.addEmployee("Pesho", 1500, "electrical engineer", "Construction");
c.addEmployee("Slavi", 500, "dyer", "Construction");
c.addEmployee("Stan", 2000, "architect", "Construction");
c.addEmployee("Stanimir", 1200, "digital marketing manager", "Marketing");
c.addEmployee("Pesho", 1000, "graphical designer", "Marketing");
c.addEmployee("Gosho", 1000, "HR", "Human resources");
console.log(c.bestDepartment());


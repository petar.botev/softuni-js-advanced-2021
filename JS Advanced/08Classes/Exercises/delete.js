class MyClass{
    constructor(one, two){
        this.one = one;
        this.two = two;
    }
    get getOne(){
        return this._one + 1;
    }
    set setOne(value){
        if(value < 50){
            return this._one = value;
        }
    }
}

let obj = new MyClass(2,3);

console.log(obj.one);
console.log(obj._one);
obj._one = 46;
console.log(obj.getOne);

class List {
  constructor() {
    this.myList = [];
    this.size = this.myList.length;
  }

  add(element) {
    this.myList.push(element);
    this.size = this.myList.length;
    return this.myList.sort((a, b) => a - b);
  }
  remove(index) {
    if (index > -1 && index < this.myList.length) {
      this.myList.splice(index, 1);
    }
    this.size = this.myList.length;
    return this.myList.sort((a, b) => a - b);
  }
  get(index) {
    return this.myList[index];
  }
}

let list = new List();
list.add(5);
list.add(7);
list.add(6);
console.log(list.get(1));
list.remove(1);
console.log(list.get(1));

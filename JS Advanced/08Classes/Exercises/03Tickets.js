function sortTickets(arr, str) {
  class Ticket {
    constructor(destination, price, status) {
      this.destination = destination;
      this.price = price;
      this.status = status;
    }
  }
  let ticketsArr = [];
  arr.forEach((el) => {
    let args = el.split('|');
    let ticket = new Ticket(args[0], Number(args[1]), args[2]);
    ticketsArr.push(ticket);
  });

  if (str == 'destination') {
    ticketsArr.sort((a, b) => a.destination.localeCompare(b.destination));
  } else if (str == 'price') {
    ticketsArr.sort((a, b) => a.price - b.price);
  } else if (str == 'status') {
    ticketsArr.sort((a, b) => a.status.localeCompare(b.status));
  }

  return ticketsArr;
}

console.log(
  sortTickets(
    [
      'Philadelphia|94.20|available',
      'New York City|95.99|available',
      'New York City|95.99|sold',
      'Boston|126.20|departed',
    ],
    'price'
  )
);

console.log(
  sortTickets(
    [
      'Philadelphia|94.20|available',
      'New York City|95.99|available',
      'New York City|95.99|sold',
      'Boston|126.20|departed',
    ],
    'status'
  )
);

function validate() {
  let companyInfoElement = document.getElementById('companyInfo');

  document.getElementById('submit').addEventListener('click', function (e) {
    e.preventDefault();

    // username
    let userNameElement = document.getElementById('username');
    let userNameValue = userNameElement.value;
    console.log(userNameValue);

    //E-mail
    let emailElement = document.getElementById('email');
    let emailValue = emailElement.value;
    console.log(emailValue);

    //password
    let passwordElement = document.getElementById('password');
    let passwordValue = passwordElement.value;
    console.log(passwordValue);

    //confirm password
    let confirmPasswordElement = document.getElementById('confirm-password');
    let confirmPasswordValue = confirmPasswordElement.value;
    console.log(confirmPasswordValue);

    let funcResultArr = [];
    //executed functions
    funcResultArr.push(usernameValidator());
    funcResultArr.push(emailValidator());
    funcResultArr.push(passwordValidator());
    funcResultArr.push(confirmPasswordValidator());

    funcResultArr.push(isPasswordMatch());
    if (isPasswordMatch()) {
      redFrame(true, passwordElement);
      redFrame(true, confirmPasswordElement);
    } else {
      redFrame(false, passwordElement);
      redFrame(false, confirmPasswordElement);
    }

    //company
    let companyElement = document.getElementById('company');
    let companyNumberElement = document.getElementById('companyNumber');
    if (companyElement.checked == true) {
      funcResultArr.push(
        validateCompany(companyNumberElement.value, companyNumberElement)
      );
    } else {
      redFrame(true, companyNumberElement);
    }

    trueValidations(funcResultArr);

    //validateCompany(companyNumberElement.value, companyNumberElement);
    //check event

    //declared functions
    function trueValidations(arr) {
      let validElement = document.getElementById('valid');

      if (arr.includes(false) || arr.includes(undefined)) {
        validElement.style.display = 'none';
      } else {
        validElement.style.display = 'block';
      }
    }

    function validateCompany(elValue, element) {
      if (elValue < 1000 || elValue > 9999) {
        redFrame(false, element);
        return false;
      } else {
        redFrame(true, element);
        return true;
      }
    }

    function redFrame(bool, element) {
      if (bool == true) {
        element.style.border = 'none';
      } else {
        element.style.border = 'solid';
        element.style.borderColor = 'red';
      }
    }

    function usernameValidator() {
      let regex = new RegExp('^[a-zA-Z0-9]{3,20}$', 'm');
      //let regex = /[a-zA-Z0-9]{3,20}/;
      let usernameIsValid = regex.test(userNameValue);
      return isValid(usernameIsValid, userNameElement);
    }

    function emailValidator() {
      //let regex = new RegExp('.+@.+\..+', 'm');
      let regex = /.*@.*\..*/;
      let emailIsValid = regex.test(emailValue);
      return isValid(emailIsValid, emailElement);
    }

    function passwordValidator() {
      let regex = new RegExp('^[a-zA-Z0-9_]{5,15}$', 'm');
      //let regex = /[a-zA-Z0-9_]{5,15}/;
      let passwordIsValid = regex.test(passwordValue);
      return isValid(passwordIsValid, passwordElement);
    }

    function confirmPasswordValidator() {
      let regex = new RegExp('^[a-zA-Z0-9_]{5,15}$', 'm');

      // let regex = /[a-zA-Z0-9_]{5,15}/;
      let confirmPasswordIsValid = regex.test(confirmPasswordValue);
      return isValid(confirmPasswordIsValid, confirmPasswordElement);
    }

    function isValid(isValueValid, element) {
      if (isValueValid == true) {
        redFrame(isValueValid, element);
        return true;
      } else {
        redFrame(isValueValid, element);
        return false;
      }
    }

    function isPasswordMatch() {
      let passwordValid = passwordValidator();
      let confirmPasswordValid = confirmPasswordValidator();
      if (
        passwordValue != '' &&
        confirmPasswordValue != '' &&
        passwordValid == true &&
        confirmPasswordValid == true
      ) {
        return passwordValue == confirmPasswordValue ? true : false;
      } else {
        return false;
      }
    }
  });

  document.getElementById('company').addEventListener('change', function (e) {
    if (e.target.checked == true) {
      companyInfoElement.style.display = 'block';
    } else {
      companyInfoElement.style.display = 'none';
    }
  });
}

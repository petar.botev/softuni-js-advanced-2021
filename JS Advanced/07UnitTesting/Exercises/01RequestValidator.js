function solve(obj){
    validateMethod(obj);
    validateUri(obj);
    validateVersion(obj);
    validateMessage(obj);

    return obj;

    function validateMethod(obj){
        let objectMethods = ['GET', 'POST', 'DELETE', 'CONNECT'];

        if(obj['method'] === undefined || !objectMethods.includes(obj['method'])){
            throw new Error(`Invalid request header: Invalid Method`);
        }
    }

    function validateUri(obj){
        let regex = /^\*$|^[a-zA-Z0-9.]+$/;

        if(obj['uri'] === undefined || !regex.test(obj['uri'])){
            throw new Error('Invalid request header: Invalid URI');
        }
    }

    function validateVersion(obj){
        let versions = ['HTTP/0.9', 'HTTP/1.0', 'HTTP/1.1', 'HTTP/2.0' ];

        if(obj['version'] === undefined || !versions.includes(obj['version'])){
            throw new Error('Invalid request header: Invalid Version');
        }
    }

    function validateMessage(obj){
        let regex = /^[^<>\\&'"]*$/;

        if(obj['message'] === undefined || !regex.test(obj['message'])){
            throw new Error('Invalid request header: Invalid Message');
        }
    }
}

// console.log(solve({
//     method: 'GET',
//     uri: 'svn.public.catalog',
//     version: 'HTTP/1.1',
//     message: ''
//   }
//   ));


  console.log(solve({
    method: 'OPTIONS',
    uri: 'git.master',
    version: 'HTTP/1.1',
    message: '-recursive'
  }
  
  ));
const { assert } = require('chai');
const lookupChar = require('../03CharLookup');

describe('lookup char', () => {
    it('first parameter should be string', () => {
        assert.isUndefined(lookupChar(1, 1));
    });
    it('second parameter should be number', () => {
        assert.isUndefined(lookupChar('1', 'a'));
    });
    it('second parameter should not be floating number', () => {
        assert.isUndefined(lookupChar('1', 1.1));
    });
    it('first and second parameter should be correct', () => {
        assert.isUndefined(lookupChar(1, 1.1));
    });

    it('index should be bigger than string length', () => {
        assert.equal(lookupChar('as', 3), 'Incorrect index');
    });

    it('index should be bigger than and not equal to string length', () => {
        assert.equal(lookupChar('as', 2), 'Incorrect index');
    });

    it('index should not be less than zero', () => {
        assert.equal(lookupChar('as', -1), 'Incorrect index');
    });

    it('should return correct index', () => {
        assert.equal(lookupChar('as', 1), 's');
    });
});
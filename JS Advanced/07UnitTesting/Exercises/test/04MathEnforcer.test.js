const { assert } = require('chai');
const mathEnforcer = require('../04MathEnforcer');

describe('mathEnforcer', () => {
    describe('addFive', () => {
        it('should return correct result when number parameter is added', () => {
            assert.equal(mathEnforcer.addFive(1), 6);
            assert.equal(mathEnforcer.addFive(0), 5);
            assert.equal(mathEnforcer.addFive(-1), 4);
            assert.equal(mathEnforcer.addFive(-6), -1);
            assert.closeTo(mathEnforcer.addFive(1.1), 6.1, 0.01);
            assert.closeTo(mathEnforcer.addFive(-1.1), 3.9, 0.01);
            assert.closeTo(mathEnforcer.addFive(-5.1), -0.1, 0.01);
        });

        it('should return undefind when function argument is not a number', () => {
            assert.isUndefined(mathEnforcer.addFive('1'));
            assert.isUndefined(mathEnforcer.addFive(undefined));
            assert.isUndefined(mathEnforcer.addFive(null));
            assert.isUndefined(mathEnforcer.addFive('a'));
            assert.isUndefined(mathEnforcer.addFive(''));
        });
    });

    describe('subtractTen', () => {
        it('should return correct result when number parameter is added', () => {
            assert.equal(mathEnforcer.subtractTen(10), 0);
            assert.equal(mathEnforcer.subtractTen(11), 1);
            assert.equal(mathEnforcer.subtractTen(9), -1);
            assert.equal(mathEnforcer.subtractTen(-1), -11);
            assert.closeTo(mathEnforcer.subtractTen(10.1), 0.1, 0.01);
            assert.closeTo(mathEnforcer.subtractTen(9.1), -0.9, 0.01);
            assert.closeTo(mathEnforcer.subtractTen(-1.1), -11.1, 0.01);
            
        });

        it('should return undefind when function argument is not a string', () => {
            assert.isUndefined(mathEnforcer.subtractTen('1'));
            assert.isUndefined(mathEnforcer.subtractTen(undefined));
            assert.isUndefined(mathEnforcer.subtractTen(null));
            assert.isUndefined(mathEnforcer.subtractTen('a'));
            assert.isUndefined(mathEnforcer.subtractTen(''));
        });
    });

    describe('sum', () => {
        it('should return undefined when first parameter is not a number', () => {
            assert.isUndefined(mathEnforcer.sum('1', 1));
            assert.isUndefined(mathEnforcer.sum(undefined, 1));
            assert.isUndefined(mathEnforcer.sum(null, 1));
            assert.isUndefined(mathEnforcer.sum('', 1));
        });
        it('should return undefined when second parameter is not a number', () => {
            assert.isUndefined(mathEnforcer.sum(1, '1'));
            assert.isUndefined(mathEnforcer.sum(1, undefined));
            //assert.isUndefined(mathEnforcer.sum(null, 1));
            assert.isUndefined(mathEnforcer.sum(1, null));
            assert.isUndefined(mathEnforcer.sum(1, ''));
        });
        it('should return undefined when both parameters are not a number', () => {
            assert.isUndefined(mathEnforcer.sum('1', '1'));
            assert.isUndefined(mathEnforcer.sum(undefined, undefined));
            assert.isUndefined(mathEnforcer.sum(null, null));
            assert.isUndefined(mathEnforcer.sum('', ''));
            assert.isUndefined(mathEnforcer.sum('1', undefined));
        });

        it('should return correct result when number parameters are added', () => {
            assert.equal(mathEnforcer.sum(1, 1), 2);
            assert.equal(mathEnforcer.sum(0, 0), 0);
            assert.equal(mathEnforcer.sum(-1, -1), -2);
            assert.equal(mathEnforcer.sum(1, -2), -1);
            assert.equal(mathEnforcer.sum(-1, 2), 1);

            assert.closeTo(mathEnforcer.sum(1.1, 1.1), 2.2, 0.01);
            assert.closeTo(mathEnforcer.sum(-1.1, 1.1), 0, 0.01);
            assert.closeTo(mathEnforcer.sum(-1.1, -1.1), -2.2, 0.01);
            assert.closeTo(mathEnforcer.sum(-1.1, 2.1), 1, 0.01);

        });
    });
});
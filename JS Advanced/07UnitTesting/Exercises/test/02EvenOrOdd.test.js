const { assert } = require('chai');
const isOddOrEven = require('../02EvenOrOdd');

describe('even or odd', () => {
    it('input should be a string', () => {
        assert.isUndefined(isOddOrEven(1));
    });

    it('output should be even', () => {
        assert.equal(isOddOrEven('as'), 'even');
    });

    it('output should be odd', () => {
        assert.equal(isOddOrEven('a'), 'odd');
    });
});
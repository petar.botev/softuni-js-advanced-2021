function solve(input){
    let faces = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    let suits = {'S':'\u2660', 
    'H':'\u2665', 
    'D':'\u2666', 
    'C':'\u2663'};

    let cards = [];
    let suitsKeys = Object.keys(suits);

    for (let i = 0; i < input.length; i++) {
        let suit = input[i].slice(-1)[0];
        let face = input[i].substring(input[i].length - 1, -1);
  
        if (faces.includes(face) && suitsKeys.includes(suit)) {
            cards.push({'face': face, 'suit': suits[suit], toString() {return `${this.face}${this.suit}`}});
        }
        else{
            throw new Error(`Invalid card: ${suit}${face}`);
        }
        
    }

    let str = '';
    for (let i = 0; i < cards.length; i++) {
       str += cards[i].toString() + ' ';
        
    }
    return str;
}

console.log(solve(['AS', '10D', 'KH', '2C']));
function solve(face, suit){
    let faces = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    let suits = {'S':'\u2660', 
    'H':'\u2665', 
    'D':'\u2666', 
    'C':'\u2663'};

    let resultObj = '';
    let suitsKeys = Object.keys(suits);
  
    if (faces.includes(face) && suitsKeys.includes(suit)) {
        resultObj = {
            'face': face, 
            'suit': suits[suit],
             toString() {
                 return `${this.face}${this.suit}`;
             }
        }
    }
    else {
        throw new Error('Error');
    }
    
    return resultObj.toString();
}

console.log(solve('A', 'C'));
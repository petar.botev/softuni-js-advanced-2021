const { assert } = require('chai');
const sum = require('../04SumOfNumbers'); 

describe('sum of numbers', () => {
    it('should return correct value', () => {
        assert.equal(sum([1,2]), 3);
    });

    it('should return array', () => {
        let arr = [1,2];
        let result = sum(arr);

        assert.isArray(arr);
    });
    it('should asd array', () => {
        let arr = [1];
        let result = sum(arr);

        assert.isArray(arr);
    });
});
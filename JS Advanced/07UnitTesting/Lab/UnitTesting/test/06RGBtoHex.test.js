const { assert } = require('chai');
const rgbToHexColor = require('../06RGBtoHex');

describe('rgb to hex', () => {
    it('red is not integer', () => {
        assert.isUndefined(rgbToHexColor(1.1, 1, 1));
    });
    it('green is not integer', () => {
        assert.isUndefined(rgbToHexColor(1, 1.1, 1));
    });
    it('blue is not integer', () => {
        assert.isUndefined(rgbToHexColor(1, 1, 1.1));
    });

    it('red is below range', () => {
        assert.isUndefined(rgbToHexColor(-1, 1, 1));
    });
    it('green is below range', () => {
        assert.isUndefined(rgbToHexColor(1, -1, 1));
    });

    it('blue is below range', () => {
        assert.isUndefined(rgbToHexColor(1, 1, -1));
    });

    //above the range
    it('red is above range', () => {
        assert.isUndefined(rgbToHexColor(256, 1, 1));
    });
    it('green is above range', () => {
        assert.isUndefined(rgbToHexColor(1, 256, 1));
    });

    it('blue is above range', () => {
        assert.isUndefined(rgbToHexColor(1, 1, 256));
    });

    it('return correct color in hexadecimal', () => {
        assert.equal(rgbToHexColor(255,255,255), '#FFFFFF');
    });

    it('return correct color in hexadecimal if values are zero', () => {
        assert.equal(rgbToHexColor(0,0,0), '#000000');
    });

    it('incorrect parameter', () => {
        assert.notEqual(rgbToHexColor('FF',255,255), '#FFFFFF');
    });
});
const { assert } = require('chai');
const isSymmetric = require('../05CheckforSymmetry');

describe('is-Symetric', () => {
    it('if result is symetric', () => {
        assert.equal(isSymmetric([1,2,1]), true);
    });

    it('result is bot symmetric', () => {
        assert.equal(isSymmetric([1,2,2]), false);
    });

    it('function argument should be an array', () => {
        let isArr = isSymmetric(1,2,1);
        assert.isFalse(isArr);
    });

    it('parameters should be an array', () => {
        let isArr = isSymmetric([1,2,1]);
        assert.isTrue(isArr);
    });

    it('parameters should be an array2', () => {
        let isArr = isSymmetric('a');
        assert.isFalse(isArr);
    });

    it('parameters should be of one type', () => {
        let isArr = isSymmetric(['1', 1]);
        assert.isFalse(isArr);
    });
});
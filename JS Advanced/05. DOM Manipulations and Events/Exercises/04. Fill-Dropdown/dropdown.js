function addItem() {
    let textElement = document.querySelector('#newItemText');
    let valueElement = document.querySelector('#newItemValue');
    
    let optionElement = document.createElement('option');
    optionElement.textContent = textElement.value;
    optionElement.value = valueElement.value;
   
    document.querySelector('#menu').appendChild(optionElement);
    
    textElement.value = '';
    valueElement.value = '';
}
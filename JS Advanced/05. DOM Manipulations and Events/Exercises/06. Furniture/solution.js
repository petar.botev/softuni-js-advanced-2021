function solve() {
  let inputElement = document.querySelector('#exercise :nth-child(2)');
  let tbodyElement = document.querySelector('table tbody');
  document
    .querySelector('#exercise :nth-child(3)')
    .addEventListener('click', function () {
      let inputText = inputElement.value;

      let objs = JSON.parse(inputText);

      for (let i = 0; i < objs.length; i++) {
        let trElement = document.createElement('tr');
        let tdElementImg = document.createElement('td');
        let tdElementTitle = document.createElement('td');
        let tdElementPrice = document.createElement('td');
        let tdElementFactor = document.createElement('td');
        let tdElementType = document.createElement('td');

        //add img
        let imgElement = document.createElement('img');
        console.log(objs[i].img);
        imgElement.setAttribute('src', objs[i].img);
        tdElementImg.appendChild(imgElement);
        trElement.appendChild(tdElementImg);

        //add title
        let titlePElement = document.createElement('p');
        titlePElement.textContent = objs[i].name;
        tdElementTitle.appendChild(titlePElement);
        trElement.appendChild(tdElementTitle);

        //add price
        let priceElement = document.createElement('p');
        priceElement.textContent = objs[i].price;
        tdElementPrice.appendChild(priceElement);
        trElement.appendChild(tdElementPrice);

        //add factor
        let factorElement = document.createElement('p');
        factorElement.textContent = objs[i].decFactor;
        tdElementFactor.appendChild(factorElement);
        trElement.appendChild(tdElementFactor);

        //add type
        let inputElementCheckbox = document.createElement('input');
        inputElementCheckbox.setAttribute('type', 'checkbox');
        //inputElementCheckbox.setAttribute('disabled', true);
        tdElementType.appendChild(inputElementCheckbox);
        trElement.appendChild(tdElementType);

        tbodyElement.appendChild(trElement);
      }

      console.log(objs);

      console.log(inputElement);

      console.log(tbodyElement);
    });


  let outputElement = document.querySelector('#exercise > :nth-child(5)');
  let outputButtonElement = document.querySelector('#exercise > :nth-child(6)');

  outputButtonElement.addEventListener('click', function () {
    let allRows = Array.from(document.querySelectorAll('.table tbody tr'));
    let selectedRows = allRows.filter((x) => x.querySelector('input:checked'));
    let rowNames = selectedRows
      .map((x) => x.querySelector('td:nth-of-type(2) p'))
      .map((x) => x.textContent)
      .join(', ');
    let str = '';
    str += `Bought furniture: ${rowNames}\n`;
    str += `Total price: ${selectedRows.reduce((acc, x) => acc += Number(x.querySelector('td:nth-of-type(3) p').textContent),0).toFixed(2)}\n`;
    let counter = 0;
    str += `Average decoration factor: ${selectedRows.reduce(
      (acc, x) => acc += Number(x.querySelector('td:nth-of-type(4) p').textContent) / selectedRows.length,0
    )}`;

    outputElement.textContent = str;
  });
}

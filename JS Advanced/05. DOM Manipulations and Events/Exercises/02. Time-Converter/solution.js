function attachEventsListeners() {
  let allButtonElements = document.querySelectorAll('input[type="button"]');

  for (let i = 0; i < allButtonElements.length; i++) {
    allButtonElements[i].addEventListener('click', function (e) {
      let days = '';
      let hours = '';
      let minutes = '';
      let seconds = '';
      let value = e.target.previousElementSibling.value;
        if (value != '') {
            switch (e.target.id) {
                case 'daysBtn':
                  days = value;
                  hours = days * 24;
                  minutes = hours * 60;
                  seconds = minutes * 60;
                  
                  break;
                case 'hoursBtn':
                  hours = value;
                  minutes = hours * 60;
                  seconds = minutes * 60;
                  days = hours / 24;
                  
                  break;
                case 'minutesBtn':
                  minutes = value;
                  seconds = value * 60;
                  hours = minutes / 60;
                  days = hours / 24;
                  
                  break;
                case 'secondsBtn':
                  seconds = value;
                  minutes = value / 60;
                  hours = minutes / 60;
                  days = hours / 24;
                  
                  break;
        
                default:
                  break;
              }
              document.querySelector('#days').value = days;
              document.querySelector('#hours').value = hours;
              document.querySelector('#minutes').value = minutes;
              document.querySelector('#seconds').value = seconds;
        }
    });
  }
}

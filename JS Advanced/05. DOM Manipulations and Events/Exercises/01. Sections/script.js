function create(words) {
  let contentElement = document.querySelector('#content');
  for (let i = 0; i < words.length; i++) {
    let newDiv = document.createElement('div');
    let newP = document.createElement('p');
    

    newP.textContent = words[i];
    newP.setAttribute('style', 'display:none');
    newDiv.appendChild(newP);
    contentElement.appendChild(newDiv);
  }

  contentElement.addEventListener('click', function (e) {
    console.log(contentElement.firstChild);
    e.target.firstChild.setAttribute('style', 'display:block');
  });
}
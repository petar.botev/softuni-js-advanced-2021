function encodeAndDecodeMessages() {
  let first = document
    .querySelector('#main')
    .firstElementChild.querySelector('textarea');
  let second = document
    .querySelector('#main')
    .lastElementChild.querySelector('textarea');
  
  first.nextElementSibling.addEventListener('click', function () {
    if (first.value != '') {
      let encripted = first.value.split('').map((x) => x.charCodeAt(0) + 1);
      let encriptedStr = encripted.map((x) => String.fromCharCode(x));
      second.value = encriptedStr.join('');
      first.value = '';
    }
    second.nextElementSibling.addEventListener('click', function (e) {
        let decripted = second.value.split('').map((x) => x.charCodeAt(0) - 1);
        let decriptedStr = decripted.map((x) => String.fromCharCode(x));
    
        second.value = decriptedStr.join('');
        e.target.removeEventListener(e.type, arguments.callee);
      });
  });
}

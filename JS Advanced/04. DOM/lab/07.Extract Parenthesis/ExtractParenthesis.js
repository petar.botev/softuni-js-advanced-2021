function extract(content) {
  let pElement = document.querySelector('#content').textContent;
  let regex = /\(.*?\)/g;
  let allTextInBrackets = pElement.match(regex);
  let resultArr = [];

  allTextInBrackets.forEach((x) => {
    let firstBracketremoved = x.slice(1);
    let finalRemove = firstBracketremoved.substring(
      0,
      firstBracketremoved.length - 1
    );
    resultArr.push(finalRemove);
  });

  let text = resultArr.join('; ') + ';';
  return text;
}

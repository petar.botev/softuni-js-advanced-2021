function calc() {
    let num1Element = document.querySelector('#num1').value;
    let num2Element = document.querySelector('#num2').value;

    let result = Number(num1Element) + Number(num2Element);

    document.querySelector('#sum').value = result;
}

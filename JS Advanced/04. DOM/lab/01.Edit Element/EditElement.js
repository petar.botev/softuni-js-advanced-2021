// function editElement(ref, match, replacer) {
//     let newStr = ref.textContent.replace(match, replacer);
//     //document.querySelector('#e1').textContent = newStr;
//     ref.textContent = newStr;
// }
function editElement(ref, match, replacer){
    let content = ref.textContent;
    let matcher = new RegExp(match, 'g');
    let edited = content.replace(matcher, replacer);
    ref.textContent = edited;
}
function sumTable() {
    let tdElements = document.querySelectorAll('td');
    let sum = 0;
    for (let i = 1; i < tdElements.length - 1; i+=2) {
        sum += Number(tdElements[i].textContent);
    }

    document.querySelector('#sum').textContent = sum;
}
function solve() {
  let textInputElement = document.getElementById('text').value.toLowerCase();

  let namigElement = document.getElementById('naming-convention').value;

  let result = document.getElementById('result');

  let inputArguments = textInputElement.split(' ');

  let toUpper = inputArguments.map((x) => {
    return x.charAt(0).toUpperCase() + x.slice(1);
  });

  if (namigElement == 'Camel Case') {
    let camelStr = toUpper.join('');
    result.textContent = camelStr.charAt(0).toLowerCase() + camelStr.slice(1);
    
  } else if (namigElement == 'Pascal Case') {
    result.textContent = toUpper.join('');
    
  } else {
    result.textContent = 'Error!';
  }
}

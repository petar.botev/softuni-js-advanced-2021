function toggle() {
    let extraElement = document.getElementById('extra');
    let buttonElement = document.querySelector('.button');
    if (extraElement.style.display == 'none') {
        extraElement.style.display = 'block';
        buttonElement.textContent = 'Less';
    }
    else{
        extraElement.style.display = 'none';
        buttonElement.textContent = 'More';
    }
}
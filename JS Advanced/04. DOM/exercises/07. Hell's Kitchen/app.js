function solve() {
   document.querySelector('#btnSend').addEventListener('click', onClick);

   function onClick() {
      let regex = /\["|",\n?"|"\]/g;
      let allRestaurants = document.querySelector('#inputs textarea').value.split(regex);
      let restaurants = [];

      for (let i = 1; i < allRestaurants.length - 1; i++) {

         let restaurantArgs = allRestaurants[i].split(' - ');
         let restaurantName = restaurantArgs[0];
         let workersStringArr = restaurantArgs[1].split(', ');

         let workerName = '';
         let workerSalary = '';
         let restaurantObj = '';

         if (!restaurants.some(x => x.name == restaurantName)) {
            restaurantObj = {name: restaurantName, workers: []};
         }
         
         workersStringArr.forEach(el => {
            let workerArgs = el.split(' ');
            workerName = workerArgs[0];
            workerSalary = workerArgs[1];
            let workerObj = {name: workerName, salary: workerSalary};
            let currRestaurant = restaurants.find(x => x.name == restaurantName)
            if (currRestaurant == undefined) {
               restaurantObj.workers.push(workerObj);
            }else{
               let currWorker = restaurants.workers.find(x => x.name == workerName);
               currWorker.salary = workerSalary;
            }
            
         });
         
         restaurants.push(restaurantObj);
      }

      let bestRestaurant = restaurants.reduce((acc, x) => {
         let sum = x.workers.reduce((acc2, w) => {
            acc2 += Number(w.salary);
            return acc2;
         }, 0);
         let maxSum = Number.MIN_SAFE_INTEGER;
         if (maxSum < Number(sum)) {
            acc = x;
         }
         return acc;
      }, Number(0));

      let bestRestaurantSalary = bestRestaurant.workers.reduce((acc, w) => {
         acc += Number(w.salary);
         return acc;
      }, 0);

      let bestSalary = bestRestaurant.workers.reduce((acc, w) => {
         
         if(acc < Number(w.salary)){
            acc = Number(w.salary);
         }
         
         return Number(acc).toFixed(2);
      }, 0);

      let bestRestaurantAvgSalary = (bestRestaurantSalary / bestRestaurant.workers.length).toFixed(2);
      
      let bestRestaurantElement = document.querySelector('#bestRestaurant p');
      bestRestaurantElement.textContent = `Name: ${bestRestaurant.name} Average Salary: ${bestRestaurantAvgSalary} Best Salary: ${bestSalary}`;

      let workers = '';

      bestRestaurant.workers.sort((a,b) => a - b).forEach(x => {
           workers += `Name: ${x.name} With Salary: ${x.salary} `;
      });

      let workersElement = document.querySelector('#workers p');
      workersElement.textContent = workers;
   }
}
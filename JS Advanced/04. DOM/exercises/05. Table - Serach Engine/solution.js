function solve() {
  document.querySelector('#searchBtn').addEventListener('click', onClick);

  function onClick() {
    let inputElement = document.getElementById('searchField').value;

    let allTdElements = document.querySelectorAll('table tbody tr td');

    for (let i = 0; i < allTdElements.length; i++) {
      if (i % 3 == 0 || i == 0) {
        allTdElements[i].parentNode.removeAttribute('class');
      }
      if (allTdElements[i].textContent.includes(inputElement)) {
        allTdElements[i].parentNode.setAttribute('class', 'select');
      }
    }

    document.getElementById('searchField').value = '';
  }
}

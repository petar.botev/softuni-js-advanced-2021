function solve() {
  let textAreaElement = document.getElementById('input');
  let outputElement = document.getElementById('output');  
  let allSentances = textAreaElement.value.substring(0, textAreaElement.value.length - 1).split('.');
  let str = '';
  for (let i = 1; i <= allSentances.length; i++) {
    
    str += allSentances[i - 1]+'.';
    if (i % 3 == 0 || i == allSentances.length) {
      let newP = document.createElement('p');
      newP.textContent = str;
      
      outputElement.appendChild(newP);

      str = '';
    }
  }
}
function search() {
  
   let allTownsElements = document.querySelectorAll('#towns li');
   
   let inputValue = document.getElementById('searchText').value;
   let counter = 0;
   let resultElement = document.getElementById('result');

   for (let i = 0; i < allTownsElements.length; i++) {
      
      allTownsElements[i].style.textDecoration = 'none';
      allTownsElements[i].style.fontWeight = 'normal';
      if (allTownsElements[i].textContent.includes(inputValue) && inputValue != '') {
        
         allTownsElements[i].style.textDecoration = 'underline';
         allTownsElements[i].style.fontWeight = 'bold';
         counter++;
      }
   }

   resultElement.textContent = `${counter} matches found`;
}

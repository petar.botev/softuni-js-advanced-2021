function subtract() {
    let firstNumberElement = document.getElementById('firstNumber');
    let secondNumberElement = document.getElementById('secondNumber');
    firstNumberElement.removeAttribute('disabled');
    secondNumberElement.removeAttribute('disabled');
    firstNumber = firstNumberElement.value;
    secondNumber = secondNumberElement.value;

    let result = Number(firstNumber) - Number(secondNumber);
    document.getElementById('result').textContent = result;
    
    console.log(
        result
    );
}
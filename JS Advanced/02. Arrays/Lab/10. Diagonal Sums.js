function execute(matrix) {
  let firstDiagonal = 0;
  let secondDiagonal = 0;
  for (let i = 0; i < matrix.length; i++) {
    firstDiagonal += matrix[i][i];
  }
  for (let i = matrix.length - 1, j = 0; j < matrix.length; i--, j++) {
      
      secondDiagonal += matrix[j][i];
  }
  
  return firstDiagonal + ' ' + secondDiagonal;
}

console.log(
  execute([
    [3, 5, 17],
    [-1, 7, 14],  
    [1, -8, 89],
  ])
);

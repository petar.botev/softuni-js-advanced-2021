function execute(n, k){
    let arr = [];
    arr.push(1);
    while (arr.length < n) {
        let lastSum = arr.slice(-k).reduce((acc, v) => acc + v, 0);
        arr.push(lastSum);
    }
    
    return arr;
}

console.log(execute(8, 2));
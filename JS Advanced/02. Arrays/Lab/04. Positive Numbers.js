function execute(arr){

    arr.forEach((element, i) => {
        if (element < 0) {
            arr.unshift(arr.splice(i, 1)[0]);
        }
    });

    return arr;
}

console.log(execute([3, -2, 0, -1]));
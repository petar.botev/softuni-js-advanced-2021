function evenPosition(arr){

   let evenElements = arr.filter((x, i) => i % 2 == 0);
   return evenElements.join(' ');
}

console.log(evenPosition(['20', '30', '40', '50', '60']));
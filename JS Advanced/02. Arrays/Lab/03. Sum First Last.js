function execute(arr){

    let firstElement = Number(arr.shift());
    let lastElement = Number(arr.pop());

    return firstElement + lastElement;
}

console.log(execute(['20', '30', '40']));
function execute(matrix) {
  return matrix.reduce((acc, x) => {
    x.forEach((e) => {
      if (acc < e) {
        acc = e;
      }
    });

    return acc;
  }, Number.MIN_SAFE_INTEGER);
}

console.log(
  execute([
    [3, 5, 7, 12],
    [-1, 4, 33, 2],
    [8, 3, 0, 4],
  ])
);

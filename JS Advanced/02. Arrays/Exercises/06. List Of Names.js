function execute(arr){
    let res = arr.sort((a,b) => a.localeCompare(b)).map((x,i) => x = `${i+1}.${x}`);

    return res.join('\n');
}


console.log(execute(["John", "Bob", "Christina", "Ema"]));
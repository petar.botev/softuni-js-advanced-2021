function execute(arr) {
  let res = arr.reduce((acc, x, i, a) => {
    if (i == 0) {
      acc.push(a[i]);
    } else {
      if (a[i] >= acc[acc.length - 1]) {
        acc.push(a[i]);
      }
    }

    return acc;
  }, []);

  return res;
}

console.log(execute([1, 3, 8, 4, 10, 12, 3, 2, 24]));

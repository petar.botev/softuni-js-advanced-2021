function execute(arr, number) {
  return arr.filter((x, i) => i % number === 0);
}

console.log(execute(['1', '2', '3', '4', '5'], 6));

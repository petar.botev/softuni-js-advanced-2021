function execute(arr) {
  let sorted = arr.sort((a, b) => {
    if (a.length < b.length) {
      return -1;
    } else if (a.length > b.length) {
      return 1;
    } else {
      if (a.localeCompare(b) == 1) {
        return 1;
      } else if (a.localeCompare(b) == -1) {
        return -1;
      } else {
        return 0;
      }
    }
  });

  return sorted.join('\n');
}

console.log(execute(['test', 'Deny', 'omen', 'Default']));

function execute(arr){
    let sorted = arr.sort((a,b) => a-b);
    let resultArr = [];

    while(sorted.length > 0){
        resultArr.push(sorted.shift());
        if(sorted.length > 0){
           resultArr.push(sorted.pop());
        }
    }

    return resultArr;
}

console.log(execute([1, 65, 3, 52, 48, 63, 31, -3, 18, 56]));
function execute(matrix) {
  let sum = 0;
  for (let i = 0; i < matrix.length; i++) {
    var currSum = matrix[i].reduce((acc, x, i, arr) => {
      acc += Number(x);
      return acc;
    }, 0);

    if (i == 0) {
      sum = currSum;
    } else {
      if (sum !== currSum) {
        return false;
      }
    }
  }

  sum = 0;

  for (let i = 0; i < matrix.length; i++) {
    let currSum2 = 0;
    for (let j = 0; j < matrix.length; j++) {
      currSum2 += Number(matrix[j][i]);
      if (i == 0) {
        sum = currSum2;
      }
    }
    if (currSum2 !== sum) {
      return false;
    }
  }

  return true;
}

console.log(
  execute([
    [11, 32, 45],
    [21, 0, 1],
    [21, 1, 1],
  ])
);

function execute(arr) {
  let myArr = [];
  arr.forEach((e, i) => {
    if (e == 'add') {
      myArr.push(i + 1);
    } else if (e == 'remove') {
      myArr.pop();
    }
  });
  if (myArr.length > 0) {
    return myArr.join('\n');
  } else {
    return 'Empty';
  }
}

console.log(execute(['remove', 'remove', 'remove']));

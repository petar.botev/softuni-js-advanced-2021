function execute(arr, separator) {
    return arr.join(separator);
}

console.log(execute(['One', 'Two', 'Three', 'Four', 'Five'], '-'));

const { assert } = require('chai');
const { HolidayPackage } = require('../app');

describe('tests', function () {
  describe('addVacationer', function () {
    it('should throw exeption when vacantioner is not string or is empty string', function () {
      let instance = new HolidayPackage('Italy', 'Summer');

      assert.throw(() => instance.addVacationer(12));
      assert.throw(() => instance.addVacationer(' '));
    });

    it('should throw exeption when vacantioner is not at least two words', function () {
      let instance = new HolidayPackage('Italy', 'Summer');

      assert.throw(() => instance.addVacationer('asd'));
    });

    it('should add vacantioner to array when executed', function () {
      let instance = new HolidayPackage('Italy', 'Summer');
      instance.addVacationer('asd asd');
      assert.equal(instance.vacationers.length, 1);
      assert.equal(instance.vacationers[0], 'asd asd');
    });
  });

  describe('showVacationers', function () {
    it('should return vacantioner when array length is more than zero', function () {
      let instance = new HolidayPackage('Italy', 'Summer');
      instance.addVacationer('asd asd');
      instance.addVacationer('dsa dsa');

      assert.equal(instance.showVacationers(), "Vacationers:\n" + instance.vacationers.join("\n"));
      
    });

    it('should return message when array length is zero', function () {
        let instance = new HolidayPackage('Italy', 'Summer');
  
        assert.equal(instance.showVacationers(), "No vacationers are added yet");
        
      });
  });

  describe('insuranceIncluded', function () {
    it('should return insurance when executed', function () {
      let instance = new HolidayPackage('Italy', 'Summer');

      assert.equal(instance.insuranceIncluded, false);
      
    });

    it('should set when executed', function () {
        let instance = new HolidayPackage('Italy', 'Summer');
        
        instance.insuranceIncluded = true;
  
        assert.equal(instance.insuranceIncluded, true);
        
      });

      it('should throw error when executed', function () {
        let instance = new HolidayPackage('Italy', 'Summer');
  
        assert.throw(() => instance.insuranceIncluded = 'true', "Insurance status must be a boolean");
        
      });
    
  });

  describe('generateHolidayPackage', function () {
    it('should throw error when vacantioner length is less than 1', function () {
      let instance = new HolidayPackage('Italy', 'Summer');

      assert.throw(() => instance.generateHolidayPackage());
      
    });

    it('should return vacation price when executed', function () {
        let instance = new HolidayPackage('Italy', 'Spring');
        instance.addVacationer('asd asd');

        assert.equal(instance.generateHolidayPackage(), "Holiday Package Generated\n" +
        "Destination: " + instance.destination + "\n" +
        instance.showVacationers() + "\n" +
        "Price: " + 400);
        
      });

      it('should add seasonal price when season is summer', function () {
        let instance = new HolidayPackage('Italy', 'Summer');
        instance.addVacationer('asd asd');

        assert.equal(instance.generateHolidayPackage(), "Holiday Package Generated\n" +
        "Destination: " + instance.destination + "\n" +
        instance.showVacationers() + "\n" +
        "Price: " + 600);
        
      });

      it('should add seasonal price when season is winter', function () {
        let instance = new HolidayPackage('Italy', 'Winter');
        instance.addVacationer('asd asd');

        assert.equal(instance.generateHolidayPackage(), "Holiday Package Generated\n" +
        "Destination: " + instance.destination + "\n" +
        instance.showVacationers() + "\n" +
        "Price: " + 600);
        
      });

      it('should add price for insurance when it is true', function () {
        let instance = new HolidayPackage('Italy', 'Winter');
        instance.addVacationer('asd asd');
        instance.insuranceIncluded = true;

        assert.equal(instance.generateHolidayPackage(), "Holiday Package Generated\n" +
        "Destination: " + instance.destination + "\n" +
        instance.showVacationers() + "\n" +
        "Price: " + 700);
        
      });
  });
});

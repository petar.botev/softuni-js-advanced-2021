function solution() {
    document.querySelector('input[placeholder="Gift name"]').nextSibling.nextSibling.addEventListener('click', function (){
        let addGiftInputElement = document.querySelector('input[placeholder="Gift name"]');
        let addGiftInputText = addGiftInputElement.value;

        let listOfGiftsElement = Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'List of gifts');
        listOfGiftsElement.nextElementSibling.appendChild(createGift(addGiftInputText));

        addGiftInputElement.value = '';
        sortGiftList(listOfGiftsElement);

        document.getElementById('sendButton').addEventListener('click', function (e){
            let elementToMove = e.target.parentNode;
            let listOfGiftsElement = Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'Sent gifts');
            listOfGiftsElement.parentNode.querySelector('ul').appendChild(elementToMove);
            
            elementToMove.querySelector('button').remove();
            elementToMove.querySelector('button').remove();
        });

        document.getElementById('discardButton').addEventListener('click', function (e){
            let elementToMove = e.target.parentNode;
            let listOfGiftsElement = Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'Discarded gifts');
            listOfGiftsElement.parentNode.querySelector('ul').appendChild(elementToMove);
            
            elementToMove.querySelector('button').remove();
            elementToMove.querySelector('button').remove();
        });
    });

    function sortGiftList(lisOfGifs){
        let allLiEls = Array.from(lisOfGifs.nextElementSibling.querySelectorAll('li'));
      
        allLiEls.sort((a,b) => a.childNodes[0].data.localeCompare(b.childNodes[0].data));

        for (let i = 0; i < allLiEls.length; i++) {
            lisOfGifs.nextElementSibling.appendChild(allLiEls[i]);
        }
    }
    
    function createGift(gift){
        let newLiElementGifts = document.createElement('li');
        newLiElementGifts.setAttribute('class', 'gift');

        let newButtonElementSend = document.createElement('button');
        newButtonElementSend.setAttribute('id', 'sendButton');
        newButtonElementSend.textContent = 'Send';

        let newButtonElementDiscard = document.createElement('button');
        newButtonElementDiscard.setAttribute('id', 'discardButton');
        newButtonElementDiscard.textContent = 'Discard';

        newLiElementGifts.textContent = gift;
        newLiElementGifts.appendChild(newButtonElementSend);
        newLiElementGifts.appendChild(newButtonElementDiscard);

        return newLiElementGifts;
    }
}
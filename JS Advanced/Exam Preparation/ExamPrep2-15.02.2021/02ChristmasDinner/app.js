class ChristmasDinner {

    constructor(budget){
        this.budget = budget;
        this.dishes = [];
        this.products = [];
        this.guests = {};    
    }

    set budget(value){
        if(value < 0){
            throw new Error("The budget cannot be a negative number");
        }
        this._budget = value;
    }

    get budget(){
        return this._budget;
    }

    shopping(product){
        let prodType = product[0];
        let price = Number(product[1]);
        if((this.budget - price) <= 0){
            throw new Error("Not enough money to buy this product");
        }
        this.products.push(prodType);
        this.budget -= price;

        return `You have successfully bought ${prodType}!`;
    }

    recipes(recipe){
        let recipeName = recipe.recipeName;
        let productsList = recipe.productsList;
        
        for (let i = 0; i < productsList.length; i++) {
           if(!this.products.includes(productsList[i])){
               throw new Error("We do not have this product");
           }
        }

        let newDish = {recipeName,productsList};
        this.dishes.push(newDish);
        
        return `${recipeName} has been successfully cooked!`;
    }

    inviteGuests(name, dish){
        
        if(!this.dishes.some(x => x.recipeName == dish)){
            throw new Error("We do not have this dish");
        }
        if(this.guests.hasOwnProperty(name)){
            throw new Error("This guest has already been invited");
        }
        this.guests[name] = dish;
        return `You have successfully invited ${name}!`;
    }

    showAttendance(){
       let res = '';
       let guestKeys = Object.keys(this.guests);
       let guestValues = Object.values(this.guests);
       
        for (let i = 0; i < guestKeys.length; i++) {
            let currDish = this.dishes.find(x => x.recipeName == guestValues[i]);
            
            if (i == guestKeys.length - 1){
                res += `${guestKeys[i]} will eat ${guestValues[i]}, which consists of ${currDish.productsList.join(', ')}`;
            }
            else{
                res += `${guestKeys[i]} will eat ${guestValues[i]}, which consists of ${currDish.productsList.join(', ')}\n`;
            }
        }

        return res;
    }
}


let dinner = new ChristmasDinner(300);

dinner.shopping(['Salt', 1]);
dinner.shopping(['Beans', 3]);
dinner.shopping(['Cabbage', 4]);
dinner.shopping(['Rice', 2]);
dinner.shopping(['Savory', 1]);
dinner.shopping(['Peppers', 1]);
dinner.shopping(['Fruits', 40]);
dinner.shopping(['Honey', 10]);

dinner.recipes({
    recipeName: 'Oshav',
    productsList: ['Fruits', 'Honey']
});
dinner.recipes({
    recipeName: 'Folded cabbage leaves filled with rice',
    productsList: ['Cabbage', 'Rice', 'Salt', 'Savory']
});
dinner.recipes({
    recipeName: 'Peppers filled with beans',
    productsList: ['Beans', 'Peppers', 'Salt']
});

dinner.inviteGuests('Ivan', 'Oshav');
dinner.inviteGuests('Petar', 'Folded cabbage leaves filled with rice');
dinner.inviteGuests('Georgi', 'Peppers filled with beans');

console.log(dinner.showAttendance());

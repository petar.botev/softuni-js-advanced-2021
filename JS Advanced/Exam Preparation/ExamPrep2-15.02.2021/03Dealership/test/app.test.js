const { assert } = require('chai');
const dealership = require('../app');

describe("Tests …", function() {
    describe("newCarCost", function() {

        it('should return price with discount when car is old', function() {
            assert.equal(dealership.newCarCost('Audi A4 B8', 15001), 1);
        });
        it('should return price withont discount when car is new', function() {
            assert.equal(dealership.newCarCost('Audi A4 B1', 1), 1);
        });
    });

    describe("carEquipment", function() {

        it('should return array when executed', function() {
            assert.isArray(dealership.carEquipment(['asd', 'dsa'], [0,1]));
        });
        it('should return array with indexes when executed', function() {
            assert.deepEqual(dealership.carEquipment(['asd', 'dsa'], [0,1]), ['asd', 'dsa']);
        });
    });

    describe("euroCategory", function() {

        it('should have discount when category is 4 or higher', function() {

            assert.equal(dealership.euroCategory(4), `We have added 5% discount to the final price: 14250.`);
        });
        it('should not have discount when cartegory is below 4', function() {
            assert.equal(dealership.euroCategory(3), 'Your euro category is low, so there is no discount from the final price!');
        });
    });
});

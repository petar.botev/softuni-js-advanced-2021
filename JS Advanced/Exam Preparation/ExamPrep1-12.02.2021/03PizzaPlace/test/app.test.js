const { assert } = require('chai');
const pizzUni = require('../app');

describe('Tests', function(){
    describe('makeAnOrder', function(){
        it('should throw error when object orderedPizza is false', function(){
            assert.throws(() => pizzUni.makeAnOrder(false));
        });
        it('should return only pizza when no drink', function(){
            let obj = {orderedPizza: 'pizza'}
             
            assert.equal(pizzUni.makeAnOrder(obj), `You just ordered ${obj.orderedPizza}`);
        });
    
        it('should return drink and pizza when there is a drink', function(){
            let obj = {orderedPizza: 'pizza', orderedDrink: 'drink'}
             
            assert.equal(pizzUni.makeAnOrder(obj), `You just ordered ${obj.orderedPizza} and ${obj.orderedDrink}.`);
        });
    });

    describe('getRemainingWork', function (){
        it('should return pizza names when there are not finished pizzas', function (){
            let arr = [{pizzaName:'pizza', status:'preparing'}];
            assert.equal(pizzUni.getRemainingWork(arr),`The following pizzas are still preparing: pizza.`);
        });
        it('should return text when all pizzas are finished', function (){
            let arr = [{pizzaName:'pizza', status:'ready'}];
            assert.equal(pizzUni.getRemainingWork(arr),`All orders are complete!`);
        });
    });
    
    describe('orderType', function (){
        it('should return total sum plus additionals when is takeaway', function (){
            
            assert.equal(pizzUni.orderType(2, 'Carry Out'), 1.8);
        });
        it('should return total sum when is normal order', function (){
            
            assert.equal(pizzUni.orderType(2, 'Delivery'),2);
        });
    });
});
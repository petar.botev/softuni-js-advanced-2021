function solve() {
  document
    .querySelector('.form-control button')
    .addEventListener('click', function (e) {
      e.preventDefault();

      // get lecture name
      let lectureNameElement = document.querySelector(
        'input[name="lecture-name"]'
      );
      let lectureNameInput = lectureNameElement.value;

      // get date
      let lectureDateElement = document.querySelector(
        'input[name="lecture-date"]'
      );
      let lectureDateInput = lectureDateElement.value;
        console.log(lectureDateInput);

       let realDate = formatDate(lectureDateInput);

      
      // get module
      let moduleElement = document.querySelector(
        'select[name="lecture-module"]'
      );
      let moduleInput = moduleElement.value;

      if (
        lectureNameInput == '' ||
        realDate.substring(0, 3) == 'NaN' ||
        moduleInput == 'Select module'
      ) {
        return;
      }

      // create HTML structure
      // h4 in Li
      let newH4Element = document.createElement('h4');
      newH4Element.textContent = `${lectureNameInput} - ${realDate}`;

      //button delete
      let newButtonDelete = document.createElement('button');
      newButtonDelete.setAttribute('class', 'red');
      newButtonDelete.textContent = 'Del';
      console.log(newButtonDelete);

      // li element
      let newLiElement = document.createElement('li');
      newLiElement.setAttribute('class', 'flex');
      newLiElement.appendChild(newH4Element);
      newLiElement.appendChild(newButtonDelete);

      //ul element
      let newUlElement = document.createElement('ul');
      newUlElement.appendChild(newLiElement);

      // h3
      let newH3Element = document.createElement('h3');
      newH3Element.textContent = `${moduleInput.toUpperCase()}-MODULE`;

      let allH3Elements = Array.from(document.querySelectorAll('h3'));
      let allUlElements = document.querySelectorAll('.module');
      for (let i = 0; i < allUlElements.length; i++) {
          let lis = Array.from(allUlElements[i].querySelectorAll('li'));
          let res = lis.map(x => {
            return x.textContent.split(' - ')[1];
          }).sort((a,b) => a.localeCompare(b));
          console.log(res);
      }
     
      let newDivModuleElement = '';
      if (allH3Elements.some((x) => x.textContent == newH3Element.textContent)) {
        let currH3Element = allH3Elements.find(x => x.textContent == newH3Element.textContent);
        currH3Element.parentNode.appendChild(newUlElement);
        let alUlEl = Array.from(currH3Element.parentNode.querySelectorAll('ul')).sort((a,b) => a.querySelector('li h4').textContent.split(' - ')[1].localeCompare(b.querySelector('li h4').textContent.split(' - ')[1]));
        console.log(alUlEl);

       for (let i = 0; i < alUlEl.length; i++) {
        currH3Element.parentNode.appendChild(alUlEl[i]);
       }
        
      } else {
        // div module
        newDivModuleElement = document.createElement('div');
        newDivModuleElement.setAttribute('class', 'module');
        newDivModuleElement.appendChild(newH3Element);
        newDivModuleElement.appendChild(newUlElement);
        
        document.querySelector('.modules').appendChild(newDivModuleElement);
      }

      let allDelButtons = document.querySelectorAll('.red');
      for (let i = 0; i < allDelButtons.length; i++) {
          allDelButtons[i].addEventListener('click', function(e){
               let removed =  e.target.parentNode.parentNode.parentNode;
               e.target.parentNode.parentNode.remove();
                console.log(removed);
                if (removed.querySelectorAll('.red').length == 0) {
                    //e.target.parentNode.parentNode.parentNode.remove();
                    removed.remove();
                }

          });
      }
    });

    function formatDate(date){
        let [currDate, currTime] = date.split('T');
        currDate = currDate.replaceAll('-','/');

        return `${currDate} - ${currTime}`;
    }
}

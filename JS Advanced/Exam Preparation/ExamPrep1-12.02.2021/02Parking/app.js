class Parking {
    constructor ( capacity ){
        this.capacity = Number(capacity); 
        this.vehicles = []; 
    }

    addCar( carModel, carNumber ){
        if(this.vehicles.length == this.capacity){
            throw new Error("Not enough parking space."); 
        }
        this.vehicles.push({carModel:carModel, carNumber:carNumber, payed:false});
        return `The ${carModel}, with a registration number ${carNumber}, parked.`;
    }

    removeCar( carNumber ){
        let currCar = this.vehicles.find(x => x.carNumber == carNumber);
        if(currCar == undefined){
            throw new Error("The car, you're looking for, is not found.");
        }

        if(currCar.payed == false){
            throw new Error(`${carNumber} needs to pay before leaving the parking lot.`);
        }

        let currCarIndex = this.vehicles.indexOf(currCar);
        this.vehicles.splice(currCarIndex, 1);

        return `${carNumber} left the parking lot.`;
    }

    pay( carNumber ) {
        let currCar = this.vehicles.find(x => x.carNumber == carNumber);
        if(currCar == undefined){
            throw new Error(`${carNumber} is not in the parking lot.`);
        }
        if(currCar.payed == true){
            throw new Error(`${carNumber}'s driver has already payed his ticket.`);
        }

        currCar.payed = true;

        return `${carNumber}'s driver successfully payed for his stay.`;
    }

    getStatistics(carNumber) {
        if(typeof carNumber === 'undefined'){
            let emptySlots = this.capacity - this.vehicles.length;
            let firstRow = `The Parking Lot has ${ emptySlots } empty spots left.`;
            let sortedCars = this.vehicles.sort((a,b) => a.carModel.localeCompare(b.carModel));
            sortedCars.forEach(x => {
                let res = firstRow += `\n${x.carModel} == ${x.carNumber} - ${x.payed == true ? 'Has payed' : 'Not payed'}`;
                
            });
            return firstRow;
        }
        let currCar = this.vehicles.find(x => x.carNumber == carNumber);
        return `${currCar.carModel} == ${currCar.carNumber} - ${currCar.payed == true ? 'Has payed' : 'Not payed'}`;
    }
}

const parking = new Parking(12);

console.log(parking.addCar("Volvo t600", "TX3691CA"));
console.log(parking.getStatistics());

console.log(parking.pay("TX3691CA"));
console.log(parking.removeCar("TX3691CA"));



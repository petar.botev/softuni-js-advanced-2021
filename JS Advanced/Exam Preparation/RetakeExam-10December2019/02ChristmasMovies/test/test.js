const { assert } = require('chai');
const ChristmasMovies = require('../Resources');
const cristmasMovie = require('../Resources');


describe("Tests", function() {
    
    describe('initial', function (){
        it('should be set correctly when create instance', function (){
            let movieObj = new ChristmasMovies();
            assert.isArray(movieObj.movieCollection);
            assert.equal(movieObj.movieCollection.length, 0);
            assert.isObject(movieObj.watched);
            
            assert.isArray(movieObj.actors);
            assert.equal(movieObj.actors.length, 0);
        });
    });

    describe("buyMovie", function() {
        it("should add movie to array when movie does not exist", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';

            let result = movieObj.buyMovie(movieName, actors);

            assert.equal(result, `You just got ${movieName} to your collection in which ${actors.join(', ')} are taking part!`);
        });

        it("should throw error when movie exists", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            //let result = movieObj.buyMovie(movieName, actors);

            assert.throw(() => movieObj.buyMovie(movieName, actors), `You already own ${movieName} in your collection!`);
        });

        it("should have movie in the collection when after execution", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            //let result = movieObj.buyMovie(movieName, actors);

            assert.equal(movieObj.movieCollection.length, 1);
        });


     });

    describe("discardMovie", function() {
        it("should throw error when movie is not in the collection", function() {
            let movieObj = new ChristmasMovies();
            let movieName = 'ab';
            assert.throw(() => movieObj.discardMovie(movieName), `${movieName} is not at your collection!`);
        });

        it("should delete movie when movie exists and is already watched", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);

            assert.equal(movieObj.discardMovie(movieName), `You just threw away ${movieName}!`);
        });

        it("should return empty arr when movie is deleted", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);
            movieObj.discardMovie(movieName);

            assert.equal(movieObj.movieCollection.length, 0);
        });

        it("should not delete movie when movie is not watched", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);

            assert.throw(() => movieObj.discardMovie(movieName), `${movieName} is not watched!`);
        });

     });

     describe("watchMovie", function() {
        it("should add wached property when movie exists", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);
            let propCount = Object.entries(movieObj.watched).length;

            assert.equal(propCount, 1);
        });

        it("should add one time wached when movie exists", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);
            
            let propCount = Object.entries(movieObj.watched)[0];
            let res = propCount[1];

            assert.equal(res, 1);
        });

        it("should add wached times when movie exists", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);
            movieObj.watchMovie(movieName);
            let propCount = Object.entries(movieObj.watched)[0];
            let res = propCount[1];

            assert.equal(res, 2);
        });

        it("should throw error when movie does not exist", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ds';    
            
            movieObj.buyMovie(movieName, actors);
            
            assert.throw(() => movieObj.watchMovie('ac'), 'No such movie in your collection!');
        });

     });

     describe("favouriteMovie", function() {
        it("should show favorite movie when collection has watched movies", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.watchMovie(movieName);
            movieObj.watchMovie(movieName);
            let asd = movieObj.watched[movieName];

            assert.equal(movieObj.favouriteMovie(), `Your favourite movie is ${movieName} and you have watched it ${movieObj.watched[movieName]} times!`);
        });

        it("should throw error when movie is not watched yet", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            
            movieObj.buyMovie(movieName, actors);

            assert.throw(() => movieObj.favouriteMovie(), 'You have not watched a movie yet this year!');
        });

     });


     describe("mostStarredActor", function() {
        it("should return most starred actor when movie exists", function() {
            let movieObj = new ChristmasMovies();
            let actors = ['a','b','c'];
            let movieName = 'ab';
            let actors2 = ['a','v','g'];
            let movieName2 = 'aa';
            
            movieObj.buyMovie(movieName, actors);
            movieObj.buyMovie(movieName2, actors2);
            movieObj.watchMovie(movieName);

            assert.equal(movieObj.mostStarredActor(), 'The most starred actor is a and starred in 2 movies!');
        });

        it("should throw error when movie is not watched yet", function() {
            let movieObj = new ChristmasMovies();
            
            assert.throw(() => movieObj.mostStarredActor(), 'You have not watched a movie yet this year!');
        });

     });
});

function solution() {
   document.querySelector('input[placeholder="Gift name"]').nextElementSibling.addEventListener('click', () =>{
        let giftInputElement = document.querySelector('input[placeholder="Gift name"]');
        let giftInputValue = giftInputElement.value;

       let giftDescription = createGiftElement(giftInputValue);
       let treeElement = Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'List of gifts').nextElementSibling;

       treeElement.appendChild(giftDescription);
       sortGifts(treeElement);

       giftInputElement.value = '';
   });

   function discardGifts(e){
    let elementToMove = e.target.parentNode;
    elementToMove.firstElementChild.remove();
    elementToMove.firstElementChild.remove();

    Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'Discarded gifts').nextElementSibling.appendChild(elementToMove);
   }

   function sortGifts(parentElement){
       let allLiElements = Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'List of gifts').nextElementSibling.querySelectorAll('li');
       let allLiElementsSorted = Array.from(allLiElements).sort((a,b) => a.firstChild.textContent.localeCompare(b.firstChild.textContent));

       for (let i = 0; i < allLiElementsSorted.length; i++) {
           parentElement.appendChild(allLiElementsSorted[i]);
       }
   }

   function sendGifts(e){
        let elementToMove = e.target.parentNode;
        elementToMove.firstElementChild.remove();
        elementToMove.firstElementChild.remove();

        Array.from(document.querySelectorAll('h2')).find(x => x.textContent == 'Sent gifts').nextElementSibling.appendChild(elementToMove);
   }

   function createGiftElement(value){
       let newLiElement = document.createElement('li');
        newLiElement.textContent = value;

        let newButtonSend = document.createElement('button');
        newButtonSend.setAttribute('id', 'sendButton');
        newButtonSend.textContent = 'Send';
        newButtonSend.addEventListener('click', sendGifts);
        newLiElement.appendChild(newButtonSend);

        let newButtonDiscard = document.createElement('button');
        newButtonDiscard.setAttribute('id', 'discardButton');
        newButtonDiscard.textContent = 'Discard';
        newButtonDiscard.addEventListener('click', discardGifts);
        newLiElement.appendChild(newButtonDiscard);
       
        return newLiElement;
   }
}
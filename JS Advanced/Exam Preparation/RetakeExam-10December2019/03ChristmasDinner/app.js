class ChristmasDinner {
  constructor(budget) {
    this.budget = budget;
    this.dishes = [];
    this.products = [];
    this.guests = {};
  }

  set budget(value){
      if(value < 0){
          throw new Error("The budget cannot be a negative number");
      }

      this._budget = value;
  }

  get budget(){
      return this._budget;
  }

  shopping([product, price]){
    if(this.budget < price){
        throw new Error("Not enough money to buy this product");
    }
    this.products.push(product);
    this.budget -= price;

    return `You have successfully bought ${product}!`;
  }

  recipes(recipe){
    let recipeName = recipe.recipeName;
    let products = recipe.productsList;

    for (let i = 0; i < products.length; i++) {
        
        let isExists = this.products.includes(products[i]);
        if(isExists == false){
            throw new Error("We do not have this product");
        }
    }

    this.dishes.push(recipe);

    return `${recipeName} has been successfully cooked!`;
  }

  inviteGuests(name, dish){
      if(this.dishes.some(x => x.recipeName == dish) == false){
          throw new Error("We do not have this dish");
      }
      if(Object.entries(this.guests).some(x => x[0] == name)){
          throw new Error("This guest has already been invited");
      }

      this.guests[name] = dish;

      return `You have successfully invited ${name}!`;
  }

  showAttendance(){
      let allPeople = [];
      let allGuests = Object.entries(this.guests);

      for (let i = 0; i < allGuests.length; i++) {
          let personMealProducts = this.dishes.find(x => x.recipeName == allGuests[i][1]);
          allPeople.push(`${allGuests[i][0]} will eat ${allGuests[i][1]}, which consists of ${personMealProducts.productsList.join(', ')}`);
      }

      return allPeople.join('\n');
   }
}

let dinner = new ChristmasDinner(300);

dinner.shopping(['Salt', 1]);
dinner.shopping(['Beans', 3]);
dinner.shopping(['Cabbage', 4]);
dinner.shopping(['Rice', 2]);
dinner.shopping(['Savory', 1]);
dinner.shopping(['Peppers', 1]);
dinner.shopping(['Fruits', 40]);
dinner.shopping(['Honey', 10]);

dinner.recipes({
    recipeName: 'Oshav',
    productsList: ['Fruits', 'Honey']
});
dinner.recipes({
    recipeName: 'Folded cabbage leaves filled with rice',
    productsList: ['Cabbage', 'Rice', 'Salt', 'Savory']
});
dinner.recipes({
    recipeName: 'Peppers filled with beans',
    productsList: ['Beans', 'Peppers', 'Salt']
});

dinner.inviteGuests('Ivan', 'Oshav');
dinner.inviteGuests('Petar', 'Folded cabbage leaves filled with rice');
dinner.inviteGuests('Georgi', 'Peppers filled with beans');

console.log(dinner.showAttendance());


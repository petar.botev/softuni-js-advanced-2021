function solve(){

   document.querySelector('.btn.create').addEventListener('click', function (e){
      e.preventDefault();

      let authorElement = document.getElementById('creator');
      let authorInput = authorElement.value;
      authorElement.value = '';

      let titleElement = document.getElementById('title');
      let titleInput = titleElement.value;
      titleElement.value = '';
      
      let categoryElement = document.getElementById('category');
      let categoryInput = categoryElement.value;
      categoryElement.value = '';

      let contentElement = document.getElementById('content');
      let contentInput = contentElement.value;
      contentElement.value = '';

      let newArticleElement = document.createElement('article');

      let newH1Element = document.createElement('h1');
      newH1Element.textContent = titleInput;
      newArticleElement.appendChild(newH1Element);

      let newCategoryPElement = document.createElement('p');
      let categoryStrongElement = document.createElement('strong');
      categoryStrongElement.textContent = categoryInput;
      newCategoryPElement.textContent = 'Category:';
      newCategoryPElement.appendChild(categoryStrongElement);
      newArticleElement.appendChild(newCategoryPElement);

      let newCreatorPElement = document.createElement('p');
      let creatorStrongElement = document.createElement('strong');
      creatorStrongElement.textContent = authorInput;
      newCreatorPElement.textContent = 'Creator:';
      newCreatorPElement.appendChild(creatorStrongElement);
      newArticleElement.appendChild(newCreatorPElement);

      let newTextPElement = document.createElement('p');
      newTextPElement.textContent = contentInput;
      newArticleElement.appendChild(newTextPElement);

      let newDivButtonElement = document.createElement('div');
      newDivButtonElement.setAttribute('class', 'buttons');

      let newDeleteButtonElement = document.createElement('button');
      newDeleteButtonElement.textContent = "Delete";
      newDeleteButtonElement.setAttribute('class', 'btn delete');
      newDeleteButtonElement.addEventListener('click', function (e){

         console.log(e.target.parentNode.parentNode.remove());
      });
      newDivButtonElement.appendChild(newDeleteButtonElement);

      let newArchiveButtonElement = document.createElement('button');
      newArchiveButtonElement.textContent = 'Archive';
      newArchiveButtonElement.setAttribute('class', 'btn archive');
      newArchiveButtonElement.addEventListener('click', function (e){
           
         let newLiElement = document.createElement('li');
         newLiElement.textContent = titleInput;
         let olElement = document.querySelector('.archive-section ol');
         olElement.appendChild(newLiElement);
         e.target.parentNode.parentNode.remove();

         let allLoElements = Array.from(document.querySelectorAll('ol li'));
         console.log(allLoElements);
         allLoElements.sort((a,b) => a.textContent.localeCompare(b.textContent)).forEach(x => olElement.appendChild(x));
         
      });
      newDivButtonElement.appendChild(newArchiveButtonElement);

      newArticleElement.appendChild(newH1Element);
      newArticleElement.appendChild(newCategoryPElement);
      newArticleElement.appendChild(newCreatorPElement);
      newArticleElement.appendChild(newTextPElement);
      newArticleElement.appendChild(newDivButtonElement);

      let postSectionElement = document.querySelector('.site-content section');
      postSectionElement.appendChild(newArticleElement);
   });
}

function solve() {
    let ArticleSection = document.querySelector('main section');
    let CreateButton = document.querySelector('button');
    CreateButton.addEventListener('click', (ev) => {
       ev.preventDefault();
       let NewArticle = document.createElement('article');
       let Creator = document.querySelector('#creator');
       let Title = document.querySelector('#title');
       let Category = document.querySelector('#category');
       let Content = document.querySelector('#content');
 
       //header
       let H1 = document.createElement('h1');
       H1.textContent = Title.value;
       NewArticle.appendChild(H1);
       //header
 
       //category
       let NewCattegory = document.createElement('p');
       NewCattegory.textContent = 'Category:';
       let CategoryType = document.createElement('strong');
       CategoryType.textContent = Category.value;
       NewCattegory.appendChild(CategoryType);
       NewArticle.appendChild(NewCattegory);
       //category
 
       //creator
       let NewCreater = document.createElement('p');
       NewCreater.textContent = 'Creator:';
       let CreatorName = document.createElement('strong');
       CreatorName.textContent = Creator.value;
       NewCreater.appendChild(CreatorName);
       NewArticle.appendChild(NewCreater);
       //creator
 
       //Content
       let NewContent = document.createElement('p');
       //chek for bug maybe shound be .textContent
       NewContent.textContent = Content.value;
       NewArticle.appendChild(NewContent);
       //Content
 
       //buttons
       let Div = document.createElement('div');
       Div.className = 'buttons';
       let DeleteButton = document.createElement('button');
       DeleteButton.className = "btn delete";
       DeleteButton.textContent = 'Delete';
 
       DeleteButton.addEventListener('click', () => {
          NewArticle.parentNode.removeChild(NewArticle);
       })
 
       let ArchiveButton = document.createElement('button');
       ArchiveButton.className = 'btn archive';
       ArchiveButton.textContent = 'Archive';
       ArchiveButton.addEventListener('click', () => {
 
         
          const ol = document.querySelector('ol');
          let Li = document.createElement('li');
          Li.textContent = H1.textContent;
          ol.appendChild(Li);
          
          Array.from(ol.querySelectorAll('li')).sort((a, b) =>
          a.textContent.localeCompare(b.textContent)).forEach(li => ol.appendChild(li))
          
          NewArticle.parentNode.removeChild(NewArticle);
          
       });
       Div.appendChild(DeleteButton);
       Div.appendChild(ArchiveButton);
       //buttons
       NewArticle.appendChild(Div);
       ArticleSection.appendChild(NewArticle);
       Creator.value = '';
       Title.value = '';
       Category.value = '';
       Content.value = '';
    });
 
 }
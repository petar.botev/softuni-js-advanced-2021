const { assert } = require('chai');
const numberOperations = require('../Resources');

describe('Tests …', function () {
  describe('powNumber', function () {
    it('should return correct result when calculate', function () {
      assert.equal(numberOperations.powNumber(2), 4);
    });
  });

  describe('numberChecker', function () {
    it('should return error when input is NaN', function () {
      assert.throws(() => numberOperations.numberChecker('a'));
    });
    it('should return message when input is under 100', function () {
      assert.equal(
        numberOperations.numberChecker(99),
        'The number is lower than 100!'
      );
    });
    it('should return message when input is equal or above 100', function () {
      assert.equal(
        numberOperations.numberChecker(100),
        'The number is greater or equal to 100!'
      );
      assert.equal(
        numberOperations.numberChecker(101),
        'The number is greater or equal to 100!'
      );
    });
  });

  describe('sumArrays', function () {
    it('should return array when execute', function () {
        let arr1 = [1,1,1,1];
        let arr2 = [2,2,2];

      assert.isArray(numberOperations.sumArrays(arr1,arr2));
    });

    it('should return correct array when arr1 is longer', function () {
        let arr1 = [1,1,1,1];
        let arr2 = [2,2,2];
        let resArr = [3,3,3,1];

      assert.deepEqual(numberOperations.sumArrays(arr1,arr2), resArr);
    });
  });



});

class Story {
  constructor(title, creator) {
    this.title = title;
    this.creator = creator;
    this.comments = [];
    this.likes = [];
    this.counter = 0;
  }

  get likes() {
    if (this._likes.length == 0) {
      return `${this.title} has 0 likes`;
    } else if (this._likes.length == 1) {
      return `${this._likes[0]} likes this story!`;
    } else {
      return `${this._likes[0]} and ${
        this._likes.length - 1
      } others like this story!`;
    }
  }

  set likes(value) {
    this._likes = value;
  }

  like(username) {
    if (this._likes.includes(username)) {
      throw new Error("You can't like the same story twice!");
    } else if (username == this.creator) {
      throw new Error("You can't like your own story!");
    }

    this._likes.push(username);
    return `${username} liked ${this.title}!`;
  }

  dislike(username) {
    if (!this._likes.includes(username)) {
      throw new Error("You can't dislike this story!");
    }

    let elIndex = this._likes.indexOf(username);
    this._likes.splice(elIndex, 1);
    return `${username} disliked ${this.title}`;
  }

  comment(username, content, id) {
    let commentIndex = this.comments.findIndex((x) => x.id == id);
    if (this.comments.length == 0 || commentIndex == -1) {
      this.counter++;
      this.comments.push({
        id: this.counter,
        username: username,
        content: content,
        replys: [],
      });
      return `${username} commented on ${this.title}`;
    } else {
      let comment = this.comments.find((x) => x.id == id);
      let replysCount = comment.replys.length;
      comment.replys.push({
        id: `${id}.${replysCount + 1}`,
        username: username,
        content: content,
      });
      return 'You replied successfully';
    }
  }

  toString(order) {
    
      if (order == 'asc') {
        this.comments.sort((a, b) => a.id - b.id);
        this.comments.forEach((el) => {
          el.replys.sort((a, b) => a.id - b.id);
        });
      } else if (order == 'desc') {
        this.comments.sort((a, b) => b.id - a.id);
        this.comments.forEach((el) => {
          el.replys.sort((a, b) => b.id - a.id);
        });
      } else if (order == 'username') {
        this.comments.sort((a, b) => a.username.localeCompare(b.username));
        this.comments.forEach((el) => {
          el.replys.sort((a, b) => a.username.localeCompare(b.username));
        });
      }
  
      let resultString = `Title: ${this.title}\nCreator: ${this.creator}\nLikes: ${this._likes.length}\nComments:\n`;
  
      if (this.comments.length > 0) {
        let commentsToPrint = this.comments.forEach((x) => {
          resultString += `-- ${x.id}. ${x.username}: ${x.content}\n`;
          if (x.replys.length > 0) {
            let allReplys = x.replys.forEach(
              (y, i) =>{
                if (i == x.replys.length - 1) {
                  resultString += `--- ${y.id}. ${y.username}: ${y.content}`;
                }
                else{
                  resultString += `--- ${y.id}. ${y.username}: ${y.content}\n`;
                }
              }
                
            );
          }
        });
      }
  
      return resultString;
  }
}

let art = new Story('My Story', 'Anny');
art.like('John');
console.log(art.likes);
art.dislike('John');
console.log(art.likes);
art.comment('Sammy', 'Some Content');
console.log(art.comment('Ammy', 'New Content'));
art.comment('Zane', 'Reply', 1);
art.comment('Jessy', 'Nice :)');
console.log(art.comment('SAmmy', 'Reply@', 1));
console.log();
console.log(art.toString('username'));
console.log();
art.like('Zane');
console.log(art.toString('desc'));

class Bank {
    constructor (bankName){
        this._bankName = bankName;
        this.allCustomers = []; 
        this.transactions = {};
    }

    set _bankName(value){
        this.__bankName = value;
    }
    get _bankName(){
        return this.__bankName;
    }

    newCustomer (customer){
        if (this.allCustomers.some(x => x.personalId == customer.personalId)) {
            throw new Error(`${customer.firstName} ${customer.lastName} is already our customer!`);
        }

        this.allCustomers.push(customer);

        return customer;
    }

    depositMoney (personalId, amount){
        if (!this.allCustomers.some(x => x.personalId == personalId)) {
            throw new Error('We have no customer with this ID!');
        }

        let currCustomer = this.allCustomers.find(x => x.personalId == personalId);

        if (currCustomer.hasOwnProperty('totalMoney')) {
            currCustomer['totalMoney'] += amount;
        }
        else{
            currCustomer['totalMoney'] = Number(amount);
        }

        if (this.transactions.hasOwnProperty(personalId)) {
            this.transactions[personalId].push({firstName:currCustomer.firstName, lastName:currCustomer.lastName, transactionAmount: amount, transaction: 'deposit'});
        }
        else{
            this.transactions[personalId] = [{firstName:currCustomer.firstName, lastName:currCustomer.lastName, transactionAmount: amount, transaction: 'deposit'}];
        }
        
        
        return `${currCustomer['totalMoney']}$`;
    }

    withdrawMoney (personalId, amount){
        if (!this.allCustomers.some(x => x.personalId == personalId)) {
            throw new Error('We have no customer with this ID!');
        }
        let currCustomer = this.allCustomers.find(x => x.personalId == personalId);
        if (currCustomer.totalMoney < amount) {
            throw new Error(`${currCustomer.firstName} ${currCustomer.lastName} does not have enough money to withdraw that amount!`);
        }

        currCustomer.totalMoney -= amount;
        if (this.transactions.hasOwnProperty(personalId)) {
            this.transactions[personalId].push({firstName:currCustomer.firstName, lastName:currCustomer.lastName, transactionAmount: amount, transaction: 'withdraw'});
        }
        else{
            this.transactions[personalId] = [{firstName:currCustomer.firstName, lastName:currCustomer.lastName, transactionAmount: amount, transaction: 'withdraw'}];
        }

        return `${currCustomer['totalMoney']}$`;
    }

    customerInfo (personalId){
        if (!this.allCustomers.some(x => x.personalId == personalId)) {
            throw new Error('We have no customer with this ID!');
        }

        let currCustomer = this.allCustomers.find(x => x.personalId == personalId);
        let resArr = [];

        resArr.push(`Bank name: ${this._bankName}`);
        resArr.push(`Customer name: ${currCustomer.firstName} ${currCustomer.lastName}`);
        resArr.push(`Customer ID: ${personalId}`);
        resArr.push(`Total Money: ${currCustomer.totalMoney}$`);
        resArr.push(`Transactions:\n`);
        let currTransaction = this.transactions[personalId];

        let allTransactions = [];
        for (let i = 0; i < currTransaction.length; i++) {
            allTransactions.push(`${i+1}. ${currTransaction[i].firstName} ${currTransaction[i].lastName} ${currTransaction[i].transaction == 'withdraw' ? 'withdrew' : 'made deposit of'} ${currTransaction[i].transactionAmount}$!`);
            
        }
        allTransactions.reverse();
        
        return resArr.join('\n') + allTransactions.join('\n');
    }
}

let bank = new Bank('SoftUni Bank');

console.log(bank.newCustomer({firstName: 'Svetlin', lastName: 'Nakov', personalId: 6233267}));
console.log(bank.newCustomer({firstName: 'Mihaela', lastName: 'Mileva', personalId: 4151596}));

bank.depositMoney(6233267, 250);
console.log(bank.depositMoney(6233267, 250));
bank.depositMoney(4151596,555);

console.log(bank.withdrawMoney(6233267, 125));

console.log(bank.customerInfo(6233267));



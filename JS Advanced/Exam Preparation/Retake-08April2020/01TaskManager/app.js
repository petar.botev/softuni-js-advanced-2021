function solve() {
    document.querySelector('#add').addEventListener('click', (e) => {
        e.preventDefault();

        let taskElement = document.querySelector('#task');
        let taskValue = taskElement.value;

        let textAreaElement = document.querySelector('#description');
        let textAreaValue = textAreaElement.value;

        let dateElement = document.querySelector('#date');
        let dateValue = dateElement.value;
        let regex = /^[0-9]{4}\.[0-9]{2}\.[0-9]{2}$/;

        if(taskValue == '' || textAreaValue == '' || dateValue == '' || !regex.exec(dateValue))
        {
            return;
        }
        

        let asd = document.querySelector('.orange').parentNode.nextElementSibling;
        asd.appendChild(createTask(taskValue, textAreaValue, dateValue));
        createTask(taskValue, textAreaValue, dateValue);

        taskElement.value = '';
        textAreaElement.value = '';
        dateElement.value = '';
    });

    function handlerGreenButton(e){
        let currArticle = e.target.parentNode.parentNode;
            e.target.setAttribute('class', 'red');
            e.target.textContent = 'Delete';
            e.target.addEventListener('click', handlerRedButton);
            e.target.nextElementSibling.setAttribute('class', 'orange');
            e.target.nextElementSibling.textContent = 'Finish';
            e.target.nextElementSibling.removeEventListener('click', handlerRedButton);
            e.target.nextElementSibling.addEventListener('click', handlerOrangeButton)
            document.querySelector('#in-progress').appendChild(currArticle);
    }
    function handlerOrangeButton(e){
        let currArticle = e.target.parentNode.parentNode;
        e.target.parentNode.remove();
       document.querySelector('.wrapper').lastElementChild.lastElementChild.appendChild(currArticle);
    }
    function handlerRedButton(e){
        e.target.parentNode.parentNode.remove();
    }

    function createTask(task, text, date){
        let newArticle = document.createElement('article');

        let newH3Element = document.createElement('h3');
        newH3Element.textContent = task;
        newArticle.appendChild(newH3Element);

        let newDescriptionPElement = document.createElement('p');
        newDescriptionPElement.textContent = `Description: ${text}`;
        newArticle.appendChild(newDescriptionPElement);

        let newDatePElement = document.createElement('p');
        newDatePElement.textContent = `Due Date: ${date}`;
        newArticle.appendChild(newDatePElement);

        let newDivForButtons = document.createElement('div');
        newDivForButtons.setAttribute('class', 'flex');
        newArticle.appendChild(newDivForButtons);

        let newGreenButton = document.createElement('button');
        newGreenButton.setAttribute('class', 'green');
        newGreenButton.textContent = 'Start';
        newGreenButton.addEventListener('click',handlerGreenButton);
        newDivForButtons.appendChild(newGreenButton);

        let newRedButton = document.createElement('button');
        newRedButton.setAttribute('class', 'red');
        newRedButton.textContent = 'Delete';
        newRedButton.addEventListener('click',handlerRedButton);
        newDivForButtons.appendChild(newRedButton);

        return newArticle;
    }
}
class Restaurant {
    constructor(budget){
        this.budgetMoney = budget;
        this.menu = {};
        this.stockProducts = {};
        this.history = [];
    }

    get budgetMoney(){
        return this._budgetMoney;
    }

    set budgetMoney(value){
        this._budgetMoney = value;
    }

    loadProducts(products) {
        let productName = '';
        let productQuantity = '';
        let productTotalPrice = '';
        let allActions = [];

        for (let i = 0; i < products.length; i++) {
            let args = products[i].split(' ');

            productName = args[0];
            productQuantity = Number(args[1]);
            productTotalPrice = Number(args[2]);

            if (productTotalPrice <= this._budgetMoney) {
                if (this.stockProducts.hasOwnProperty(productName)) {
                    this.stockProducts[productName] += productQuantity;
                }
                else{
                    this.stockProducts[productName] = productQuantity;
                }

                this._budgetMoney -= productTotalPrice;
                this.history.push(`Successfully loaded ${productQuantity} ${productName}`);
                allActions.push(`Successfully loaded ${productQuantity} ${productName}`);
            }
            else{
                this.history.push(`There was not enough money to load ${productQuantity} ${productName}`);
                allActions.push(`There was not enough money to load ${productQuantity} ${productName}`);
            }
        }

        return allActions.join('\n');
    }

    addToMenu(meal, neededProducts, price){

        if(this.menu.hasOwnProperty(meal)){
            return `The ${meal} is already in the our menu, try something different.`;
        }
        else{
            this.menu[meal] = {neededProducts, price};
           if(Object.entries(this.menu).length == 1){
               return `Great idea! Now with the ${meal} we have 1 meal in the menu, other ideas?`;
           }
           else{
               return `Great idea! Now with the ${meal} we have ${Object.entries(this.menu).length} meals in the menu, other ideas?`;
           }
        }
    }

    showTheMenu(){
        let menuContent = [];
        if(Object.entries(this.menu).length == 0){
            return "Our menu is not ready yet, please come later...";
        }
        else{
            for (const key in this.menu) {
                menuContent.push(`${key} - $ ${this.menu[key].price}`);
            }
        }

        return menuContent.join('\n');
    }

    makeTheOrder(meal){
        let allProd = Object.keys(this.stockProducts);
        
        let mealProducts = this.menu[meal].neededProducts.map(x => x.split(' ')[0]);
        let hasAllProducts = true;
        if(this.menu.hasOwnProperty(meal) == false){
            return `There is not ${meal} yet in our menu, do you want to order something else?`;
        }
        else{
            for (let i = 0; i < mealProducts.length; i++) {
                if (allProd.includes(mealProducts[i]) == false) {
                    hasAllProducts = false;
                    break;
                }
            }

            if (hasAllProducts == false) {
                return `For the time being, we cannot complete your order (${meal}), we are very sorry...`;
            }
            else{
                
                let prods = this.menu[meal].neededProducts;
                for (let i = 0; i < prods.length; i++) {
                    let args = prods[i].split(' ');
                    let name = args[0];
                    let count = Number(args[1]);
                    this.stockProducts[name] -= count;
                }

                this._budgetMoney += this.menu[meal].price;
                return `Your order (${meal}) will be completed in the next 30 minutes and will cost you ${this.menu[meal].price}.`;
            }
        }
    }
}

let kitchen = new Restaurant(1000);

kitchen.loadProducts(['Yogurt 30 3', 'Honey 50 4', 'Strawberries 20 10', 'Banana 5 1']);

console.log(kitchen.loadProducts(['Banana 10 5', 'Banana 20 10', 'Strawberries 50 30', 'Yogurt 10 10', 'Yogurt 500 1500', 'Honey 5 50']));

kitchen.loadProducts(['Yogurt 30 3', 'Honey 50 4', 'Strawberries 20 10', 'Bananana 5 1']);

kitchen.addToMenu('frozenYogurt', ['Yogurt 1', 'Honey 1', 'Banana 1', 'Strawberries 10'], 9.99);

console.log(kitchen.addToMenu('frozenYogurt', ['Yogurt 1', 'Honey 1', 'Banana 1', 'Strawberries 10'], 9.99));

console.log(kitchen.addToMenu('Pizza', ['Flour 0.5', 'Oil 0.2', 'Yeast 0.5', 'Salt 0.1', 'Sugar 0.1', 'Tomato sauce 0.5', 'Pepperoni 1', 'Cheese 1.5'], 15.55));

console.log(kitchen.addToMenu('Pizza', ['Flour 0.5', 'Oil 0.2', 'Yeast 0.5', 'Salt 0.1', 'Sugar 0.1', 'Tomato sauce 0.5', 'Pepperoni 1', 'Cheese 1.5'], 15.55));

kitchen.addToMenu('frozenYogurt', ['Yogurt 1', 'Honey 1', 'Banana 1', 'Strawberries 10'], 9.99);

console.log(kitchen.showTheMenu());

console.log(kitchen.makeTheOrder('frozenYogurt'));




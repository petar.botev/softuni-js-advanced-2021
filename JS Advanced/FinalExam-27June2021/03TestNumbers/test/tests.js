const { assert } = require('chai');
const testNumbers = require('../testNumbers');

describe("Tests …", function() {
    describe("sumNumbers", function() {
        it("check if parameters are numbers", function() {
            assert.equal(testNumbers.sumNumbers(1, '1'), undefined);
            assert.equal(testNumbers.sumNumbers('1', '1'), undefined);
            assert.equal(testNumbers.sumNumbers('1', 1), undefined);
        });

        it("numbers can be positive and negative", function() {
            assert.equal(testNumbers.sumNumbers(1, -1), 0);
            assert.equal(testNumbers.sumNumbers(1, 1), 2);
            assert.equal(testNumbers.sumNumbers(-1, -1), - 2);
        });

        it("the function returns the sum of the given numbers, rounded to second number after decimal point", function() {
            assert.equal(testNumbers.sumNumbers(1.111, 1.111), 2.22);
        });
     });

     describe("numberChecker", function() {
        it("should throw error when is not parsed to number", function() {
            assert.throw(() => testNumbers.numberChecker('a'), 'The input is not a number!');
        });

        it("should return number is even when is even", function() {
            assert.equal(testNumbers.numberChecker('2'), 'The number is even!');
        });

        it("should return number is odd when is odd", function() {
            assert.equal(testNumbers.numberChecker('1'), 'The number is odd!');
        });
     });

     describe("averageSumArray", function() {
        it("should return average number of array when executed", function() {
            assert.equal(testNumbers.averageSumArray([2,2]), 2);
        });
     });
});

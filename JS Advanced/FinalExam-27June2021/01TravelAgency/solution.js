window.addEventListener('load', solution);

function solution() {
    document.querySelector('#submitBTN').addEventListener('click', function (e){
        e.preventDefault();
        let nameElement = document.querySelector('#fname');
        let nameValue = nameElement.value;
        let nameValueConst = nameValue;

        let emailElement = document.querySelector('#email');
        let emailValue = emailElement.value;
        let emailValueConst = emailValue;

        let phoneElement = document.querySelector('#phone');
        let phoneValue = phoneElement.value;
        let phoneValueConst = phoneValue;

        let addressElement = document.querySelector('#address');
        let addresValue = addressElement.value;
        let addresValueConst = addresValue;

        let postalElement = document.querySelector('#code');
        let postalValue = postalElement.value;
        let postalValueConst = postalValue;

        if(nameValue == '' || emailValue == ''){
          return;
        }
        let theElement = createInfoElement(nameValue, emailValue, phoneValue, addresValue, postalValue);
        document.querySelector('.preview').appendChild(theElement);

        nameElement.value = '';
        emailElement.value = '';
        phoneElement.value = '';
        addressElement.value = '';
        postalElement.value = '';

        document.querySelector('#submitBTN').setAttribute('disabled', true);
        document.querySelector('#editBTN').removeAttribute('disabled');
        document.querySelector('#continueBTN').removeAttribute('disabled');

        document.querySelector('#editBTN').addEventListener('click', () => {
          document.querySelector('#fname').value = nameValueConst;
          document.querySelector('#email').value = emailValueConst;
          document.querySelector('#phone').value = phoneValueConst;
          document.querySelector('#address').value = addresValueConst;
          document.querySelector('#code').value = postalValueConst;

          document.querySelector('#infoPreview').innerHTML = '';

          document.querySelector('#editBTN').setAttribute('disabled', true);
        document.querySelector('#continueBTN').setAttribute('disabled', true);
        document.querySelector('#submitBTN').removeAttribute('disabled');
      });

    });

    document.querySelector('#continueBTN').addEventListener('click', () => {
        document.querySelector('#block').innerHTML = '';
        let newH3Tag = document.createElement('h3');
        newH3Tag.textContent = 'Thank you for your reservation!';
        document.querySelector('#block').appendChild(newH3Tag);
    });

    function createInfoElement(nameValue, emailValue, phoneValue, addresValue, postalValue){
        let newLiName = document.createElement('li'); 
        newLiName.textContent = `Full Name: ${nameValue}`;
        let newLiMail = document.createElement('li');
        newLiMail.textContent = `Email: ${emailValue}`;
        let newLiPhone = document.createElement('li');
        newLiPhone.textContent = `Phone Number: ${phoneValue}`;
        let newLiAddress = document.createElement('li');
        newLiAddress.textContent = `Address: ${addresValue}`;
        let newLiPostal = document.createElement('li');
        newLiPostal.textContent = `Postal Code: ${postalValue}`;

        let infoPreview = document.querySelector('#infoPreview');

        infoPreview.appendChild(newLiName);
        infoPreview.appendChild(newLiMail);
        infoPreview.appendChild(newLiPhone);
        infoPreview.appendChild(newLiAddress);
        infoPreview.appendChild(newLiPostal);

        return infoPreview;
    }
}

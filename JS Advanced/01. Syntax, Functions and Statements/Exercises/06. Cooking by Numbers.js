function cooking(number, arg1, arg2, arg3, arg4, arg5) {

    let params = [number, arg1, arg2, arg3, arg4, arg5];
    let value = Number(params[0]);
  
    for (let i = 1; i < params.length; i++) {
      switch (params[i]) {
        case 'chop':
          value /= 2;
          break;
        case 'dice':
          value = Math.sqrt(value);
          break;
        case 'spice':
          value += 1;
          break;
        case 'bake':
          value *= 3;
          break;
        case 'fillet':
          value -= value / 5;
          break;
        default:
          break;
      }
      console.log(value);
    }
  }
  
  cooking('9', 'dice', 'spice', 'chop', 'bake', 'fillet');
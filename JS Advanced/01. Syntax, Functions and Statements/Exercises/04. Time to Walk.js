function walkTime(steps, pace, speed){

    let distance = steps * pace;
    let speedMs = speed * 1000/3600;

    let restTime = Math.floor(distance / 500) * 60;

    let timeToUniInSec = (distance / speedMs) + restTime;

    let hours = Math.floor(timeToUniInSec / 3600);
    timeToUniInSec -= hours * 3600; 
    let minutes = Math.floor(timeToUniInSec / 60);
    timeToUniInSec -= minutes * 60;
    let seconds = Math.round(timeToUniInSec);

    console.log(`${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`);
 }

 walkTime(1, 0.60, 5);
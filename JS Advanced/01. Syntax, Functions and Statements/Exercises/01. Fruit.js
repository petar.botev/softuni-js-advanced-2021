function fruitPriceCalculator(fruit, weight, price){
    let weightInKilos = (weight / 1000).toFixed(2);
    let moneyNeeded = (weightInKilos * price).toFixed(2);

    console.log(`I need $${moneyNeeded} to buy ${weightInKilos} kilograms ${fruit}.`)
}

fruitPriceCalculator('apple', 1563, 2.35);
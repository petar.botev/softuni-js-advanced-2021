function checkNumber (input){
   
    let inputString = input.toString();
    let first = Number(inputString[0]);
    let areSame = true;
    let sum = 0;

    for (let i = 0; i < inputString.length; i++) {
        
        sum += Number(inputString[i]);
        if(first !== Number(inputString[i])){
            areSame = false;
        }
    }

    return `${areSame}\n${sum}`;
}

console.log(checkNumber(1234));
function radar(number, area) {
    let speedLimit;
    switch (area) {
      case 'motorway':
        speedLimit = 130;
        break;
      case 'interstate':
        speedLimit = 90;
        break;
      case 'city':
        speedLimit = 50;
        break;
      case 'residential':
        speedLimit = 20;
        break;
      default:
        speedLimit = 0;
        break;
    }
  
    let overSpeed = number - speedLimit;
  
    if (overSpeed > 0) {
      if (overSpeed < 21) {
        return `The speed is ${Math.abs(
          overSpeed
        )} km/h faster than the allowed speed of ${speedLimit} - speeding`;
      } else if (overSpeed < 41) {
        return `The speed is ${Math.abs(
          overSpeed
        )} km/h faster than the allowed speed of ${speedLimit} - excessive speeding`;
      } else {
        return `The speed is ${Math.abs(
          overSpeed
        )} km/h faster than the allowed speed of ${speedLimit} - reckless driving`;
      }
    } else {
      return `Driving ${number} km/h in a ${speedLimit} zone`;
    }
  }
  
  console.log(radar(40, 'city'));
  console.log(radar(21, 'residential'));
  
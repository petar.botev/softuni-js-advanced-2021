function solution(n){
    return function add5(num){
        return num + n;
    }
}
let add5Func = solution(5);
console.log(add5Func(2));
console.log(add5Func(3));
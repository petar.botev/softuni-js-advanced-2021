function solve(arr){
    let args = '';
    let resultArr = [];

    for (let i = 0; i < arr.length; i++) {
       args = arr[i].split(' ');
        if (args[0] == 'add') {
            add(args[1]);
        }
        else if(args[0] == 'remove'){
            remove(args[1]);
        }
        else if(args[0] == 'print'){
            print();
        }
        
    }

    function add(str){
        resultArr.push(args[1]);
        return str;
    }
    function remove(str){
        resultArr = resultArr.filter(x => x != str);
    }

    function print(){
        console.log(resultArr.join(','));
    }
  
}

solve(['add hello', 'add again', 'remove hello', 'add again', 'print']);

function solution(){
    let myString = '';

    return {
        append,
        removeStart,
        removeEnd,
        print
    };

    function append(newStr){
        myString += newStr;
    }
    
    function removeStart(num){
        myString = myString.slice(num);
    }
    
    function removeEnd(num){
        myString = myString.slice(0, -num);
    }
    
    function print(){
        console.log(myString);
    }
}

    
let firstZeroTest = solution();

firstZeroTest.append('hello');
firstZeroTest.append('again');
firstZeroTest.removeStart(3);
firstZeroTest.removeEnd(4);
firstZeroTest.print();

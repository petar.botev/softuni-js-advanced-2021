function solution() {
  let stock = { protein: 0, carbohydrate: 0, fat: 0, flavour: 0 };
  let apple = { protein: 0, carbohydrate: 1, fat: 0, flavour: 2 };
  let lemonade = { protein: 0, carbohydrate: 10, fat: 0, flavour: 20 };
  let burger = { protein: 0, carbohydrate: 5, fat: 7, flavour: 3 };
  let eggs = { protein: 5, carbohydrate: 0, fat: 1, flavour: 1 };
  let turkey = { protein: 10, carbohydrate: 10, fat: 10, flavour: 10 };

  function inner(str) {
    if (str === 'report') {
      return Object.keys(stock)
        .map((x) => `${x}=${stock[x]}`)
        .join(' ');
    } else {
      let supplementArgs = str.split(' ');
        let currStock = Object.assign({}, stock);
       
      if (supplementArgs[0] === 'restock') {
        let suppl = supplementArgs[1];
        stock[suppl] += Number(supplementArgs[2]);
        return 'Success';
      } else if (supplementArgs[0] === 'prepare') {
        switch (supplementArgs[1]) {
          case 'apple':
            for (const key in stock) {
              if (stock[key] - apple[key] < 0) {
                console.log(`Error: not enough ${key} in stock`);
                break;
              }
              else{
                  currStock[key] -= apple[key];
              } 

            }
            console.log('Success');
            stock = currStock;
            break;
          case 'lemonade':
            prepare(stock, lemonade, currStock);
            break;
          case 'burger':
            for (const key in stock) {
              if (stock[key] - burger[key] >= 0) {
                console.log('Success');
              } else {
                console.log(`Error: not enough ${key} in stock`);
              }
            }
            break;
          case 'eggs':
            for (const key in stock) {
              if (stock[key] - eggs[key] >= 0) {
                console.log('Success');
              } else {
                console.log(`Error: not enough ${key} in stock`);
              }
            }
            break;
          case 'turkey':
            for (const key in stock) {
              if (stock[key] - turkey[key] >= 0) {
                console.log('Success');
              } else {
                console.log(`Error: not enough ${key} in stock`);
              }
            }
            break;

          default:
            break;
        }
      }
      else if(supplementArgs[0] === 'report'){
        return report(stock);
      }
    }
  }

  return inner;
}

function prepare(stock, product, currStock){
    for (const key in stock) {
        if (stock[key] - product[key] < 0) {
          
          stock = currStock;
          return `Error: not enough ${key} in stock`;
        }
        else{
            currStock[key] -= product[key];
        } 
      }
      return 'Success';
}

function report(obj){
    let res = [];
    for (const key in obj) {
        let str = `${key}=${obj[key]}`;
        res.push(str);
    }

    return res.join(' ');
}

let manager = solution();
console.log(manager('restock flavour 50')); // Success
console.log(manager('prepare lemonade 4')); // Error: not enough carbohydrate in stock
console.log(manager('restock carbohydrate 10'));
console.log(manager('restock flavour 10'));
console.log(manager('prepare apple 1'));
console.log(manager('restock fat 10'));
console.log(manager('prepare burger 1'));
console.log(manager('report'));

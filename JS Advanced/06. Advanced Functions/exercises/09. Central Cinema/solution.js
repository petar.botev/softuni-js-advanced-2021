function solve() {
  document
    .querySelector('#container button')
    .addEventListener('click', function (e) {
      e.preventDefault();

      let inputLi = document.createElement('li');
      let inputSpan = document.createElement('span');
      let inputStrong = document.createElement('strong');
      let newDiv = document.createElement('div');
      let divStrong = document.createElement('strong');
      let divInput = document.createElement('input');
      let divButton = document.createElement('button');

      let inputs = Array.from(document.querySelectorAll('#container input'));
      let [name, hall, ticket] = inputs;
      let movieName = name.value;
      let price = ticket.value;
      price = parseInt(price).toFixed(2);
      if (inputs.every((x) => x.value != '') && price !== 'NaN') {
        inputSpan.textContent = name.value;

        inputStrong.textContent = `Hall: ${hall.value}`;

        divStrong.textContent = price;

        divInput.placeholder = 'Tickets Sold';

        divButton.innerText = 'Archive';
        divButton.addEventListener('click', archiveOnClick);

        inputLi.appendChild(inputSpan);
        inputLi.appendChild(inputStrong);

        newDiv.appendChild(divStrong);
        newDiv.appendChild(divInput);
        newDiv.appendChild(divButton);

        inputLi.appendChild(newDiv);

        console.log(newDiv);

        document.querySelector('#movies ul').appendChild(inputLi);

        inputs.forEach((x) => (x.value = ''));
      }
    });

  function archiveOnClick(e) {
    let inputValueElement = e.target.previousSibling;
    let inputValue = parseInt(inputValueElement.value);

    let price = Number(inputValueElement.previousSibling.textContent);

    inputLi = document.createElement('li');
    inputSpan = document.createElement('span');
    inputStrong = document.createElement('strong');
    divButton = document.createElement('button');

    if (!isNaN(inputValue)) {
      inputValue *= price;
      inputSpan.textContent =
        e.target.parentElement.parentElement.querySelector('span').textContent;
      inputStrong.textContent = `Total amount: ${inputValue.toFixed(2)}`;
      divButton.textContent = 'Delete';
      divButton.addEventListener('click', deleteOnClick);

      inputLi.appendChild(inputSpan);
      inputLi.appendChild(inputStrong);
      inputLi.appendChild(divButton);

      document.querySelector('#archive ul').appendChild(inputLi);
      document.querySelector('#movies ul li').remove();
    }
  }

  function deleteOnClick(e) {
    e.target.parentElement.remove();
  }
}

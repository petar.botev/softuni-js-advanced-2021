function solution() {

    let product = { protein: 0, carbohydrate: 0,fat: 0, flavour: 0,}
    let obj = {
        apple: { carbohydrate: 1, flavour: 2 },
        lemonade: { carbohydrate: 10, flavour: 20 },
        burger: { carbohydrate: 5, fat: 7, flavour: 3 },
        eggs: { protein: 5, fat: 1, flavour: 1 },
        turkey: { protein: 10, carbohydrate: 10, fat: 10, flavour: 10, }
    }
    return function(str){
        let [command, el, col] = str.split(' ')
      
        col = Number(col)
        if (command === 'restock') {
            product[el] += col;
            return 'Success';
        }
       if (command === 'prepare') {

            if (obj[el]) {
                let bool = true;
                for (let key in obj[el]) {

                    if ((product[key] - (Number(obj[el][key]) * col)) < 0) {
                        bool = false;
                        return `Error: not enough ${key} in stock`;

                    }
                }
                if (bool) {
                    for (let key in obj[el]) {
                        product[key] -= Number((obj[el][key]) * col)
                    }
                    return 'Success'
                }
            }
        }
        if (command === 'report') {
            let string = ''
            for (const key in product) {
                string += `${key}=${product[key]} `
            }
			string=string.slice(0,-1)
            return string
        }

    }
}
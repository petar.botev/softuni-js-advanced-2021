function solve(...arr){
    let types = [];
    let numberOfType = {};

    arr.forEach(x => {
        let type = typeof x;
        types.push(`${type}: ${x}`);

        numberOfType[type] === undefined ? numberOfType[type] = 1 : ++numberOfType[type];
    });

    let sorted = Object.keys(numberOfType)
    .sort((a,b) => numberOfType[b] - numberOfType[a])
    .map(x => `${x} = ${numberOfType[x]}`);

    types.forEach(x => console.log(x));
    sorted.forEach(x => console.log(x));
    //console.log(types.join('\n') + '\n' + sorted.join('\n'));
    //return types.join('\n') + '\n' + sorted.join('\n');
}

solve('cat','asd', 42, function () { console.log('Hello world!'); });
function getArticleGenerator(articles) {
  
    function inner(){
        if(articles.length > 0){
        let art = document.createElement('article');
        art.textContent = articles[0];
        articles.shift();
            document.querySelector('#content').appendChild(art);
        }
        
    }
    
    return inner;
}

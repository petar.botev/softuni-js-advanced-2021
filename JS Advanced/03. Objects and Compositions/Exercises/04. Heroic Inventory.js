function execute(input) {
  let arr = [];

  for (let i = 0; i < input.length; i++) {
      
      let [name, level, items] = input[i].split(' / ');

      level = Number(level);

      if (items != undefined) {
          items = items.split(', ');
      }
      else{
          items = [];
      }

      arr.push({name, level, items});
  }

  return JSON.stringify(arr);
}

console.log(
  execute([
    'Isacc / 25',
    'Derek / 12 / BarrelVest, DestructionSword',
    'Hes / 1 / Desolator, Sentinel, Antara',
  ])
);

function execute(arr){
    let newObj = {};
    for (let i = 0; i < arr.length - 1; i+=2) {
        newObj[arr[i]] = Number(arr[i+1]);
    }

    return newObj;
}

console.log(execute(['Yoghurt', '48', 'Rise', '138', 'Apple', '52']));
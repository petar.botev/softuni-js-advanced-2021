function execute(arr) {
  let objects = {};
  
  for (let i = 0; i < arr.length; i++) {
    let [name, product, price] = arr[i].split(' | ');
    price = Number(price);

    if (!objects.hasOwnProperty(product)) {
      let newObj = { name: name, price: price };
      objects[product] = newObj;
    } else {
      let currObj = '';
      for (const key in objects) {
        if (key == product) {
          currObj = objects[key];
        }
      }

      let priceOfProduct = currObj.price;

      if (priceOfProduct > price) {
        currObj.price = price;
        currObj.name = name;
      }
    }
  }

  let result = '';
  for (const obj in objects) {
    result += `${obj} -> ${objects[obj].price} (${objects[obj].name})\n`;
  }

  return result;
}

console.log(
  execute([
    'Sample Town | Sample Product | 1000',
    'Sample Town | Orange | 2',
    'Sample Town | Peach | 1',
    'Sofia | Orange | 3',
    'Sofia | Peach | 2',
    'New York | Sample Product | 1000.1',
    'New York | Burger | 10',
  ])
);

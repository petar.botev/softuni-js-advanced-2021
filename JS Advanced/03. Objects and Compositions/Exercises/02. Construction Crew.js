function execute(obj) {

    if (obj.dizziness == true) {
        let water = (obj.weight * 0.1) * obj.experience;
        obj.levelOfHydrated += water;
        obj.dizziness = false;
    }

    return obj;
}

console.log(
  execute({
    weight: 120,
    experience: 20,
    levelOfHydrated: 200,
    dizziness: true,
  })
);

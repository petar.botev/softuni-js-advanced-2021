function execute(arr) {
  let objects = [];
  let minPrice = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < arr.length; i++) {
    let [name, product, price] = arr[i].split(' | ');

    price = Number(price);
    if (objects.some((x) => x.name == name)) {
      if (objects.some((x) => x.product == product)) {
        let currObj = objects.find((x) => x.product == product);
      currObj.price += price;
      }
      else{
        objects.push({ name, product, price });
      }
            
    } else {
      if (objects.some((x) => x.product == product)) {
        let currOb = objects.find((x) => x.product == product);
        let objIndex = objects.indexOf(currOb);
          if(Number(currOb.price) > price){
            objects.splice(objIndex, 1);
            objects.splice(objIndex, 0, { name, product, price });
            minPrice = currOb.price;
          }
      } else {
        objects.push({ name, product, price });
      }
    }
  }
  let result = '';
  for (const iterator of objects) {
    result += `${iterator.product} -> ${iterator.price} (${iterator.name})\n`;
    
  }

  return result;
}

console.log(
  execute([
    'Sample Town | Sample Product | 1000',
    'Sample Town | Orange | 199999',
    'Sample Town | Peach | 1',
    'Sofia | Orange | 99999',
    'Sofia | Peach | 2',
    'New York | Sample Product | 1000.1',
    'New York | Burger | 10',
  ])
);

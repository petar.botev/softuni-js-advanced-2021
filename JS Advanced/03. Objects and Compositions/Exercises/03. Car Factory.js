function execute(obj) {

    let newObj = {};
    newObj.model = obj.model;

    if (obj.power <= 90) {
      newObj.engine = {power: 90, volume: 1800}
    }
    else if (obj.power <= 120 && obj.power > 90){
      newObj.engine = {power: 120, volume: 2400}
    }
    else if (obj.power <= 200 && obj.power > 120){
      newObj.engine = {power: 200, volume: 3500}
    }

    newObj.carriage = {type: obj.carriage, color: obj.color};

    if (obj.wheelsize % 2 == 0) {
      obj.wheelsize -= 1;
    }
    newObj.weels = [obj.wheelsize, obj.wheelsize, obj.wheelsize, obj.wheelsize];

    return newObj;
}

console.log(
  execute({ model: 'Opel Vectra',
  power: 110,
  color: 'grey',
  carriage: 'coupe',
  wheelsize: 17 }
)
);

function execute(library, orders) {
    let objColl = [];

    for (const el of orders) {
      
        let props = Object.values(el)[0];
        let obj = {name:props.name};
        let partsArr = Object.values(el)[1];

        for (const iterator of partsArr) {
          if(iterator == 'print'){
            obj.print = library[iterator];
          }
          else if (iterator == 'scan'){
            obj.scan = library[iterator];
          }
          else if (iterator == 'play'){
            obj.play = library[iterator];
          }
        }

        objColl.push(obj);
    }

    return objColl;
}

console.log(
  execute(
    {
      print: function () {
        console.log(`${this.name} is printing a page`);
      },
      scan: function () {
        console.log(`${this.name} is scanning a document`);
      },
      play: function (artist, track) {
        console.log(`${this.name} is playing '${track}' by ${artist}`);
      },
    },
    [
      {
        template: { name: 'ACME Printer' },
        parts: ['print'],
      },
      {
        template: { name: 'Initech Scanner' },
        parts: ['scan'],
      },
      {
        template: { name: 'ComTron Copier' },
        parts: ['scan', 'print'],
      },
      {
        template: { name: 'BoomBox Stereo' },
        parts: ['play'],
      },
    ]
  )
);

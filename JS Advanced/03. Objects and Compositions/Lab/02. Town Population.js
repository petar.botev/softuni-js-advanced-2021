function execute(arr) {
  let resultArr = [];
  for (let el of arr) {
    let splittedEl = el.split(' <-> ');
    let name = splittedEl[0];
    let population = Number(splittedEl[1]);

    if (Object.values(resultArr).some((x) => x.name == name)) {
      let objK = Object.values(resultArr);
      if (objK.some((x) => x.name == name)) {
        let city = resultArr.find((x) => x.name == name);
        city.population += population;
      }
    } else {
      let cityObj = { name: name, population: population };
      resultArr.push(cityObj);
    }
  }

  let mapped = resultArr.map((x) => `${x.name} : ${x.population}`);
  return mapped.join('\n');
}

console.log(
  execute([
    'Istanbul <-> 100000',
    'Honk Kong <-> 2100004',
    'Jerusalem <-> 2352344',
    'Mexico City <-> 23401925',
    'Istanbul <-> 1000',
  ])
);

function execute(name, population, treasury) {
    let city = {
        name:name, 
        population:population, 
        treasury:treasury,
        taxRate:10,
        collectTaxes: function(){this.treasury += Math.floor(this.population * this.taxRate)},
        applyGrowth: function(percentage){this.population += Math.floor((percentage / 100) * population)},
        applyRecession: function(percentage){this.treasury -= Math.floor((percentage / 100) * this.treasury)}
    };

    return city;
}

console.log(execute('Tortuga', 7000, 15000));
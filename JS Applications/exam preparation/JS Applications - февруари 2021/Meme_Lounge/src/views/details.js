import { html } from "../../node_modules/lit-html/lit-html.js";
import { deleteRecord, getItemById } from "../api/data.js";

const detailsTemplate = (meme,onClick) => html`
       <section id="meme-details">
            <h1>Meme Title: ${meme.title}

            </h1>
            <div class="meme-details">
                <div class="meme-img">
                    <img alt="meme-alt" src=${meme.imageUrl}>
                </div>
                <div class="meme-description">
                    <h2>Meme Description</h2>
                    <p>
                        ${meme.description}
                    </p>

                    <!-- Buttons Edit/Delete should be displayed only for creator of this meme  -->
                    ${sessionStorage.getItem('userId') == meme._ownerId 
                    ? html`
                    <a class="button warning" href="/edit/${meme._id}">Edit</a>
                    <button class="button danger" @click=${onClick}>Delete</button>` 
                    : ''}
                    
                </div>
            </div>
        </section>
`;

export async function detailsPage(ctx){
    let id = ctx.params.id;
    let meme = await getItemById(id);
    ctx.render(detailsTemplate(meme,onClick));

    async function onClick(){
        let res = confirm('Do you want to delete the meme?');
        
        if(res){
            await deleteRecord(ctx.params.id);
            
            ctx.page.redirect('/allMemes');
        }
    }
}
import { html } from '../../node_modules/lit-html/lit-html.js';
import { editRecord, getItemById } from '../api/data.js';
import { notify } from '../notification.js';

const editTemplate = (meme, onSubmit) => html`
  <section id="edit-meme">
    <form id="edit-form" @submit=${onSubmit}>
      <h1>Edit Meme</h1>
      <div class="container">
        <label for="title">Title</label>
        <input
          id="title"
          type="text"
          placeholder="Enter Title"
          name="title"
          value=${meme.title}
        />
        <label for="description">Description</label>
        <textarea
          id="description"
          placeholder="Enter Description"
          name="description"
        >
                            ${meme.description}
                        </textarea
        >
        <label for="imageUrl">Image Url</label>
        <input
          id="imageUrl"
          type="text"
          placeholder="Enter Meme ImageUrl"
          name="imageUrl"
          value=${meme.imageUrl}
        />
        <input type="submit" class="registerbtn button" value="Edit Meme" />
      </div>
    </form>
  </section>
`;

export async function editPage(ctx) {
  let id = ctx.params.id;
  let meme = await getItemById(id);

  async function onSubmit(e) {
    e.preventDefault();
    let formData = new FormData(e.target);

    let title = formData.get('title').trim();
    let description = formData.get('description').trim();
    let imageUrl = formData.get('imageUrl').trim();
    try {
      if (title == '' || description == '' || imageUrl == '') {
        throw new Error('Fill all the fields!');
      }
    } catch (error) {
      notify(error.message);
      return;
    }
    let obj = {
      title: title,
      description: description,
      imageUrl: imageUrl,
    };
    await editRecord(ctx.params.id, obj);
    ctx.page.redirect(`/details/${ctx.params.id}`);
  }

  ctx.render(editTemplate(meme, onSubmit));
}

import { html } from "../../node_modules/lit-html/lit-html.js";
import { getMymemes, getUser } from "../api/data.js";

const profileTemplate = (memes, user) => html`
        <section id="user-profile-page" class="user-profile">
            <article class="user-info">
                <img id="user-avatar-url" alt="user-profile" src="/images/female.png">
                <div class="user-content">
                    <p>Username: ${user.username}</p>
                    <p>Email: ${user.email}</p>
                    <p>My memes count: ${user.memeCount}</p>
                </div>
            </article>
            <h1 id="user-listings-title">User Memes</h1>
            <div class="user-meme-listings">
                <!-- Display : All created memes by this user (If any) --> 
               ${memes.length != 0 ? memes.map(x => singleMeme(x)) : html`<p class="no-memes">No memes in database.</p>`}

                <!-- Display : If user doesn't have own memes  --> 
                
            </div>
        </section>
`;

const singleMeme = (meme) => html`
                <div class="user-meme">
                    <p class="user-meme-title">${meme.title}</p>
                    <img class="userProfileImage" alt="meme-img" src=${meme.imageUrl}>
                    <a class="button" href="/details/${meme._id}">Details</a>
                </div>
`;

export async function profilePage(ctx){
    let memes = await getMymemes();
    let username = sessionStorage.username;
    let email = sessionStorage.email;
    let memeCount = memes.length;

    console.log(window.location.href);

    let userInfo = {
        username: username,
        email: email,
        memeCount: memeCount
    }
    ctx.render(profileTemplate(memes, userInfo));

}
import { render } from "../node_modules/lit-html/lit-html.js";
import page from "../node_modules/page/page.mjs";

import { logout } from "./api/data.js";
import { homePage } from "./views/homePage.js";
import { allMemesPage } from "./views/allMemes.js";
import { loginPage } from "./views/login.js";
import { registerPage } from "./views/register.js";
import { createMemePage } from "./views/createMeme.js";
import { detailsPage } from "./views/details.js";
import { editPage } from "./views/edit.js";
import { profilePage } from "./views/profile.js";

const main = document.querySelector('main');
console.log(main);

page('/', decorateContext, homePage);
page('/homePage', decorateContext, homePage);
page('/createMeme', decorateContext, createMemePage);
page('/allMemes', decorateContext, allMemesPage);
page('/details/:id', decorateContext, detailsPage);
page('/edit/:id', decorateContext, editPage);
page('/login', decorateContext, loginPage);
page('/profile', decorateContext, profilePage);
page('/register', decorateContext, registerPage);

page.start();
setUserNav();

document.getElementById('logoutBtn').addEventListener('click', async () => {
    await logout();
    setUserNav();
    page.redirect('/');
});

function decorateContext(ctx, next){
    ctx.render = (content) => render(content, main);
    ctx.setUserNav = setUserNav;
    next();
}

function setUserNav(){
    const userId = sessionStorage.getItem('userId');
    if(userId != null){
        let email = sessionStorage.getItem('email');

        document.querySelector('.user').style.display = 'inline-block';
        document.querySelector('.guest').style.display = 'none';

        let welcomeMessage = document.querySelector('.profile span');
        welcomeMessage.textContent = `Welcome, ${email}`;
        console.log(welcomeMessage);

    } else {
        document.querySelector('.user').style.display = 'none';
        document.querySelector('.guest').style.display = 'inline-block';
    }
}
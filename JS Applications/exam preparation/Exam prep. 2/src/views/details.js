import { html } from "../../node_modules/lit-html/lit-html.js";
import { deleteRecord, getItemById } from "../api/data.js";


const detailsTemplate = (item,onClick) => html`
        <section id="listing-details">
            <h1>Details</h1>
            <div class="details-info">
                <img src=${item.imageUrl}>
                <hr>
                <ul class="listing-props">
                    <li><span>Brand:</span>${item.brand}</li>
                    <li><span>Model:</span>${item.model}</li>
                    <li><span>Year:</span>${item.year}</li>
                    <li><span>Price:</span>${item.price}$</li>
                </ul>

                <p class="description-para">${item.description}</p>
                ${sessionStorage.userId == item._ownerId ? html`
                <div class="listings-buttons">
                    <a href="/edit/${item._id}" class="button-list">Edit</a>
                    <a href="javascript:void(0)" class="button-list" @click=${onClick}>Delete</a>
                </div>
                ` : ''}
               
            </div>
        </section>
`;

export async function detailsPage(ctx){
    let id = ctx.params.id;
    let item = await getItemById(id);
    ctx.render(detailsTemplate(item,onClick));
    console.log(item);

    async function onClick(e){
        let res = confirm('Do you want to delete the meme?');
        
        if(res){
            await deleteRecord(ctx.params.id);
            
            ctx.page.redirect('/getAllCars');
        }
    }
}
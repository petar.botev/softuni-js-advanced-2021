import { html } from "../../node_modules/lit-html/lit-html.js";
import { searchCar } from "../api/data.js";

const searchTemplate = (onClick,searchedCars,query) => html`
        <section id="search-cars">
            <h1>Filter by year</h1>

            <div class="container">
                <input id="search-input" type="text" name="search" placeholder="Enter desired production year" .value=${query || ''}>
                <button class="button-list" @click=${onClick}>Search</button>
            </div>

            <h2>Results:</h2>
            <div class="listings">

                <!-- Display all records -->
                ${searchedCars.length > 0 ? searchedCars.map(x => singleCar(x)) 
                    : html`<p class="no-cars"> No results.</p>`}

                <!-- Display if there are no matches -->
                
            </div>
        </section>
`;

const singleCar = (car) => html`
        <div class="listing">
                    <div class="preview">
                        <img src=${car.imageUrl}>
                    </div>
                    <h2>${car.brand} ${car.model}</h2>
                    <div class="info">
                        <div class="data-info">
                            <h3>Year: ${car.year}</h3>
                            <h3>Price: ${car.price} $</h3>
                        </div>
                        <div class="data-buttons">
                            <a href="/details/${car._id}" class="button-carDetails">Details</a>
                        </div>
                    </div>
                </div>
`;

export async function searchPage(ctx){
    let query = Number(ctx.querystring.split('=')[1]);
    console.log(query);
    let cars = Number.isNaN(query) ? [] : await searchCar(query);
    ctx.render(searchTemplate(onClick,cars,query));

    async function onClick(){
        let inputElement = document.getElementById('search-input');
        let inputValue = Number(inputElement.value);
        
        if(Number.isNaN(inputValue) == false){
            ctx.page.redirect('/search?query=' + inputValue);
        }
        //searchedCars = await searchCar(inputValue);
    }
}
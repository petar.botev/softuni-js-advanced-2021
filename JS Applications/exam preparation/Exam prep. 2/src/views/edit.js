import { html } from '../../node_modules/lit-html/lit-html.js';
import { editRecord, getItemById } from '../api/data.js';

const editTemplate = (car, onSubmit) => html`
        <section id="edit-listing">
            <div class="container">

                <form id="edit-form" @submit=${onSubmit}>
                    <h1>Edit Car Listing</h1>
                    <p>Please fill in this form to edit an listing.</p>
                    <hr>

                    <p>Car Brand</p>
                    <input type="text" placeholder="Enter Car Brand" name="brand" value=${car.brand}>

                    <p>Car Model</p>
                    <input type="text" placeholder="Enter Car Model" name="model" value=${car.model}>

                    <p>Description</p>
                    <input type="text" placeholder="Enter Description" name="description" value=${car.description}>

                    <p>Car Year</p>
                    <input type="number" placeholder="Enter Car Year" name="year" value=${car.year}>

                    <p>Car Image</p>
                    <input type="text" placeholder="Enter Car Image" name="imageUrl" value=${car.imageUrl}>

                    <p>Car Price</p>
                    <input type="number" placeholder="Enter Car Price" name="price" value=${car.price}>

                    <hr>
                    <input type="submit" class="registerbtn" value="Edit Listing">
                </form>
            </div>
        </section>
`;

export async function editPage(ctx) {
  let id = ctx.params.id;
  let car = await getItemById(id);

  async function onSubmit(e) {
    e.preventDefault();
    let formData = new FormData(e.target);

    let brand = formData.get('brand').trim();
    let model = formData.get('model').trim();
    let description = formData.get('description').trim();
    let year = Number(formData.get('year').trim());
    let imageUrl = formData.get('imageUrl').trim();
    let price = Number(formData.get('price').trim());

    try {
      if (brand == '' || model == '' || description == '' || year == '' || imageUrl == '' || price == '') {
        throw new Error('Fill all the fields!');
      }
    } catch (error) {
      alert(error.message);
      return;
    }
    let obj = {
      brand: brand,
      model: model,
      description: description,
      year: year,
      imageUrl: imageUrl,
      price: price
    };
    await editRecord(ctx.params.id, obj);
    ctx.page.redirect(`/details/${ctx.params.id}`);
  }

  ctx.render(editTemplate(car, onSubmit));
}



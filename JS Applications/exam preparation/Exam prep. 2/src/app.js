
import { render } from "../node_modules/lit-html/lit-html.js";
import page from "../node_modules/page/page.mjs";
import { logout } from "./api/api.js";
import { createPage } from "./views/create.js";
import { detailsPage } from "./views/details.js";
import { editPage } from "./views/edit.js";
import { getAllCarsPage } from "./views/getAllCars.js";

import { homePage } from "./views/home.js";
import { loginPage } from "./views/login.js";
import { myCarsPage } from "./views/myCars.js";
import { registerPage } from "./views/register.js";
import { searchPage } from "./views/search.js";

const main = document.querySelector('#site-content');

page('/', decorateContext, homePage);
page('/home', decorateContext, homePage);
page('/create', decorateContext, createPage);
page('/getAllCars', decorateContext, getAllCarsPage);
page('/details/:id', decorateContext, detailsPage);
page('/edit/:id', decorateContext, editPage);
page('/login', decorateContext, loginPage);
page('/myCars', decorateContext, myCarsPage);
page('/register', decorateContext, registerPage);
page('/search', decorateContext, searchPage);

page.start();
setUserNav();
console.log(document.querySelector('#profile :nth-child(4)'));
document.querySelector('#profile :nth-child(4)').addEventListener('click', async () => {
    await logout();
    setUserNav();
    page.redirect('/home');
});

function decorateContext(ctx, next){
    ctx.render = (content) => render(content, main);
    ctx.setUserNav = setUserNav;
    next();
}

function setUserNav(){
    const userId = sessionStorage.getItem('userId');
    if(userId != null){
        let username = sessionStorage.getItem('username');

        document.querySelector('#profile').style.display = 'inline-block';
        document.querySelector('#guest').style.display = 'none';

        let welcomeMessage = document.querySelector('#profile > a');
        welcomeMessage.textContent = `Welcome ${username}`;
        

    } else {
        document.querySelector('#profile').style.display = 'none';
        document.querySelector('#guest').style.display = 'inline-block';
    }
}
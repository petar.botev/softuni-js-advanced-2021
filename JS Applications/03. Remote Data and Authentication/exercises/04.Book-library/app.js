function createLibrary(){
    let allBooksUrl = 'http://localhost:3030/jsonstore/collections/books';

    //load books
    document.getElementById('loadBooks').addEventListener('click', () => {
        
        fetch(allBooksUrl)
            .then(r => r.json())
            .then(d => {
                document.getElementById('books').innerHTML = '';
                let bookValues = Object.entries(d);
                bookValues.forEach(([x, y]) => {
                    let dataElement = createBookDataTableRows(y, x);
                    document.getElementById('books').append(dataElement);
                });
            });
    });

    //Create new book 
    document.querySelector('.createForm').addEventListener('submit', (e) => {
        e.preventDefault();
        
        let postObject = createPostObject(e.currentTarget, null);

    });

    //Delete book
    function deleteRow(e){
        let url = `${allBooksUrl}/${e.target.getAttribute('data-id')}`;
        if(confirm('Do you want to delete the book?')){
            fetch(url, {
                method: 'DELETE',
            });
               
            e.target.parentElement.parentElement.remove();
            document.querySelector('.editForm').style.display = 'none';
            document.querySelector('.createForm').style.display = 'block';
        }
    }

    //Edit book
    function editBook(e){
        let id = e.target.getAttribute('data-id');
        document.querySelector('.editForm button').setAttribute('data-id', `${id}`);
        let url = `${allBooksUrl}/${id}`;
        console.log(e.target);
        document.querySelector('.editForm').style.display = 'block';
        document.querySelector('.createForm').style.display = 'none';
        
        fetch(url)
            .then(r => r.json())
            .then(d => {
                document.querySelector('.editForm input[name="title"]').value = d.title;
                document.querySelector('.editForm input[name="author"]').value = d.author;
            });
    }

    //save button
    document.querySelector('.editForm button').addEventListener('click', async (el) => {
        el.preventDefault();
        
        let dataId = el.target.getAttribute('data-id');
        let postObject = await createPostObject(el.currentTarget.parentElement, dataId);
        let elementToChange = document.querySelector(`.container button[data-id="${dataId}"]`).parentElement.parentElement;
        
        elementToChange.querySelector(':nth-child(1)').textContent = postObject.title;
        elementToChange.querySelector(':nth-child(2)').textContent = postObject.author;

        document.querySelector('.editForm').style.display = 'none';
        document.querySelector('.createForm').style.display = 'block';
    });

    //Cancel button
    document.querySelector('#cancelBtn').addEventListener('click', () => {
        document.querySelector('.editForm').style.display = 'none';
        document.querySelector('.createForm').style.display = 'block';
    });

    
    async function createPostObject(el, id){
        let formData = new FormData(el);

        let currUrl = '';
        
       let formDataArray = Array.from(formData.values());
       
        if(formDataArray.includes('')){
            document.querySelector('.notification').textContent = 'All fields are obligatory!';
            return;
        }

        document.querySelector('.notification').textContent = '';
        document.querySelector('input[name="title"]').value = '';
        document.querySelector('input[name="author"]').value = '';

        let title = formData.get('title');
        let author = formData.get('author');

        let postObject = {
            title,
            author
        }
        if(id == null){
            currUrl = allBooksUrl;
            let responce = await fetch(currUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(postObject)
            });

            let booksObj = await responce.json();
            postObject.id = booksObj._id;

            let obj = {
                title: postObject.title,
                author: postObject.author
            }
            
           document.getElementById('books').append(createBookDataTableRows(obj, postObject.id));
           return postObject;
        }
        else{
            currUrl = `${allBooksUrl}/${id}`;
            fetch(currUrl, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(postObject)
                
            });
            return postObject;
        }
    }

    function createBookDataTableRows(info, id){
        let trBooks = document.createElement('tr');
    
        let tdBookTitle = document.createElement('td');
        tdBookTitle.textContent = info.title;
        let tdBookAuthor = document.createElement('td');
        tdBookAuthor.textContent = info.author;
        let tdBookAction = document.createElement('td');
        let editButton = document.createElement('button');
        editButton.textContent = 'Edit';
        editButton.setAttribute('data-id', id);
        editButton.addEventListener('click', editBook);
        let deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.setAttribute('data-id', id);
        deleteButton.addEventListener('click', deleteRow);
        tdBookAction.append(editButton);
        tdBookAction.append(deleteButton);
    
        trBooks.append(tdBookTitle);
        trBooks.append(tdBookAuthor);
        trBooks.append(tdBookAction);
    
        return trBooks;
    }
}

createLibrary();
function messenger(){
    document.getElementById('refresh').addEventListener('click', () => {
        let getAllMessagesUrl = 'http://localhost:3030/jsonstore/messenger';

        fetch(getAllMessagesUrl)
            .then(r => r.json())
            .then(d => {
                console.log(d);
                let data = Object.values(d);
                console.log(data);

                let messageArray = [];
                data.forEach(x => {
                    messageArray.push(`${x.author}: ${x.content}`);
                });

                document.getElementById('messages').textContent = messageArray.join('\n');
            });
    });
   

    document.getElementById('submit').addEventListener('click', () => {
        let getAllMessagesUrl = 'http://localhost:3030/jsonstore/messenger';

        let author = document.getElementById('author').value;
        let content = document.getElementById('content').value;

        document.getElementById('author').value = '';
        document.getElementById('content').value = '';

        let data = {
            author,
            content
        }

        fetch(getAllMessagesUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    });
}
messenger();
export function update(e){
    e.preventDefault();
    let addPanel = e.target.parentElement;
    let id = addPanel.dataset.id;

    let postUrl = `http://localhost:3030/data/catches/${id}`;
    let angler = addPanel.querySelector('input[name="angler"]');
    let weight = addPanel.querySelector('input[name="weight"]');
    let species = addPanel.querySelector('input[name="species"]');
    let location = addPanel.querySelector('input[name="location"]');
    let bait = addPanel.querySelector('input[name="bait"]');
    let captureTime = addPanel.querySelector('input[name="captureTime"]');

    let catchToAdd = {
        angler: angler.value,
        weight: Number(weight.value),
        species: species.value,
        location: location.value,
        bait: bait.value,
        captureTime: Number(captureTime.value)
    }

    fetch(postUrl, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "X-Authorization": localStorage.token
        },
        body: JSON.stringify(catchToAdd)
    });
}
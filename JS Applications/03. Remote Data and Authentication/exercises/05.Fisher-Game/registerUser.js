import setup from "./setupElements.js";
import { loginView } from "./app.js";
import { homePage } from "./homePage.js";
export function registerUser(e) {
  e.preventDefault();
  let registerUrl = 'http://localhost:3030/users/register';
  let registerData = new FormData(e.target.parentElement);

  let registerEmail = registerData.get('email');
  let registerPassword = registerData.get('password');
  let registerRePassword = registerData.get('rePass');

  if (registerPassword != registerRePassword) {
    document.querySelector('.notification').textContent =
      'Passwords does not match!';
    return;
  }

  let registerObj = {
    email: registerEmail,
    password: registerPassword,
  };

  fetch(registerUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(registerObj),
  })
    .then((r) => r.json())
    .then((d) => {
      console.log(d);
      if (d.email == undefined) {
        setup.welcomeMessage.textContent = 'guest';
      } else {
        setup.welcomeMessage.textContent = d.email;
        homePage();
        loginView();
      }

      localStorage.setItem('token', d.accessToken);
      localStorage.setItem('welcomeName', d.email);
      localStorage.setItem('id', d._id);
    });
}

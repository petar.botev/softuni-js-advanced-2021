import { ce } from "./createDOMElements.js";
import { deleteFunc } from "./deleteCatch.js";
import { update } from "./updateCatch.js";

function createCatchesElement(currCatch){
    let anglerLabel = ce('label', undefined, 'Angler');
    let anglerInlut = ce('input', {type: 'text', name: 'angler'}, currCatch.angler);
    anglerInlut.setAttribute('value', anglerInlut.textContent);
    let firstHr = ce('hr');

    let weightLabel = ce('label', undefined, 'Weight');
    let weightInlut = ce('input', {type: 'number', name: 'weight'}, currCatch.weight);
    weightInlut.setAttribute('value', weightInlut.textContent);
    let secondHr = ce('hr');

    let speciesLabel = ce('label', undefined, 'Species');
    let speciesInlut = ce('input', {type: 'text', name: 'species'}, currCatch.species);
    speciesInlut.setAttribute('value', speciesInlut.textContent);
    let thirdHr = ce('hr');

    let locationLabel = ce('label', undefined, 'Location');
    let locationInlut = ce('input', {type: 'text', name: 'location'}, currCatch.location);
    locationInlut.setAttribute('value', locationInlut.textContent);
    let forthHr = ce('hr');

    let baitLabel = ce('label', undefined, 'Bait');
    let baitInlut = ce('input', {type: 'text', name: 'bait'}, currCatch.bait);
    baitInlut.setAttribute('value', baitInlut.textContent);
    let fifthHr = ce('hr');

    let captureTimeLabel = ce('label', undefined, 'Capture Time');
    let captureTimeInlut = ce('input', {type: 'number', name: 'captureTime'}, currCatch.captureTime);
    captureTimeInlut.setAttribute('value', captureTimeInlut.textContent);
    let sixthHr = ce('hr');

    let updateButton = '';
    let deleteButton = '';
    if(localStorage.id != currCatch._ownerId) {
        updateButton = ce('button', {disabled: true, class: 'update'}, 'Update');
        updateButton.setAttribute('class', 'update');
        deleteButton = ce('button', {disabled: true, class: 'delete'}, 'Delete');
        deleteButton.setAttribute('class', 'delete');
    }
    else{
        updateButton = ce('button', {class: 'update'}, 'Update');
        updateButton.setAttribute('class', 'update');
        deleteButton = ce('button', {class: 'delete'}, 'Delete');
        deleteButton.setAttribute('class', 'delete');
    }

    updateButton.addEventListener('click', update);
    deleteButton.addEventListener('click', deleteFunc);
    
     let catchDiv = ce('div', {class:'catch'}, anglerLabel, anglerInlut, firstHr, weightLabel, weightInlut, secondHr, speciesLabel, speciesInlut, thirdHr, locationLabel, locationInlut, forthHr, baitLabel, baitInlut, fifthHr, captureTimeLabel, captureTimeInlut, sixthHr, updateButton, deleteButton);
     catchDiv.setAttribute('class', 'catch');
     catchDiv.setAttribute('data-id', `${currCatch._id}`);
     catchDiv.setAttribute('data-ownerId', `${currCatch._ownerId}`);

    return catchDiv;
}

function newCatch(e){
    e.preventDefault();
    let addPanel = e.target.parentElement;

    let postUrl = 'http://localhost:3030/data/catches';
    let angler = addPanel.querySelector('input[name="angler"]');
    let weight = addPanel.querySelector('input[name="weight"]');
    let species = addPanel.querySelector('input[name="species"]');
    let location = addPanel.querySelector('input[name="location"]');
    let bait = addPanel.querySelector('input[name="bait"]');
    let captureTime = addPanel.querySelector('input[name="captureTime"]');

    let catchToAdd = {
        angler: angler.value,
        weight: Number(weight.value),
        species: species.value,
        location: location.value,
        bait: bait.value,
        captureTime: Number(captureTime.value)
    }

    fetch(postUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-Authorization": localStorage.token
        },
        body: JSON.stringify(catchToAdd)
    });
}

let catches = {
    createCatchesElement,
    newCatch
}

export default catches;
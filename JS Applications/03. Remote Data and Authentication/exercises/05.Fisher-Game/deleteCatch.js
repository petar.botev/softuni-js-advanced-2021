export function deleteFunc(e){
    let elementToDelete = e.target.parentElement;
    let id = elementToDelete.dataset.id;

    let deleteUrl = `http://localhost:3030/data/catches/${id}`;

    fetch(deleteUrl, {
        method: "DELETE",
        headers: {
            "X-Authorization": localStorage.token
        }
    });

    elementToDelete.remove();
}
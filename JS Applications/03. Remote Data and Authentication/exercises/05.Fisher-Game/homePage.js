import setup from "./setupElements.js";

export function homePage() {
    setup.loginViewElement.style.display = 'none';
    setup.registerViewElement.style.display = 'none';
    setup.asideElement.style.display = 'inline-block';
    setup.containerElement.style.display = 'none';
    setup.fieldsetElement.style.display = 'none';
}
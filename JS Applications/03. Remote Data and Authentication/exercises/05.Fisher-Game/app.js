import catches from "./catches.js";
import { homePage } from "./homePage.js";
import { loginUser } from "./loginUser.js";
import { registerUser } from "./registerUser.js";
import setup from "./setupElements.js";
//import { update } from "./updateCatch.js";

setup.fieldsetElement.style.display = 'none';

if(localStorage.getItem('token') != null){
    console.log(localStorage.welcomeName);
    setup.welcomeMessage.textContent = localStorage.welcomeName;
    loginView();
}

//create catch
document.querySelector('fieldset button').addEventListener('click', catches.newCatch);

//register button
setup.headerRegister.addEventListener('click', () => {

    setup.registerViewElement.style.display = 'inline-block';
    setup.loginViewElement.style.display = 'none';
    setup.asideElement.style.display = 'none';
    setup.containerElement.style.display = 'none';
    document.getElementById('main').style.display = 'none';
});

//login button
setup.headerLogin.addEventListener('click', () => {

    setup.loginViewElement.style.display = 'inline-block';
    setup.registerViewElement.style.display = 'none';
    setup.asideElement.style.display = 'none';
    setup.containerElement.style.display = 'none';
    document.getElementById('main').style.display = 'none';
});

//home button
document.getElementById('home').addEventListener('click', () => {
    homePage();
});

//register user
document.getElementById('register-query').addEventListener('click', registerUser);

//login user
document.getElementById('login-query').addEventListener('click', loginUser);

//logout user
document.querySelector('#logout').addEventListener('click', async () => {
    let logoutUrl = 'http://localhost:3030/users/logout';

    await fetch(logoutUrl, {
        method: "GET",
        headers:{
            "X-Authorization": localStorage.token
        }
    });

     localStorage.clear();
     setup.headerLogin.style.display = 'inline-block';
     setup.headerRegister.style.display = 'inline-block';
     setup.headerLogout.style.display = 'none';
     document.querySelector('.add').setAttribute('disabled', true);
     setup.welcomeMessage.textContent = 'guest';
     setup.fieldsetElement.setAttribute('style', 'display: none');

});

//load button
document.querySelector('.load').addEventListener('click', () => {
    let loadUrl = 'http://localhost:3030/data/catches';

    fetch(loadUrl)
        .then(r => r.json())
        .then(d => {
            console.log(d);
            document.getElementById('catches').innerHTML = '';
            document.getElementById('main').style.display = 'inline-block';
            d.forEach(x => {
                
                document.getElementById('catches').append(catches.createCatchesElement(x));
            });


            //document.querySelector('.update').addEventListener('click', update);
        });
});

//login view
export function loginView() {
    document.getElementById('user').style.display = 'inline-block';
    setup.headerLogin.style.display = 'none';
    setup.headerRegister.style.display = 'none';
    setup.headerLogout.style.display = 'inline-block';
    document.querySelector('.add').removeAttribute('disabled');
    //document.querySelector('fieldset#main').style.display = 'inline-block';
}
 
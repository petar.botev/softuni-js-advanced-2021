
    document.getElementById('views').style.display = 'inherit';

    let registerViewElement = document.getElementById('register-view');
    registerViewElement.style.display = 'none';
    
    let loginViewElement = document.getElementById('login-view');
    loginViewElement.style.display = 'none';
    
    let asideElement = document.querySelector('aside');
    let fieldsetElement = document.querySelector('fieldset#main');

    let containerElement = document.querySelector('#container');
    containerElement.style.display = 'none';
    let welcomeMessage = document.querySelector('p.email span');
    welcomeMessage.textContent = 'guest';
    
    let headerRegister = document.getElementById('header-register');
    let headerLogin = document.getElementById('header-login');
    let headerLogout = document.getElementById('logout');

    let setup = {
        registerViewElement,
        loginViewElement,
        asideElement,
        containerElement,
        welcomeMessage,
        headerRegister,
        headerLogin,
        headerLogout,
        fieldsetElement
    }

    export default setup;


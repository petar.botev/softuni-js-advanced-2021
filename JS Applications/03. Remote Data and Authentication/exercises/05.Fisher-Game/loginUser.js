import { loginView } from "./app.js";
import { homePage } from "./homePage.js";
import setup from "./setupElements.js";

//let welcomeMessage = document.querySelector('p.email span');

export function loginUser(e) {
  e.preventDefault();
  let loginUrl = 'http://localhost:3030/users/login';
  console.log(e.target.parentElement);
  let loginData = new FormData(e.target.parentElement);

  let loginEmail = loginData.get('email');
  let loginPassword = loginData.get('password');

  fetch(loginUrl, {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: loginEmail,
      password: loginPassword,
    }),
  })
    .then((r) => r.json())
    .then((d) => {
     if(d.code > 400){
       throw new Error(d.message);
     }
      console.log(d.email);
      if (d.email == undefined) {
        setup.welcomeMessage.textContent = 'guest';
      } else {
        setup.welcomeMessage.textContent = d.email;

        homePage();
        loginView();
      }

      localStorage.setItem('token', d.accessToken);
      localStorage.setItem('welcomeName', d.email);
      localStorage.setItem('id', d._id);
    })
    .catch(err => {
      alert(err.message);
    });
}

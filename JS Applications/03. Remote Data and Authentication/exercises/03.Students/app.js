function studentsTable(){
    let studentsUrl = 'http://localhost:3030/jsonstore/collections/students';

    document.querySelector('input[name="facultyNumber"]').addEventListener('click', () => {
        
    });

    fetch(studentsUrl)
        .then(r => r.json())
        .then(d => {
            let dataValues = Object.values(d);
            dataValues.forEach(x => {
                let tableData = createStudentsTableElement(x);
                document.querySelector('#results tbody').append(tableData);
            });
        });

        document.querySelector('form').addEventListener('submit', (e) =>{
            e.preventDefault();
            let formObj = new FormData(e.currentTarget);
            console.log();
            if(Array.from(formObj.values()).includes('')){
                alert('All fields are obligatory!');
                return;
            }
            
            let firstName = formObj.get('firstName');
            let lastName = formObj.get('lastName');
            let facultyNumber = formObj.get('facultyNumber');
            let grade = formObj.get('grade');
            console.log(parseInt(facultyNumber));

            if(facultyNumber.length !== 11 || parseInt(facultyNumber) === NaN){
                //document.querySelector('input[name="facultyNumber"]').setAttribute('class', 'errorBox');
                alert('Faculty number should be a number and should be 11 digits long!');
                
                return;
            }

            if(grade < 2 || grade > 6) {
                alert('The grade must be between 2 and 6!');
            }

            let postObject = {
                firstName,
                lastName,
                facultyNumber,
                grade
            }

            fetch(studentsUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(postObject)
            });
           
            document.querySelector('#results tbody').append(createStudentsTableElement(postObject));
        });
}

function createStudentsTableElement(info){
    let trElement = document.createElement('tr');

    let tdFirstName = document.createElement('td');
    tdFirstName.textContent = info.firstName;
    let tdLastName = document.createElement('td');
    tdLastName.textContent = info.lastName;
    let tdFacNumber = document.createElement('td');
    tdFacNumber.textContent = info.facultyNumber;
    let tdGrade = document.createElement('td');
    tdGrade.textContent = info.grade;

    trElement.append(tdFirstName);
    trElement.append(tdLastName);
    trElement.append(tdFacNumber);
    trElement.append(tdGrade);

    return trElement;
}
studentsTable();
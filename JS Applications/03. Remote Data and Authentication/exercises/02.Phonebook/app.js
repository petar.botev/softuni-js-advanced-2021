function createPhonebook(){
    document.getElementById('btnLoad').addEventListener('click', () => {
        let phonebookUrl = 'http://localhost:3030/jsonstore/phonebook';

        fetch(phonebookUrl)
            .then(r => r.json())
            .then(d => {
                document.getElementById('phonebook').innerHTML = '';
                console.log(d);
                let objectArray = Object.values(d);
                objectArray.forEach(x => {
                    let phonebookRow = createPhonebookElement(x);
                    document.getElementById('phonebook').append(phonebookRow);
                })
            });
    });

    document.getElementById('btnCreate').addEventListener('click', () => {
        let phonebookUrl = 'http://localhost:3030/jsonstore/phonebook';

        let person = document.getElementById('person').value;
        let phone = document.getElementById('phone').value;
        document.querySelector('.notification').textContent = '';

        if(person == '' || phone == ''){
           let pErr = document.querySelector('.notification');
           pErr.textContent = 'All fields are obligatory!';

           return ;
        }

        document.getElementById('person').value = '';
        document.getElementById('phone').value = '';

        let dataObject = {
            person: person,
            phone: phone
        };

        fetch(phonebookUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataObject)
        });

        let phonebookRow = createPhonebookElement(dataObject);
        document.getElementById('phonebook').append(phonebookRow);
    });
}

function deleteRow(e){
    let result = window.confirm('Do you want to delete this contact?');
    if(result){
        if(confirm){
            let id = e.target.getAttribute('data-id');
            console.log(id);
            let deleteUrl = `http://localhost:3030/jsonstore/phonebook/${id}`;
            fetch(deleteUrl, {
                method:'DELETE'
            });
        
            e.target.parentElement.parentElement.remove();
        }
    }
}

function createPhonebookElement(dataObject){
    let trElement = document.createElement('tr');
    let tdNameElement = document.createElement('td');
    tdNameElement.textContent = `${dataObject.person}`;
    let tdNumberElement = document.createElement('td');
    tdNumberElement.textContent = `${dataObject.phone}`;
    let tdButton = document.createElement('td');
    let deleteButtonElement = document.createElement('button');
    deleteButtonElement.addEventListener('click', deleteRow);
    deleteButtonElement.textContent = 'Delete';
    deleteButtonElement.setAttribute('data-id', `${dataObject._id}`);

    tdButton.append(deleteButtonElement);

    trElement.append(tdNameElement);
    trElement.append(tdNumberElement);
    trElement.append(tdButton);

    return trElement;
}
createPhonebook();

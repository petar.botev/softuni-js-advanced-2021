//I don't have load and catch functions

let greetingElement = document.querySelector('p.email');
greetingElement.textContent += 'guest';
let homeElement = document.getElementById('home');
homeElement.addEventListener('click', addHomeView)

function addHomeView() {
    loginView.style.display = 'none'
    asideView.style.display = 'inline-block';
    registerView.style.display = 'none'
}

let asideView = document.querySelector('#home-view aside');
let main = document.querySelector('body main');

let logoutButton = document.getElementById('logout');
logoutButton.style.display = 'none'

let registerView = document.getElementById('register-view');
registerView.style.display = 'none'

let loginView = document.getElementById('login-view');
loginView.style.display = 'none'

let loginElement = document.getElementById('login');
loginElement.addEventListener('click', addLoginView);

let registerElement = document.getElementById('register');
registerElement.addEventListener('click', addRegisterView);

function addLoginView() {
    loginView.style.display = 'inline-block'
    asideView.style.display = 'none';
    registerView.style.display = 'none'
}

function addRegisterView() {
    registerView.style.display = 'inline-block'
    asideView.style.display = 'none';
    loginView.style.display = 'none'
}

let registerButton = document.querySelector('#register button');
let loginButton = document.querySelector('#login button');

registerButton.addEventListener('click', register);
loginButton.addEventListener('click', login);
logoutButton.addEventListener('click', logout);

async function register(e) {
    e.preventDefault();
    let password = document.querySelector('#register input[name="password"]');
    let repeatedPassword = document.querySelector('#register input[name="rePass"]');
    let email = document.querySelector('#register input[name="email"]');

    if (password.value !== repeatedPassword.value) {
        alert(`Password don't match!`);
        return
    }

    if (email.value == '' || password.value == '' || repeatedPassword.value == '') {
        alert('You must fill all!');
        return;
    }

    let newUser = {
        email: email.value,
        password: password.value,
    }

    let url = `http://localhost:3030/users/register`;

    let registerResponse = await fetch(url, {
        method: 'POST',
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify(newUser)
    });
    let registerData = await registerResponse.json();
    let accessToken = registerData.accessToken
    localStorage.setItem('token', accessToken);
    loginView.style.display = 'none'
    asideView.style.display = 'inline-block';
    registerView.style.display = 'none';
    loginElement.style.display = 'inline-block';
    registerElement.style.display = 'inline-block';
    password.value = '';
    email.value = '';
    repeatedPassword.value = '';
    
};

async function login(e) {
    e.preventDefault();
    let password = document.querySelector('#login input[name="password"]');
    let email = document.querySelector('#login input[name="email"]');

    if (email.value == '' || password.value == '') {
        alert('You must fill all!');
        return;
    }

    let loginUser = {
        email: email.value,
        password: password.value,
    }

    let url = `http://localhost:3030/users/login`;

    let loginResponse = await fetch(url, {
        method: 'POST',
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify(loginUser)
    });
    let loginData = await loginResponse.json();
    let accessToken = loginData.accessToken
    localStorage.setItem('token', accessToken);
    greetingElement.textContent = 'Welcome, ';
    greetingElement.textContent += email.value;
    loginView.style.display = 'none'
    asideView.style.display = 'inline-block';
    registerView.style.display = 'none';
    loginElement.style.display = 'none';
    registerElement.style.display = 'none';
    logoutButton.style.display = 'inline-block';
    password.value = '';
    email.value = '';
};

//Logout function doesn't work
async function logout() {
    let url = `http://localhost:3030/users/logout`;

    let logoutResponse = await fetch(url);
    console.log(logoutResponse.status);
     
}

let loadButton = document.querySelector('button.load');
loadButton.addEventListener('click', load);

async function load(e) {
e.preventDefault();
let url = `http://localhost:3030/data/catches`;
let loadResponse = await fetch(url);
let loadData = await loadResponse.json();
console.log(loadData);
}
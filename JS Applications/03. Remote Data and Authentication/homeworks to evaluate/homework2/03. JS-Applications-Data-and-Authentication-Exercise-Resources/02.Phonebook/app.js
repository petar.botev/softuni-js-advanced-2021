function attachEvents() {
    let ulPhonebookList = document.getElementById('phonebook');
    let loadButton = document.getElementById('btnLoad');
    let personInput = document.getElementById('person');
    let phoneInput = document.getElementById('phone');
    let createButton = document.getElementById('btnCreate');

    loadButton.addEventListener('click', loadPhonebook);
    createButton.addEventListener('click', createRecord);

    async function loadPhonebook() {
        try {
            ulPhonebookList.querySelectorAll('li').forEach(li => li.remove())
            let url = 'http://localhost:3030/jsonstore/phonebook';
            let response = await fetch(url);
            let data = await response.json();
            Object.entries(data).forEach(element => {
                let key = element[0];
                let liElement = document.createElement('li');
                liElement.textContent = `${element[1].person}: ${element[1].phone}`
                let deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                liElement.append(deleteButton)
                ulPhonebookList.append(liElement);
                deleteButton.addEventListener('click', (e) => {
                    fetch(`http://localhost:3030/jsonstore/phonebook/${key}`, {
                        method: 'DELETE',
                    })
                    e.target.parentElement.remove();
                })
            });
        } catch (error) {
            console.error(error);
        }
    }

    async function createRecord() {
        try {
            if (personInput.value !== '' && phoneInput.value !== '') {
                let record = {
                    person: personInput.value,
                    phone: phoneInput.value
                }
                let url = 'http://localhost:3030/jsonstore/phonebook';
                let response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    body: JSON.stringify(record)
                });
                personInput.value = '';
                phoneInput.value = '';
                loadPhonebook();
            }
        } catch (error) {
            console.error(error);
        }
    }
}

attachEvents();
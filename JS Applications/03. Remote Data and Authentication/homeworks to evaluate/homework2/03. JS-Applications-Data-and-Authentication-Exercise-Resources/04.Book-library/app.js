function manageBooks() {
    let loadButton = document.getElementById('loadBooks');
    let bookTitle = document.getElementById('title');
    let bookAuthor = document.getElementById('author');
    let submitButton = document.getElementById('submit');
    let tbody = document.getElementById('table-body');
    let formTitle = document.getElementById('form-title');

    clearAllBooks();

    loadButton.addEventListener('click', (e) => {
        e.preventDefault();
        clearAllBooks();
        loadBooks();
    });

    submitButton.addEventListener('click', (e) => {
        if (e.target.textContent === 'Submit') {
            e.preventDefault();
            addBook();
        } else {
            e.preventDefault();
            saveBook();
        }
    })

    async function loadBooks() {
        try {
            let url = `http://localhost:3030/jsonstore/collections/books`;
            let response = await fetch(url);
            let allBooks = await response.json();
            Object.keys(allBooks).forEach(key => {
                let tr = document.createElement('tr');

                let titleTd = document.createElement('td');
                titleTd.textContent = allBooks[key].title;

                let authorTd = document.createElement('td');
                authorTd.textContent = allBooks[key].author;

                let buttonsTd = document.createElement('td');

                let editButton = document.createElement('button');
                editButton.textContent = 'Edit';
                editButton.addEventListener('click', editBook)

                let deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                deleteButton.addEventListener('click', deleteBook);

                buttonsTd.append(editButton, deleteButton);
                tr.append(titleTd, authorTd, buttonsTd);
                tr.dataset.id = key
                tbody.append(tr);
            })
        } catch (error) {
            return alert('Error');
        }
    };

    async function addBook() {

        if (bookTitle.value === '' || bookAuthor.value === '') {
            return
        }

        let newBook = {
            author: bookAuthor.value,
            title: bookTitle.value,
        }

        let url = `http://localhost:3030/jsonstore/collections/books`;
        let response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(newBook)
        });
        let data = await response.json();

        let tr = document.createElement('tr');

        let titleTd = document.createElement('td');
        titleTd.textContent = data.title;

        let authorTd = document.createElement('td');
        authorTd.textContent = data.author;

        let buttonsTd = document.createElement('td');

        let editButton = document.createElement('button');
        editButton.textContent = 'Edit';
        editButton.addEventListener('click', editBook)

        let deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', deleteBook);

        buttonsTd.append(editButton, deleteButton);
        tr.append(titleTd, authorTd, buttonsTd);
        tr.dataset.id = data._id
        tbody.append(tr);
        bookAuthor.value = '';
        bookTitle.value = '';
    };

    async function editBook(e) {
        e.preventDefault();
        formTitle.textContent = 'Edit FORM';
        let currentBookTr = e.target.parentElement.parentElement;
        tbody.dataset.id = currentBookTr.dataset.id
        tbody.dataset.isEdit = true
        let [title, author] = currentBookTr.querySelectorAll('td');
        bookTitle.value = title.textContent;
        bookAuthor.value = author.textContent;
        submitButton.textContent = 'Save';
    };

    async function saveBook() {
        if (tbody.dataset.isEdit) {
            let id = tbody.dataset.id;
            let editedBook = {
                author: bookAuthor.value,
                title: bookTitle.value,
            }
            let url = `http://localhost:3030/jsonstore/collections/books/${id}`;
            let response = await fetch(url, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(editedBook)
            });
            let data = await response.json();

            tbody.querySelectorAll('tr').forEach(tr => {
                if (tbody.dataset.id === tr.dataset.id) {
                    let [title, author] = tr.querySelectorAll('td');
                    title.textContent = data.title
                    author.textContent = data.author;
                }
            });

            bookAuthor.value = '';
            bookTitle.value = '';
            formTitle = document.getElementById('form-title');
            formTitle.textContent = 'FORM';
            submitButton.textContent = 'Submit';
        }
    }


    async function deleteBook(e) {
        let currentBookTr = e.target.parentElement.parentElement;
        let id = currentBookTr.dataset.id;
        let url = `http://localhost:3030/jsonstore/collections/books/${id}`;
        let response = fetch(url, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            },
        });
        currentBookTr.remove();
    }

    function clearAllBooks() {
        tbody.querySelectorAll('tr').forEach(el => el.remove());
    };
}
manageBooks();
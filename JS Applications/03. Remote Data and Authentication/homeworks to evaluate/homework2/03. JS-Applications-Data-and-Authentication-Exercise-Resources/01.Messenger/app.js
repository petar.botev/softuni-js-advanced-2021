function attachEvents() {
    let sendButtonElement = document.getElementById('submit');
    let refreshButtonElement = document.getElementById('refresh');
    let authorInputElement = document.querySelector('input[name="author"]');
    let contentInputElement = document.querySelector('input[name="content"]');
    let textareaElement = document.getElementById('messages');
    sendButtonElement.addEventListener('click', createMessage);
    refreshButtonElement.addEventListener('click', getMessages)

    async function createMessage() {
        try {
            let message = {
                author: authorInputElement.value,
                content: contentInputElement.value,
            };

            let url = `http://localhost:3030/jsonstore/messenger`;

            let createdResponse = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(message),
            });

            let createdResult = await createdResponse.json();
            authorInputElement.value = '';
            contentInputElement.value = '';
            textareaElement.value = '';

        } catch (error) {
            console.error(error);
        }
    }

    async function getMessages() {
        try {
            let url = `http://localhost:3030/jsonstore/messenger`;
            let messagesResponse = await fetch(url);
            let messagesResult = await messagesResponse.json();

            let allMessages = Object.values(messagesResult)
                .map(messages => `${messages.author}: ${messages.content}`)
                .join('\n')

            textareaElement.value = allMessages;
        } catch (error) {
            console.error(error);
        }
    }
}
attachEvents();

/* sendButtonElement.addEventListener('click', createMessage => {
        let message = {
            author: authorInputElement.value,
            content: contentInputElement.value,
        };

        let url = `http://localhost:3030/jsonstore/messenger`;

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(message),
        })

        authorInputElement.value = '';
        contentInputElement.value = '';
        textareaElement.value = '';
    });

    refreshButtonElement.addEventListener('click', (e) => {
        let url = `http://localhost:3030/jsonstore/messenger`;
        fetch(url)
            .then(res => res.json())
            .then(messages => {
                let messagesAll = Object.values(messages)
                    .map(message => `${message.author}: ${message.content}`)
                    .join('\n');
                textareaElement.value = messagesAll;
            })
    }); */

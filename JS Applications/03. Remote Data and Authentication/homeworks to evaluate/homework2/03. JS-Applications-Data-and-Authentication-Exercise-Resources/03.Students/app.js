function students() {
    let firstNameInput = document.querySelector('input[name="firstName"]');
    let lastNameInput = document.querySelector('input[name="lastName"]');
    let facultyNumberInput = document.querySelector('input[name="facultyNumber"]');
    let gradeInput = document.querySelector('input[name="grade"]');
    let submitButton = document.getElementById('submit');
    let table = document.getElementById('results');

    submitButton.addEventListener('click', async (e) => {
        e.preventDefault();
        let addNewStudent = await addStudent();
    });

    async function addStudent() {
        if (firstNameInput.value == '' || lastNameInput.value == '' || facultyNumberInput.value == '' || gradeInput.value == '') {
            return
        }

        if (isNaN(gradeInput.value)) {
            return alert('Grade must be a number');
        } else if (isNaN(Number(facultyNumberInput.value))) {
            return alert('Faculty number must be digits only')
        }

        let newStudent = {
            firstName: firstNameInput.value,
            lastName: lastNameInput.value,
            facultyNumber: facultyNumberInput.value,
            grade: gradeInput.value,
        };

        let url = `http://localhost:3030/jsonstore/collections/students`;
        let post = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(newStudent)
        });
        let response = await fetch(url);
        let result = await response.json();
        let table = await tableData(result);

        firstNameInput.value = '';
        lastNameInput.value = '';
        facultyNumberInput.value = '';
        gradeInput.value = '';
    }

    async function tableData(result) {
        Object.values(result).forEach(element => {
            let tr = document.createElement('tr');

            let firstNameTd = document.createElement('td');
            firstNameTd.textContent = element.firstName;

            let lastNameTd = document.createElement('td');
            lastNameTd.textContent = element.lastName;

            let facultyNumberTd = document.createElement('td');
            facultyNumberTd.textContent = element.facultyNumber;

            let gradeTd = document.createElement('td');
            gradeTd.textContent = element.grade;

            tr.append(firstNameTd, lastNameTd, facultyNumberTd, gradeTd);

            let tableBody = document.querySelector('#results tbody');
            tableBody.append(tr);
        });
    };
}
students();
let sendButton = document.getElementById('submit');
let refreshButton = document.getElementById('refresh');

let nameElement = document.getElementById('author');
let messageElement = document.getElementById('content');
let textarea = document.getElementById('messages');
let url = 'http://localhost:3030/jsonstore/messenger';

sendButton.addEventListener('click', sendButtonFunctionallity);
refreshButton.addEventListener('click', refreshButtonFunctionallity);

async function sendButtonFunctionallity() {
  try {
    const message = {
      author: nameElement.value,
      content: messageElement.value,
    };

    let createResponse = await fetch(url, {
      method: 'Post',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(message),
    });

    let createResult = await createResponse.json();
    if (createResult.author !== '' && createResult.content !== '') {
      let string = `${createResult.author}: ${createResult.content}`;
      if (textarea.innerHTML === '') {
        textarea.textContent += string;
      } else {
        textarea.textContent += '\n' + string;
      }
    }
  } catch (error) {
    console.log(error);
  }
}

async function refreshButtonFunctionallity() {
  let data = await fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
  let tempArray = [];

  Object.values(data).forEach((e) => {
    if (e.author !== '' && e.content !== '') {
      tempArray.push(`${e.author}: ${e.content}`);
    }
  });
  textarea.textContent = tempArray.join('\n');
}

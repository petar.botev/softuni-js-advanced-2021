// url
let baseUrl = 'http://localhost:3030/jsonstore/collections/books';

//html elements
let loadButton = document.getElementById('loadBooks');
let submitButton = document.querySelector('.createForm button');
let saveButton = document.querySelector('.editForm button');
let cancelButton = document.getElementById('cancelBtn');

let tableBody = document.getElementById('books');

let createFormElement = document.querySelector('.createForm');
let editFormElement = document.querySelector('.editForm');

// events
loadButton.addEventListener('click', loadButtonFunctionallity);
submitButton.addEventListener('click', submitButtonFunctionallity);

async function loadButtonFunctionallity() {
  let books = await fetch(baseUrl)
    .then((response) => response.json())
    .catch((error) => console.log(error));
  tableBody.innerHTML = '';

  Object.keys(books).forEach((id) => {
    let tr = document.createElement('tr');

    tr.setAttribute('data-id', id);

    let nameTh = document.createElement('th');
    nameTh.textContent = books[id].title;

    let authorTh = document.createElement('th');
    authorTh.textContent = books[id].author;

    let buttonsTh = document.createElement('th');
    let editButton = document.createElement('button');
    editButton.textContent = 'Edit';

    editButton.addEventListener('click', editButtonFunctionallity);

    let deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';

    deleteButton.addEventListener('click', deleteButtonFunctionallity);

    buttonsTh.appendChild(editButton);
    buttonsTh.appendChild(deleteButton);

    tr.appendChild(nameTh);
    tr.appendChild(authorTh);
    tr.appendChild(buttonsTh);

    tableBody.appendChild(tr);
  });
}

async function submitButtonFunctionallity(e) {
  e.preventDefault();

  let titleElement = document.querySelector('.createForm input[name="title"]');
  let authorElement = document.querySelector(
    '.createForm input[name="author"]'
  );

  let title = titleElement.value;
  let author = authorElement.value;

  if (title !== '' && author !== '') {
    tableBody.innerHTML = '';

    const book = {
      author,
      title,
    };

    await fetch(baseUrl, {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(book),
    }).catch((error) => console.log(error));

    titleElement.value = '';
    authorElement.value = '';

    loadButtonFunctionallity();
  }
}

function editButtonFunctionallity(e) {
  let currentBook = e.target.closest('tr');
  let currentBookId = currentBook.getAttribute('data-id');

  let title = currentBook.querySelector('th:nth-child(1)');
  let author = currentBook.querySelector('th:nth-child(2)');

  let editFormTitleElement = document.querySelector(
    '.editForm input[name="title"]'
  );
  let editFormAuthorElement = document.querySelector(
    '.editForm input[name="author"]'
  );

  editFormTitleElement.value = title.textContent;
  editFormAuthorElement.value = author.textContent;

  createFormElement.style.display = 'none';
  editFormElement.style.display = 'block';

  cancelButton.addEventListener('click', cancelButtonFunctionallity);

  saveButton.addEventListener('click', async (e) => {
    e.preventDefault();

    const book = {
      title: editFormTitleElement.value,
      author: editFormAuthorElement.value,
    };

    let editResponse = await fetch(`${baseUrl}/${currentBookId}`, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'Put',
      body: JSON.stringify(book),
    })
      .then((response) => response.json())
      .catch((error) => console.log(error));

    title.textContent = editResponse.title;
    author.textContent = editResponse.author;

    cancelButtonFunctionallity();
  });
}

function cancelButtonFunctionallity() {
  createFormElement.style.display = 'block';
  editFormElement.style.display = 'none';
}

function deleteButtonFunctionallity(e) {
  let book = e.target.closest('tr');
  let bookId = book.getAttribute('data-id');

  console.log(bookId);
  book.remove();

  fetch(`${baseUrl}/${bookId}`, {
    method: 'Delete',
  });
}

// html buttons
let loadButton = document.getElementById('btnLoad');
let createButton = document.getElementById('btnCreate');

// html elements
let phonebookElement = document.getElementById('phonebook');
let nameInputElement = document.getElementById('person');
let phoneInputElement = document.getElementById('phone');

// html urls
let url = 'http://localhost:3030/jsonstore/phonebook';

// events
loadButton.addEventListener('click', loadButtonFunctionallity);
createButton.addEventListener('click', createButtonFunctionallity);

async function loadButtonFunctionallity() {
  let phones = await fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));

  phonebookElement.innerHTML = '';
  Object.keys(phones).forEach((p) => {
    let name = phones[p].person;
    let phone = phones[p].phone;
    let id = phones[p]._id;

    let tr = document.createElement('tr');

    let nameTd = document.createElement('td');
    nameTd.textContent = name;

    let phoneTd = document.createElement('td');
    phoneTd.textContent = phone;

    let buttonTd = document.createElement('td');
    let button = document.createElement('button');

    button.setAttribute('data-id', id);
    button.textContent = 'Delete';

    button.addEventListener('click', deleteButtonFunctionallity);

    buttonTd.appendChild(button);

    tr.appendChild(nameTd);
    tr.appendChild(phoneTd);
    tr.appendChild(buttonTd);

    phonebookElement.appendChild(tr);

    nameInputElement.value = '';
    phoneInputElement.value = '';
  });
}

async function deleteButtonFunctionallity(e) {
  let id = e.target.getAttribute('data-id');

  e.target.closest('tr').remove();

  fetch(`${url}/${id}`, {
    method: 'DELETE',
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

async function createButtonFunctionallity() {
  const postRequestData = {
    person: nameInputElement.value,
    phone: phoneInputElement.value,
  };

  if (nameInputElement.value !== '' && phoneInputElement.value !== '') {
    await fetch(url, {
      method: 'Post',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(postRequestData),
    })
      .then((response) => response.json())
      .catch((error) => console.log(error));

    phonebookElement.innerHTML = '';
    loadButtonFunctionallity();
  }
}

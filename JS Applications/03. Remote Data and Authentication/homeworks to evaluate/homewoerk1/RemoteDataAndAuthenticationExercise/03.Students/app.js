const url = 'http://localhost:3030/jsonstore/collections/students';

// html elements
let tableBody = document.querySelector('#results tbody');
let submitButton = document.getElementById('submit');

let firstNameInputElement = document.querySelector('input[name="firstName"]');
let lastNameInputElement = document.querySelector('input[name="lastName"]');
let numberInputElement = document.querySelector('input[name="facultyNumber"]');
let gradeInputElement = document.querySelector('input[name="grade"]');

async function displayStudents() {
  let students = await fetch(url).then((response) => response.json());

  Object.values(students).forEach((s) => {
    let tr = document.createElement('tr');

    let firstNameTd = document.createElement('td');
    let lastNameTd = document.createElement('td');
    let numberTd = document.createElement('td');
    let gradeTd = document.createElement('td');

    firstNameTd.textContent = s.firstName;
    lastNameTd.textContent = s.lastName;
    numberTd.textContent = s.facultyNumber;
    gradeTd.textContent = s.grade;

    tr.appendChild(firstNameTd);
    tr.appendChild(lastNameTd);
    tr.appendChild(numberTd);
    tr.appendChild(gradeTd);

    tableBody.appendChild(tr);
  });
}

displayStudents();

submitButton.addEventListener('click', submitButtonFunctionallity);

function submitButtonFunctionallity(e) {
  e.preventDefault();

  if (
    firstNameInputElement.value !== '' &&
    lastNameInputElement.value !== '' &&
    numberInputElement.value !== '' &&
    gradeInputElement.value !== ''
  ) {
    const student = {
      firstName: firstNameInputElement.value,
      lastName: lastNameInputElement.value,
      facultyNumber: numberInputElement.value,
      grade: gradeInputElement.value,
    };

    fetch(url, {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(student),
    }).then((response) => response.json());

    tableBody.innerHTML = '';
    displayStudents();

    firstNameInputElement.value = '';
    lastNameInputElement.value = '';
    numberInputElement.value = '';
    gradeInputElement.value = '';
  }
}

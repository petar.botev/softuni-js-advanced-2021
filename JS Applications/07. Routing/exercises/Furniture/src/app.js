import page from "../node_modules/page/page.mjs";
//import page from '//unpkg.com/page/page.mjs';
import {dashboardPage} from "./views/dashboard.js";
import {createPage} from "./views/create.js";
import {detailsPage} from "./views/details.js";
import {editPage} from "./views/edit.js";
import {loginPage} from "./views/login.js";
import {myPage} from "./views/myFurniture.js";
import {registerPage} from "./views/register.js";

import { render } from "../node_modules/lit-html/lit-html.js";
import { logout } from "./api/api.js";

const main = document.querySelector('.container');

page('/', decorateContext, dashboardPage);
page('/create', decorateContext, createPage);
page('/dashboard', decorateContext, dashboardPage);
page('/details/:id', decorateContext, detailsPage);
page('/edit/:id', decorateContext, editPage);
page('/login', decorateContext, loginPage);
page('/myFurniture', decorateContext, myPage);
page('/register', decorateContext, registerPage);

setUserNav();

page.start();

document.getElementById('logoutBtn').addEventListener('click', async () => {
    await logout();
    setUserNav();
    page.redirect('/');
});

function decorateContext(ctx, next){
    ctx.render = (content) => render(content, main);
    ctx.setUserNav = setUserNav;
    next();
}

function setUserNav(){
    const userId = sessionStorage.getItem('userId');
    if(userId != null){
        document.getElementById('user').style.display = 'inline-block';
        document.getElementById('guest').style.display = 'none';

    } else {
        document.getElementById('user').style.display = 'none';
        document.getElementById('guest').style.display = 'inline-block';
    }
}
import { html } from '../../node_modules/lit-html/lit-html.js';
import { register } from '../api/api.js';

const registerTemplate = (onClick, invalidEmail, invalidPassword, invalidRepas) => html`
       <div class="row space-top">
            <div class="col-md-12">
                <h1>Register New User</h1>
                <p>Please fill all fields.</p>
            </div>
        </div>
        <form @submit=${onClick}>
            <div class="row space-top">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="email">Email</label>
                        <input class=${"form-control" + (invalidEmail ? ' is-invalid' : '')} id="email" type="text" name="email">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="password">Password</label>
                        <input class=${"form-control" + (invalidPassword ? ' is-invalid' : '')} id="password" type="password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="rePass">Repeat</label>
                        <input class=${"form-control" + (invalidRepas ? ' is-invalid' : '')} id="rePass" type="password" name="rePass">
                    </div>
                    <input type="submit" class="btn btn-primary" value="Register" />
                </div>
            </div>
        </form>
</div>
`;

export async function registerPage(context){

    async function onClick(e){
        e.preventDefault();
        let formData = new FormData(e.target);
        
        let email = formData.get('email').trim();
        let password = formData.get('password').trim();
        let repas = formData.get('rePass').trim();
    
        if(email == '' || password == '' || repas == ''){
            context.render(registerTemplate(onClick, email == '', password == '', repas == ''));
            return alert('All fields must have a value!');
        }
        if(password != repas){
            context.render(registerTemplate(onClick, false, true, true));
            return alert('Password does not match!');
        }
    
        await register(email, password);
        
        context.setUserNav();
        context.page.redirect('/');
    }

    context.render(registerTemplate(onClick))

}
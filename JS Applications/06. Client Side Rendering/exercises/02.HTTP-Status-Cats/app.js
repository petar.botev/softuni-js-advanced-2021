import { render } from "../node_modules/lit-html/lit-html.js";
import { cats } from "./catSeeder.js";
import { ulTemplate } from "./template.js";

export function onClick(e){
    let buttonElement = e.target;
    let elementToShow = buttonElement.nextElementSibling;
    
    if (elementToShow.style.display == 'none' ) {
        buttonElement.textContent = "Hide status code";
        elementToShow.style.display = 'block';
    } else {
        buttonElement.textContent = "Show status code";
        elementToShow.style.display = 'none';
    }
}

function start(){

    let mainElement = document.getElementById('allCats');
    let result = ulTemplate(cats);

    render(result, mainElement);
}
start();


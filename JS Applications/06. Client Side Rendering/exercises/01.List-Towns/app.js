import template from "./template.js";

function start(){
    document.querySelector('form').addEventListener('submit', (e) => {
        e.preventDefault();
        template();
    });
}

start();
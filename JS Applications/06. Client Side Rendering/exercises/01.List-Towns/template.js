import { html, render } from '../node_modules/lit-html/lit-html.js';

function template() {
  const myTemplate = (data) => html` <li>${data}</li>`;

  let input = document.getElementById('towns').value.split(', ');
  
  let result = input.map(myTemplate);
  let mainElement = document.querySelector('#root ul');

  render(result, mainElement);
}

export default template;

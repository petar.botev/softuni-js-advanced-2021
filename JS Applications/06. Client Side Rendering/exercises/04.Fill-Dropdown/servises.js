
async function getRequest(){
    let getUrl = 'http://localhost:3030/jsonstore/advanced/dropdown';
    let allOptions = await fetch(getUrl);
    let allOptionsObj = Object.values(await allOptions.json());

    return allOptionsObj;
}

async function postRequest(townsArr){
    let getUrl = 'http://localhost:3030/jsonstore/advanced/dropdown';
    let inputElement = document.querySelector('#form');
    let dataForm = new FormData(inputElement);
    let townName = dataForm.get('input-text');

    if(townsArr.some(x => x.text == townName)){
        alert("The town already exists!");
        return;
    }
    if(townName == ''){
        alert('Input can not be empty.');        
        return;
    }

    let bodyObj = {
        text: townName
    }

    let postData = await fetch(getUrl, {
        method: "Post",
        headers: {
            "Content-Type": "application/jsom"
        },
        body:JSON.stringify(bodyObj)
    });

    return postData;
}

export {
    getRequest,
    postRequest
}


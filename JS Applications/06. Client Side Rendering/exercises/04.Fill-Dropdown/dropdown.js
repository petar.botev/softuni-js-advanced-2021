import { render } from "../node_modules/lit-html/lit-html.js";
import { getRequest, postRequest } from "./servises.js";
import { optionElement } from "./templste.js";

let allOptionsObj = await getRequest();

renderElements(allOptionsObj);

document.querySelector('#form').addEventListener('submit', async (e) => {
    e.preventDefault();

    allOptionsObj = await getRequest();
    
    await postRequest(allOptionsObj);
    allOptionsObj = await getRequest();
    renderElements(allOptionsObj);
    clearInputField();
});

function clearInputField(){
    document.querySelector('#itemText').value = '';
}

function renderElements(allOptionsObj) {
    let meinElement = document.querySelector('#menu');
    
    let res = allOptionsObj.map(x => optionElement(x));
    
    render(res, meinElement)
}

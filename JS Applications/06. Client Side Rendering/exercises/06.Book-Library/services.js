async function request(url, options) {
  const responce = await fetch(url, options);
  const data = await responce.json();

  return data;
}

const host = 'http://localhost:3030/jsonstore/collections/books';

async function getAllBooks() {
  return Object.entries(await request(host));
}

async function getBookById(id) {
  return await request(host + '/' + id);
}

async function createBook(book) {
  return await request(host, {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(book)
  });
}

async function updateBook(id, book) {
    return await request(host + '/' + id, {
      method: 'Put',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(book)
    });
}
  
async function deleteBook(id) {
    return await request(host + '/' + id, {
      method: 'Delete',
    });
}

export {
    getAllBooks,
    getBookById,
    createBook,
    updateBook,
    deleteBook
}
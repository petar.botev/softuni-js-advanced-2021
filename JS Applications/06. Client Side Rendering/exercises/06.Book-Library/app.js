import { render } from "../node_modules/lit-html/lit-html.js";
import { allTemplateElements } from "../06.Book-Library/templates.js"
import { getAllBooks, getBookById, updateBook } from "./services.js";

let allBooksArray = [];

export async function load() {
    let booksArr = await getAllBooks();
    allBooksArray = booksArr;
    renderElement(booksArr);
}

async function start(){
    
    renderElement([]);
}

function renderElement(arr, isEdit, bookToEdit) {
    let booksArray = arr.map((x) => {
        return {
            title: x[1].title,
            author: x[1].author,
            id: x[0]
        }
    });
    
    let bodyElement = document.querySelector('body');
   
    render(allTemplateElements(booksArray, isEdit, bookToEdit), bodyElement);
}

export async function clickEdit(e){
    
    let id = e.target.parentElement.dataset.id;
    let response = await getBookById(id);
    console.log(response);
    
    renderElement(allBooksArray, true, response);
}

export async function submitEdit(){
    let form = document.querySelector('form');
    let formData = new FormData(form);

    let posObj = {
        title: formData.get('title'),
        author: formData.get('author'),
        _id: id
    }

    updateBook(id, posObj);

    form.reset();
}

start();
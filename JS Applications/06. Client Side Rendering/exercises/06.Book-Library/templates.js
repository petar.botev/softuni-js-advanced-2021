import { html } from '../node_modules/lit-html/lit-html.js';
import { clickEdit, load } from './app.js';

let trElement = (book) => html`
  <tr>
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td data-id=${book.id}>
      <button class="editBtn" @click=${clickEdit}>Edit</button>
      <button class="deleteBtn">Delete</button>
    </td>
  </tr>
`;

let tableElement = (books) => html`
  <button id="loadBooks" @click=${load}>LOAD ALL BOOKS</button>
  <table>
    <thead>
      <tr>
        <th>Title</th>
        <th>Author</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody @click=${onClick}>
      ${books.map((x) => trElement(x))}
    </tbody>
  </table>
`;

let addForm = () => html`
  <form id="add-form">
    <h3>Add book</h3>
    <label>TITLE</label>
    <input type="text" name="title" placeholder="Title..." />
    <label>AUTHOR</label>
    <input type="text" name="author" placeholder="Author..." />
    <input type="submit" value="Submit" />
  </form>
`;

let editForm = (book) => html`
  <form id="edit-form">
    <input type="hidden" name="id" />
    <h3>Edit book</h3>
    <label>TITLE</label>
    <input type="text" name="title" placeholder="Title..." .value=${book.title}>
    <label>AUTHOR</label>
    <input type="text" name="author" placeholder="Author..." .value=${book.author}>
    <input type="submit" value="Save" @click=${submitEdit}/>
  </form>
`;

let allTemplateElements = (books, isEdit, bookToEdit) => html`
  ${tableElement(books)} 
  ${isEdit ? editForm(bookToEdit) : addForm()}
`;


function onClick(e) {
  if(e.target.classList.contains('editBtn')){
    console.log('edit');
  } else if(e.target.classList.contains('deleteBtn')){
    console.log('delted');
  }
}

export { allTemplateElements };

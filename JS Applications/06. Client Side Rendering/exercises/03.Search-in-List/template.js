import { html } from "../node_modules/lit-html/lit-html.js";

let townTemplate = (town) => html`<li>${town}</li>`;

export let allTownsTemplate = (towns) => html`<ul>${towns.map(townTemplate)}</ul>`;
import { render } from "../node_modules/lit-html/lit-html.js";
import { allTownsTemplate } from "./template.js";
import { towns } from "./towns.js";

let mainElement = document.querySelector('#towns');
let result = allTownsTemplate(towns);
render(result, mainElement);

document.querySelector('#search-button').addEventListener('click', search);

function search() {
   let allLiElements = Array.from(mainElement.querySelectorAll('li'));
   let allLiTextContent = allLiElements.map(x => x.textContent);
   
   let input = document.querySelector('#searchText').value;
   
   let currTowns = '';
   if (allLiTextContent.some(x => x.toLowerCase().includes(input.toLowerCase())) && input !== '') {
      document.querySelector('#searchText').value = '';
      allLiElements.forEach(x => x.classList.remove('active'));
      currTowns = allLiElements.filter(x => x.textContent.toLowerCase().includes(input));
      currTowns.forEach(x => x.setAttribute('class', 'active'));
   }else{
      document.querySelector('#searchText').value = '';

      allLiElements.forEach(x => x.classList.remove('active'));
      //currTowns.length = 0;
   } 


   document.querySelector('#result').textContent = `${currTowns.length} matches found`;
}

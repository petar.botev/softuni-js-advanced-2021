import { render } from '../node_modules/lit-html/lit-html.js';
import { createTd } from './templates.js';

async function solve() {
  document.querySelector('#searchBtn').addEventListener('click', onClick);

  let requestData = await request();
  let requestDataArr = Object.values(requestData);
  let mainElement = document.querySelector('#tBody-element');
  let res = requestDataArr.map((x) => createTd(x));
  render(res, mainElement);

  function onClick() {
    let input = document.querySelector('#searchField');
    let inputText = input.value.toLowerCase();
    document.querySelector('#searchField').value = '';
    let allRowsElements = document.querySelectorAll('tbody tr');
    allRowsElements.forEach((x) => x.classList.remove('select'));

    for (let i = 0; i < allRowsElements.length; i++) {
      let allTd = allRowsElements[i].querySelectorAll('td');
      for (let j = 0; j < allTd.length; j++) {
        if (allTd[j].textContent.toLocaleLowerCase().includes(inputText)) {
          allRowsElements[i].classList.add('select');

          break;
        }
      }
    }
  }

  async function request() {
    let url = 'http://localhost:3030/jsonstore/advanced/table';
    let responce = await fetch(url);
    let responceDataObj = await responce.json();

    return responceDataObj;
  }
}

solve();

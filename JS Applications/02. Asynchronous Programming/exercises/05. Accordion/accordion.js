function solution() {
    let listUrl = 'http://localhost:3030/jsonstore/advanced/articles/list';

    fetch(listUrl)
        .then(r => r.json())
        .then(d => {
            d.forEach(el => {
                let accordionElement = createHeadElement(el);
                document.getElementById('main').append(accordionElement);
            });
        });
}

function createHeadElement(info){
    let divAccordion = document.createElement('div');
    divAccordion.classList.add('accordion');

    let divHead = document.createElement('div');
    divHead.classList.add('head');

    let spanTitle = document.createElement('span');
    spanTitle.textContent = info.title;

    let buttonMore = document.createElement('button');
    buttonMore.classList.add('button');
    buttonMore.setAttribute('id', `${info._id}`);
    buttonMore.textContent = 'More';
    buttonMore.addEventListener('click', moreText);

    divHead.appendChild(spanTitle);
    divHead.appendChild(buttonMore);

    divAccordion.append(divHead);

    return divAccordion;
}

function moreText(e){
    if(e.target.textContent == 'More'){
        let currId = e.target.id;
        e.target.textContent = 'Less';
        let extendUrl = `http://localhost:3030/jsonstore/advanced/articles/details/${currId}`;
        
        fetch(extendUrl)
            .then(r => r.json())
            .then(d => {
                let extraElement = createExtendElement(d);
                extraElement.style.display = 'block';
                e.target.parentElement.parentElement.append(extraElement);
                console.log(d);
            });
    }
    else{
        e.target.parentElement.nextSibling.remove();
        e.target.textContent = 'More';
    }
}

function createExtendElement(info){
    let divExtra = document.createElement('div');
    divExtra.classList.add('extra');

    let pText = document.createElement('p');
    pText.textContent = info.content;

    divExtra.append(pText);

    return divExtra;
}

solution();
function solve() {
  let currStop = 'depot';
  let nextStop = '';

  function depart() {
    
    let url = `http://localhost:3030/jsonstore/bus/schedule/${currStop}`;

    document.getElementById('depart').disabled = true;
    document.getElementById('arrive').disabled = false;

    fetch(url)
      .then((r) => r.json())
      .then((d) => {
        document.querySelector('.info').textContent = `Next stop ${d.name}`;
        nextStop = d.name;
        currStop = d.next;
      })
      .catch((error) => {
        document.querySelector('.info').textContent = `ERROR`;
      });
  }

  function arrive() {
    document.getElementById('depart').disabled = false;
    document.getElementById('arrive').disabled = true;

    document.querySelector('.info').textContent = `Arriving at ${nextStop}`;
  }

  return {
    depart,
    arrive,
  };
}

let result = solve();

function attachEvents() {
    //Load Posts Button
    document.getElementById('btnLoadPosts').addEventListener('click', () => {
        let postsUrl = 'http://localhost:3030/jsonstore/blog/posts';
        fetch(postsUrl)
            .then(r => r.json())
            .then(d => {
                document.getElementById('posts').innerHTML = '';
                createPostMenue(d);
            });
    });

    // View Button
    document.getElementById('btnViewPost').addEventListener('click', () => {
        let postId = document.getElementById('posts').value;
        console.log(postId);
        if(postId == ''){
            alert("First press Load Posts button!");
            return; 
        }
        let viewUrl = `http://localhost:3030/jsonstore/blog/posts/${postId}`;
        fetch(viewUrl)
            .then(r => r.json())
            .then(d => {
                document.getElementById('post-body').innerHTML = '';
                let currId = d.id;
                let pBodyElement = createPostDetailsElement(d);
                //let refNode = document.getElementById('post-body');
                document.getElementById('post-body').append(pBodyElement);
                //document.querySelector('body').insertBefore(pBodyElement, refNode);

                let commentUrl = 'http://localhost:3030/jsonstore/blog/comments';
                fetch(commentUrl)
                    .then(r => r.json())
                    .then(comment => {
                        document.getElementById('post-comments').innerHTML = '';
                        let allComments = Object.entries(comment).filter(x => x[1].postId == currId);
                        for (let i = 0; i < allComments.length; i++) {
                            
                            let liElement = createCommentsElement(allComments[i]);
                            document.getElementById('post-comments').appendChild(liElement);
                        }
                      console.log(allComments);
                    });
            });
    });
}

function createCommentsElement(info){
    
    let newli = document.createElement('li');
    newli.setAttribute('id', `${info[1].id}`);
    newli.textContent = `${info[1].text}`;

    return newli;
}

function createPostDetailsElement(info){
    document.querySelector('#post-title').textContent = '';
    document.querySelector('#post-title').textContent = info.title;

    let pBody = document.createElement('p');
    pBody.setAttribute('id', 'post-body');
    pBody.textContent = `${info.body}`;

    return pBody;
}

function createPostMenue(info){

    for (const key in info) {
        let postOption = document.createElement('option');
        postOption.value = info[key].id;
        postOption.textContent = info[key].title;
        document.getElementById('posts').append(postOption);
        console.log(postOption);
    }
}

attachEvents();
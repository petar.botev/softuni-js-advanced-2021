function getInfo() {
    let input = document.getElementById('stopId');
    let inputValue = input.value;
    let stopNameElement =  document.getElementById('stopName');

    let apiUrl = `http://localhost:3030/jsonstore/bus/businfo/${inputValue}`;

    fetch(apiUrl)
      .then(r => r.json())
      .then(d => {
          input.value = '';
          stopNameElement.textContent = d.name; 
          document.getElementById('buses').innerHTML = '';
          Object.keys(d.buses).forEach(key => {
              let newLiElement = document.createElement('li');
              newLiElement.textContent = `Bus ${key} arrives in ${d.buses[key]}`;
              document.getElementById('buses').appendChild(newLiElement);
            });
      })
      .catch(error => {
        document.getElementById('buses').innerHTML = '';
        stopNameElement.textContent = 'ERROR';
      });
}
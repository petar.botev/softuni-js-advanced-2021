function lockedProfile() {
    let url = 'http://localhost:3030/jsonstore/advanced/profiles';

    fetch(url)
        .then(r => r.json())
        .then(d => {
            document.querySelector('.profile').remove();
            let counter = 0;
            for (const key in d) {
                let personInfo = d[key];
                console.log(personInfo);
                let cardElement = createCard(counter, personInfo);
                document.querySelector('#main').appendChild(cardElement);
                counter++;
            }
        });

        function createCard(index, info){
            let divProfile = document.createElement('div');
            divProfile.classList.add('profile');

            let divUserFields = document.createElement('div');
            divUserFields.setAttribute('id', `user${index}HiddenFields`);
            divUserFields.style.display = 'none';
            //divUserFields.setAttribute('hidden', true);

            let imgElement = document.createElement('img');
            imgElement.setAttribute('src', './iconProfile2.png');
            imgElement.setAttribute('class', 'userIcon');

            let labelLock = document.createElement('label');
            labelLock.textContent = 'Lock';

            let radioButton = document.createElement('input');
            radioButton.setAttribute('type', 'radio');
            radioButton.setAttribute('name', `user${index}Locked`);
            radioButton.setAttribute('value', 'lock');
            radioButton.setAttribute('checked', true);

            let labelUnLock = document.createElement('label');
            labelUnLock.textContent = 'Unlock';

            let unlockRadioButton = document.createElement('input');
            unlockRadioButton.setAttribute('type', 'radio');
            unlockRadioButton.setAttribute('name', `user${index}Locked`);
            unlockRadioButton.setAttribute('value', 'unlock');
            //unlockRadioButton.setAttribute('checked', 'checked');


            let labelUsername = document.createElement('label');
            labelUsername.textContent = 'Username';

            let labelUsernameInput = document.createElement('input');
            labelUsernameInput.setAttribute('type', 'text');
            labelUsernameInput.setAttribute('name', `user${index}Username`);
            labelUsernameInput.setAttribute('value', '');
            labelUsernameInput.setAttribute('disabled', true);
            labelUsernameInput.setAttribute('readonly', true);
            labelUsernameInput.value = info.username;

            let button = document.createElement('button');
            button.textContent = 'Show more';
            button.addEventListener('click', toggleCard);

            let mainBrElement = document.createElement('br');
            let mainHrElement = document.createElement('hr');

            let labelEmail = document.createElement('label');
            labelEmail.textContent = 'Email:';

            let inputEmail = document.createElement('input');
            inputEmail.setAttribute('type', 'email');
            inputEmail.setAttribute('name', `user${index}Email`);
            inputEmail.setAttribute('value', '');
            inputEmail.setAttribute('disabled', true);
            inputEmail.setAttribute('readonly', true);
            inputEmail.value = info.email;

            let labelAge = document.createElement('label');
            labelAge.textContent = 'Age:';

            let inputAge = document.createElement('input');
            inputAge.setAttribute('type', 'email');
            inputAge.setAttribute('name', `user${index}Age`);
            inputAge.setAttribute('value', '');
            inputAge.setAttribute('disabled', true);
            inputAge.setAttribute('readonly', true);
            inputAge.value = info.age;

            let newHrElement = document.createElement('hr');

            divUserFields.appendChild(newHrElement);
            divUserFields.appendChild(labelEmail);
            divUserFields.appendChild(inputEmail);
            divUserFields.appendChild(labelAge);
            divUserFields.appendChild(inputAge);

            divProfile.appendChild(imgElement);
            divProfile.appendChild(labelLock);
            divProfile.appendChild(radioButton);
            divProfile.appendChild(labelUnLock);
            divProfile.appendChild(unlockRadioButton);
            divProfile.appendChild(mainBrElement);
            divProfile.appendChild(mainHrElement);

            divProfile.appendChild(labelUsername);
            divProfile.appendChild(labelUsernameInput);
            divProfile.appendChild(divUserFields);
            divProfile.appendChild(button);

            return divProfile;
        }

        function toggleCard (e){
            let unlockChecked = e.target.parentElement.querySelector(':nth-child(5)');
            console.log(unlockChecked);
            if(unlockChecked.checked == true){
                if(e.target.previousSibling.style.display === 'none'){
                    //e.target.previousSibling.removeAttribute('hidden');
                    e.target.previousSibling.style.display = 'block';
                    e.target.textContent = 'Hide it';
                }
                else{
                    //e.target.previousSibling.setAttribute('hidden', true);
                    e.target.previousSibling.style.display = 'none';
                    e.target.textContent = 'Show more';
                }
            }
        }
}
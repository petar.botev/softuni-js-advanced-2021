function attachEvents() {
    document.querySelector('#submit').addEventListener('click', () =>{
        let input = document.getElementById('location').value;
        let symbolObj = {Sunny: '☀', "Partly sunny": '⛅', Overcast: '☁', Rain: '☂'};
        let forcastElement = document.querySelector('#current .forecasts');
        let upcomingForcast = document.querySelector('#upcoming .forecasts-info');
        
        if(forcastElement !== null){
            document.querySelector('#current .forecasts').remove();
            if(upcomingForcast !== null){
                document.querySelector('#upcoming .forecasts-info').remove();
            }
        }

        let url = 'http://localhost:3030/jsonstore/forecaster/locations';
        fetch(url)
            .then(r => r.json())
            .then(d => {
                
                let currObject = d.find(x => x.name == input);

                if(currObject == undefined){
                    document.getElementById('forecast').style.display = 'block';
                   let elementError = document.querySelector('.forecasts');
                   if(elementError !== null){
                    elementError.remove();
                   }
                    document.getElementById('current').appendChild(createCurrentWeatherElements());
                                        
                    document.querySelector('.condition .forecast-data').textContent = 'ERROR';
                    
                }
                let conditionUrl = `http://localhost:3030/jsonstore/forecaster/today/${currObject.code}`;

                fetch(conditionUrl)
                    .then(r => r.json())
                    .then(d => {
                        let currentWeatherBlock = createCurrentWeatherElements();
                        document.getElementById('current').appendChild(currentWeatherBlock);

                        document.querySelector('.symbol').textContent = symbolObj[d.forecast.condition];
                        document.querySelector('.forecasts').lastElementChild.querySelector(':nth-child(1)').textContent = d.name;
                        document.querySelector('.forecasts').lastElementChild.querySelector(':nth-child(2)').textContent = `${d.forecast.low}°/${d.forecast.high}°`;
                        document.querySelector('.forecasts').lastElementChild.querySelector(':nth-child(3)').textContent = d.forecast.condition;
                                                
                        document.getElementById('forecast').style.display = 'block';
                    });

                    let forecastUrl = `http://localhost:3030/jsonstore/forecaster/upcoming/${currObject.code}`;  

                    fetch(forecastUrl)
                        .then(r => r.json())
                        .then(d => {

                            let divForcast = document.createElement('div');
                            divForcast.classList.add('forecasts-info');

                            for (let i = 0; i < 3; i++) {
                                let currentWeatherBlock = createForecastWeatherElements();
                                divForcast.appendChild(currentWeatherBlock);
                                document.getElementById('upcoming').appendChild(divForcast);
        
                                document.querySelector('.forecasts-info').lastChild.querySelector(':nth-child(1)').textContent = symbolObj[d.forecast[i].condition];
                                document.querySelector('.forecasts-info').lastChild.querySelector(':nth-child(2)').textContent = `${d.forecast[i].low}°/${d.forecast[i].high}°`;
                                document.querySelector('.forecasts-info').lastChild.querySelector(':nth-child(3)').textContent = d.forecast[i].condition;
                            }
                        });
            })
    });

    function createForecastWeatherElements(){
        
        let spanCondition = document.createElement('span');
        spanCondition.classList.add('upcoming');
        
        let spanWeatherSymbol = document.createElement('span');
        spanWeatherSymbol.classList.add('symbol');

        let spanTemperature = document.createElement('span');
        spanTemperature.classList.add('forecast-data');

        let spanWeatherCondition = document.createElement('span');
        spanWeatherCondition.classList.add('forecast-data');

        spanCondition.appendChild(spanWeatherSymbol);
        spanCondition.appendChild(spanTemperature);
        spanCondition.appendChild(spanWeatherCondition);

        return spanCondition;
    }

    function createCurrentWeatherElements(){
        let divForcast = document.createElement('div');
        divForcast.classList.add('forecasts');

        let spanConditionSymbol = document.createElement('span');
        //spanConditionSymbol.classList.add('condition symbol');
        spanConditionSymbol.setAttribute('class', 'condition symbol');

        let spanCondition = document.createElement('span');
        spanCondition.classList.add('condition');

        let spanLocation = document.createElement('span');
        spanLocation.classList.add('forecast-data');

        let spanTemperature = document.createElement('span');
        spanTemperature.classList.add('forecast-data');

        let spanWeather = document.createElement('span');
        spanWeather.classList.add('forecast-data');

        spanCondition.appendChild(spanLocation);
        spanCondition.appendChild(spanTemperature);
        spanCondition.appendChild(spanWeather);

        divForcast.appendChild(spanConditionSymbol);
        divForcast.appendChild(spanCondition);

        return divForcast;
    }
}

attachEvents();
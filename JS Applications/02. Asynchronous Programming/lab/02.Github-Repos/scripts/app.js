function loadRepos() {
	const username = document.querySelector('#username').value;
	const url = `https://api.github.com/users/${username}/repos`;

	fetch(url)
	.then(r => r.json())
	.then(d => {
		const ulElement = document.getElementById('repos');
		ulElement.innerHTML = '';
		d.forEach(el => {
			const liElement = document.createElement('li');
			const aElement = document.createElement('a');
			aElement.textContent = el.full_name;
		    aElement.setAttribute('href', el.html_url);
			liElement.appendChild(aElement);
			ulElement.appendChild(liElement);
		});
	});
	
}
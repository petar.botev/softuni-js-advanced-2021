let button = document.querySelector('button');
button.addEventListener('click', () => {
   loadRepos();
});

function loadRepos() {
   let httpInstance = new XMLHttpRequest();
   let url = 'https://api.github.com/users/testnakov/repos';
   
   httpInstance.addEventListener('readystatechange', () => {
      if (httpInstance.readyState == 4 && httpInstance.status == 200) {

         document.getElementById("res").textContent = httpInstance.responseText;
         
      }
   });

   httpInstance.open("GET", url);
   
   httpInstance.send();
}

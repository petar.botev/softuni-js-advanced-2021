async function getInfo() {

    const busIdElement = document.querySelector('#stopId').value;
    const stopNameElement = document.querySelector('#stopName');
    const busesData = document.querySelector('#buses');

    let url = `http://localhost:3030/jsonstore/bus/businfo/${busIdElement}`;

    try {

        const response = await fetch(url);

        if (!response.ok || busIdElement !== '1287' &&
            busIdElement != '1308' && busIdElement != '1327' &&
            busIdElement != '2334') {
            throw new Error('Error');
        }
        busesData.innerHTML = '';

        const data = await response.json();

        stopNameElement.textContent = data.name;

        Object.entries(data.buses).forEach(b => {
            let liElement = document.createElement('li');
            liElement.textContent = `Bus ${b[0]} arrives in ${b[1]}`;
            busesData.appendChild(liElement);
        })

    } catch (error) {
        stopNameElement.textContent = error.message;
    }

}
function solve() {
    let currentStop = 'depot';
    let nextStop = 'depot';
    let apiUrl = `http://localhost:3030/jsonstore/bus/businfo/${currentStop}`;
 
    function depart() {
        toggleButtons('#arrive', '#depart');
        $.ajax({
            method: 'GET',
            url: apiUrl + currentStop + '.json',
            success: function (data) {
                nextStop = data.next;
                $('#info').find('span').text(`Next stop ${data.name}`);
            }
        });
    }
 
    function arrive() {
        toggleButtons('#depart', '#arrive');
        $.ajax({
            method: 'GET',
            url: apiUrl + currentStop + '.json',
            success: function (data) {
                $('#info').find('span').text(`Arriving at ${data.name}`);
                currentStop = nextStop;
            }
        });
    }
 
    function toggleButtons(buttonA, buttonB) {
        $(buttonA).removeAttribute('disabled');
        $(buttonB).attr('disabled', 'disabled');
    }
 
    return {
        depart,
        arrive
    };
}

let result = solve();
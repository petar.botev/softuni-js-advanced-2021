function attachEvents() {

    let conditions = {
        Sunny: () => '☀',
        'Partly sunny': () => '⛅',
        Overcast: () => '☁',
        Rain: () => '☂',
    }

    let submitButton = document.getElementById('submit')
    submitButton.addEventListener('click', getCity)


    function getCity() {
        
        let currentDiv = document.getElementById('current')
        Array.from(currentDiv.querySelectorAll('span')).forEach(span => span.remove())
        let upcomingDiv = document.getElementById('upcoming');
        Array.from(upcomingDiv.querySelectorAll('span')).forEach(span => span.remove())
        let forecastDiv = document.getElementById('forecast')
        Array.from(forecastDiv.querySelectorAll('h4')).forEach(h4 => h4.remove())

        
        fetch(`http://localhost:3030/jsonstore/forecaster/locations`)
        .then(response => response.json())
        .then(data => {
            let inputCity = document.getElementById('location');
            
            document.getElementById('forecast').style.display = 'block';

                cityData = data.find(city => city.name === inputCity.value);
                console.log(cityData.code)

                fetch(`http://localhost:3030/jsonstore/forecaster/today/${cityData.code}`)
                    .then(response => response.json())
                    .then(todayForecast => {
                        console.log(todayForecast)
                        console.log(todayForecast.forecast.condition)

                        let forecastDiv = document.createElement('div');
                        forecastDiv.classList.add('forecast')

                        let conditionSymbolSpan = document.createElement('span')
                        conditionSymbolSpan.classList.add('condition', 'symbol')
                        conditionSymbolSpan.textContent = conditions[todayForecast.forecast.condition]();

                        let conditionSpan = document.createElement('span')
                        conditionSpan.classList.add('condition')

                        let nameSpan = document.createElement('span')
                        nameSpan.classList.add('forecast-data')
                        nameSpan.textContent = todayForecast.name

                        let tempSpan = document.createElement('span')
                        tempSpan.classList.add('forecast-data')
                        tempSpan.textContent = `${todayForecast.forecast.low}°/${todayForecast.forecast.high}°`

                        let weatherSpan = document.createElement('span')
                        weatherSpan.classList.add('forecast-data')
                        weatherSpan.textContent = todayForecast.forecast.condition

                        conditionSpan.appendChild(nameSpan)
                        conditionSpan.appendChild(tempSpan)
                        conditionSpan.appendChild(weatherSpan)

                        currentDiv.appendChild(conditionSymbolSpan)
                        currentDiv.appendChild(conditionSpan)
                    })

                fetch(`http://localhost:3030/jsonstore/forecaster/upcoming/${cityData.code}`)
                    .then(response => response.json())
                    .then(upcomingForecast => {
                        console.log(upcomingForecast)

                        let upcomingWeather = upcomingForecastCreateElement(upcomingForecast)

                        upcomingDiv.appendChild(upcomingWeather)

                    })
            })
            .catch(err => {
                let h4 = document.createElement('h4')
                h4.classList.add('label')
                h4.textContent = 'Error'
                forecastDiv.appendChild(h4)
                // currentDiv.remove()
                // upcomingDiv.remove()

            })
        }
        
        let forecastDiv = document.createElement('div');
        forecastDiv.classList.add('forecast-info')
    function upcomingForecastCreateElement(weather) {


        let firstDay = upcomingForecastCreate(weather.forecast[0]);
        let secondDay = upcomingForecastCreate(weather.forecast[1]);
        let thirdDay = upcomingForecastCreate(weather.forecast[2]);

        forecastDiv.appendChild(firstDay)
        forecastDiv.appendChild(secondDay)
        forecastDiv.appendChild(thirdDay)

        return forecastDiv
    }

    function upcomingForecastCreate(weather) {

        let upcomingSpan = document.createElement('span');
        upcomingSpan.classList.add('upcoming')

        let symbolSpan = document.createElement('span')
        symbolSpan.classList.add('symbol')
        symbolSpan.textContent = conditions[weather.condition]();

        let tempSpan = document.createElement('span')
        tempSpan.classList.add('forecast-data')
        tempSpan.textContent = `${weather.low}°/${weather.high}°`

        let weatherSpan = document.createElement('span')
        weatherSpan.classList.add('forecast-data')
        weatherSpan.textContent = weather.condition

        upcomingSpan.appendChild(symbolSpan)
        upcomingSpan.appendChild(tempSpan)
        upcomingSpan.appendChild(weatherSpan)

        return upcomingSpan
    }
}
attachEvents();
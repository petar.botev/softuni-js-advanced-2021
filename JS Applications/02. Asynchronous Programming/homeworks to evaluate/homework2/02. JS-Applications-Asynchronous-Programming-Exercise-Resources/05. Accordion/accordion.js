function accordion() {

    let newMainSection = document.createElement('section');
    newMainSection.id = 'main';

    fetch(`http://localhost:3030/jsonstore/advanced/articles/list`)
        .then(response => response.json())
        .then(list => {
            Object.values(list).forEach(el => {


                    fetch(`http://localhost:3030/jsonstore/advanced/articles/details/${el._id}`)
                        .then(response => response.json())
                        .then(data => {

                            let createAccordion = newAccordion(data)
                            let body = document.querySelector('body')
                            body.appendChild(createAccordion)
                        })


            })
        })
        .catch(err =>{
            console.log('Error')
        })
    function newAccordion(data) {
        // console.log(data)

        let accordingDiv = document.createElement('div');
        accordingDiv.classList.add('accordion');

        let headDiv = document.createElement('div');
        headDiv.classList.add('head');

        let spanTitle = document.createElement('span');
        spanTitle.textContent = data.title

        let button = document.createElement('button');
        button.classList.add('button')
        button.id = data._id
        button.textContent = 'More'
        button.addEventListener('click', showHide)

        let extraDiv = document.createElement('div');
        extraDiv.classList.add('extra');

        let createP = document.createElement('p');
        createP.textContent = data.content;

        extraDiv.appendChild(createP)

        headDiv.appendChild(spanTitle);
        headDiv.appendChild(button);

        accordingDiv.appendChild(headDiv)
        accordingDiv.appendChild(extraDiv)

        newMainSection.appendChild(accordingDiv);

        return newMainSection
    }

    function showHide(e){

        let shownOrHiddenPElement = e.target.parentElement.nextSibling
        // console.log(shownOrHiddenPElement)

        if(e.target.textContent === 'More'){
            shownOrHiddenPElement.style.display = 'block';
            e.target.textContent = 'Less'
        }else{
            shownOrHiddenPElement.style.display = 'none';
            e.target.textContent = 'More'
        }
    }
}
accordion()
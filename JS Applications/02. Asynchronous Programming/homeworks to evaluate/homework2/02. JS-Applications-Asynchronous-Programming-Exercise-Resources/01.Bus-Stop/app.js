function getInfo() {

    let stopIdElement = document.getElementById('stopId');

    let stopId = stopIdElement.value;
    
    fetch(`http://localhost:3030/jsonstore/bus/businfo/${stopId}`)
    .then(response => response.json())
    .then(stopInfo => {

        console.log(stopInfo.name)
            let stopName = document.getElementById('stopName')
            stopName.textContent = stopInfo.name
            
            let busesUl = document.getElementById('buses')

            Array.from(busesUl.querySelectorAll('li')).forEach(li => li.remove())
            Object.entries(stopInfo.buses).forEach(bus =>{
                let creatLi = document.createElement('li')
                
                creatLi.textContent = `Bus ${bus[0]} arrives in ${bus[1]} minutes`
                busesUl.appendChild(creatLi)
                
            })
        })
    .catch(err => {
        let stopName = document.getElementById('stopName')
        stopName.textContent = 'Error'
        let busesUl = document.getElementById('buses')
        Array.from(busesUl.querySelectorAll('li')).forEach(li => li.remove())

    })
}
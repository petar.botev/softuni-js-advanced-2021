function lockedProfile() {

    
    fetch(`http://localhost:3030/jsonstore/advanced/profiles`)
    .then(respone => respone.json())
    .then(data => {
        
        Object.values(data).forEach((el, i) => {
                let mainSection = document.getElementById('main');
                let profile = createProfile(i + 1, el.username, el.email, el.age)
                mainSection.appendChild(profile)
            });


        })
        .catch(err => {
            console.log('Error')
        })
    function createProfile(index, username, email, age) {

        let profileDiv = document.createElement('div')
        profileDiv.classList.add('profile');
        

        let profileImage = document.createElement('img');
        profileImage.src = './iconProfile2.png';
        profileImage.classList.add('userIcon');

        let labelLock = document.createElement('label');
        labelLock.textContent = 'Lock'

        let lockInput = document.createElement('input')
        lockInput.type = 'radio';
        lockInput.name = `user${index}Locked`;
        lockInput.value = 'lock';
        lockInput.checked = true;

        let labelUnlock = document.createElement('label');
        labelUnlock.textContent = 'Unlock'

        let unlockInput = document.createElement('input')
        unlockInput.type = 'radio';
        unlockInput.name = `user${index}Locked`;
        unlockInput.value = 'unlock';

        let createHr = document.createElement('hr');

        let usernameLabel = document.createElement('label');
        usernameLabel.textContent = 'Username'

        let usernameInput = document.createElement('input');
        usernameInput.id = 'username';
        usernameInput.type = 'text';
        usernameInput.name = `user${index}Username`
        usernameInput.value = username
        usernameInput.disabled = true;
        usernameInput.readOnly = true;

        let userHiddenFieldsDiv = document.createElement('div');
        userHiddenFieldsDiv.id = `user${index}HiddenFields`;
        userHiddenFieldsDiv.style.display = 'none';

        let emailLabel = document.createElement('label')
        emailLabel.textContent = 'Email:'

        let emailInput = document.createElement('input');
        emailInput.id = 'email';
        emailInput.type = 'email';
        emailInput.name = `user${index}Email`
        emailInput.value = email
        emailInput.disabled = true;
        emailInput.readOnly = true;

        let ageLabel = document.createElement('label');
        ageLabel.textContent = 'Age:'

        let ageInput = document.createElement('input');
        ageInput.id = 'age';
        ageInput.type = 'email';
        ageInput.name = `user${index}Age`
        ageInput.value = age
        ageInput.disabled = true;
        ageInput.readOnly = true;

        userHiddenFieldsDiv.appendChild(createHr)
        userHiddenFieldsDiv.appendChild(emailLabel)
        userHiddenFieldsDiv.appendChild(ageLabel)
        userHiddenFieldsDiv.appendChild(emailInput)
        userHiddenFieldsDiv.appendChild(ageInput)

        let showMoreButton = document.createElement('button');
        showMoreButton.textContent = 'Show more';
        showMoreButton.addEventListener('click', showHiddenInfo)

        profileDiv.appendChild(profileImage)
        profileDiv.appendChild(labelLock)
        profileDiv.appendChild(lockInput)
        profileDiv.appendChild(labelUnlock)
        profileDiv.appendChild(unlockInput)
        profileDiv.appendChild(createHr)
        profileDiv.appendChild(usernameLabel)
        profileDiv.appendChild(usernameInput)
        profileDiv.appendChild(userHiddenFieldsDiv)
        profileDiv.appendChild(showMoreButton)

        return profileDiv
    }
    function showHiddenInfo(e) {
        let profile = e.target.parentElement;
        let showMoreButton = e.target;
        let hiddenFieldsDiv = e.target.previousElementSibling;
        let radioButton = profile.querySelector('input[type="radio"]:checked');
        console.log(radioButton);

        if (radioButton.value !== 'unlock') {
            return;
        }
        if(showMoreButton.textContent === 'Show more'){
            hiddenFieldsDiv.style.display = 'block';
            showMoreButton.textContent = 'Hide it'
        }else{
            hiddenFieldsDiv.style.display = 'none';
            showMoreButton.textContent = 'Show more'
        }
    }

}
function attachEvents() {

    let loadPostsButton = document.getElementById('btnLoadPosts')
    loadPostsButton.addEventListener('click', loadPosts)

    let viewButton = document.getElementById('btnViewPost')
    viewButton.addEventListener('click', loadView)
}


function loadPosts() {
    fetch(`http://localhost:3030/jsonstore/blog/posts`)
        .then(response => response.json())
        .then(posts => {
            Object.values(posts).forEach(postsData => {

                let createOption = document.createElement('option')
                createOption.value = postsData.id;
                createOption.textContent = postsData.title

                selectPosts.appendChild(createOption)


            })
        })
    let selectPosts = document.getElementById('posts')
    Array.from(selectPosts.querySelectorAll('option')).forEach(el => el.remove())
}
function loadView() {

    let postsId = document.getElementById('posts').value
    // console.log(postsId)
    
    fetch(`http://localhost:3030/jsonstore/blog/posts/${postsId}`)
    .then(respone => respone.json())
    .then(data => {
        let postTitle = document.getElementById('post-title');
        let createP = document.createElement('p')
        postTitle.textContent = data.title;
        let postBody = document.getElementById('post-body');
        
        createP.textContent = data.body
        
        postBody.appendChild(createP)
        
        fetch(`http://localhost:3030/jsonstore/blog/comments`)
        .then(response => response.json())
        .then(comments => {
            Object.values(comments).forEach(el => {
                if (el.postId === postsId) {
                    let postCommentsUl = document.getElementById('post-comments')
                    
                    let createLi = document.createElement('li');
                    createLi.id = el.id
                    createLi.textContent = el.text
                    
                    postCommentsUl.appendChild(createLi)
                }
                
            })
        })
    })
    .catch(err=>{
        console.log('Error')
    })
    let postBody = document.getElementById('post-body');
    Array.from(postBody.querySelectorAll('p')).forEach(el => el.remove())

    let postCommentsUl = document.getElementById('post-comments')
    Array.from(postCommentsUl.querySelectorAll('li')).forEach(el => el.remove())

}

attachEvents();
function solve() {

    let nextStop = 'depot'
    function depart() {
        let arriveButton = document.getElementById('arrive')
        let departButton = document.getElementById('depart');

        fetch(`http://localhost:3030/jsonstore/bus/schedule/${nextStop}`)
        .then(response => response.json())
        .then(stop => {

            let busStopInfoSpan = document.querySelector('#info .info')
            
            busStopInfoSpan.setAttribute('stop-name', stop.name);
            busStopInfoSpan.setAttribute('next-stop-id',stop.next)
            busStopInfoSpan.textContent = `Next stop ${stop.name}`;

            arriveButton.disabled = false;
            departButton.disabled = true;
            
        })
        .catch(err =>{
            arriveButton.disabled = true;
            departButton.disabled = true;
            let busInfo = document.getElementById('info')
            busInfo.textContent = `Error`
        })
    }

    function arrive() {
        
        let arriveButton = document.getElementById('arrive');
        let departButton = document.getElementById('depart');
        let busStopInfoSpan = document.querySelector('#info .info')

        let stopName = busStopInfoSpan.getAttribute('stop-name')
        let nextStopId = busStopInfoSpan.getAttribute('next-stop-id')
        busStopInfoSpan.textContent = `Arriving at ${stopName}`

        nextStop = nextStopId
        arriveButton.disabled = true;
        departButton.disabled = false;
    }

    return {
        depart,
        arrive
    };
}

let result = solve();
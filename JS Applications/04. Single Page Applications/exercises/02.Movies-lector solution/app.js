import addMoviePage from "./pages/addMovie.js";
import editMoviePage from "./pages/editMovie.js";
import homePage from "./pages/homePage.js";
import loginPage from "./pages/login.js";
import movieDetailPage from "./pages/movieDetails.js";
import registerPage from "./pages/register.js";
import viewChanger from "./viewChanger.js";
import viewFinder from "./viewFinder.js";

async function setup() {
    
    //let appElement = document.querySelector('#main');
    let linkSelector = '.link';

    loginPage.initialize(document.getElementById('form-login'));
    registerPage.initialize(document.getElementById('form-sign-up'));
    homePage.initialize(document.getElementById('home-page'));
    addMoviePage.initialize(document.getElementById('add-movie'));
    movieDetailPage.initialize(document.getElementById('movie-details'));
    editMoviePage.initialize(document.getElementById('edit-movie'));
    
    nav.initialize(document.querySelector('nav'));

    let appElement = document.getElementById('main');
   

    viewChanger.initialize(appElement, '.view');
    viewFinder.initialize(document.querySelectorAll(linkSelector), linkSelector, viewChanger.changeView);

   viewFinder.navigateTo('home');
}

setup();    
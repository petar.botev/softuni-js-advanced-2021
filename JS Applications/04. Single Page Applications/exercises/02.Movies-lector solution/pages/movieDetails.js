import viewFinder from "../viewFinder.js";
import auth from "./helpers/authService.js";
import { jsonRequest } from "./helpers/httpService.js";
import likesService from "./services/likesService.js";
import movieService from "./services/movieService.js";

let section = undefined;
let navLinkClass = undefined;

function initialize(domElement, linkClass){
    section = domElement;
    navLinkClass = linkClass;

}

async function getView(id){
    let movieDetail = await movieService.getMovie(id);
    let movieContainer = section.querySelector('#movie-details-container');
    [...movieContainer.children].forEach(x => x.remove());

    let userId = auth.getUserId();
    let userLikesArr = await likesService.getUserLikes(id);

    let movieLikes = await likesService.getMovieLikes(id);

    let movieDetails = createMovieDetails(movieDetail, userLikesArr.length > 0, movieLikes);
    movieContainer.innerHTML = movieDetails;
    movieContainer.querySelectorAll('.link')
       .forEach(l => l.addEventListener('click', viewFinder.changeViewHandler));
    return section;
}

async function like(movieId){
    //let movieDetail = await jsonRequest(`http://localhost:3030/data/movies/${movieId}`);
    let body = {movieId: movieId};
    let result = await likesService.likeMovie(body);
    return movieDetailsPage.getView(movieId);
}

async function deleteMovie(movieId){
    try {
        let deleteResult = await movieService.deleteMovie(movieId);
        return viewFinder.navigateTo('home');
    } catch (err) {
        console.error(err);
        alert(err);
    }
}

function createMovieDetails(movie, hasLiked, likes){

    let editButton = `<a class="btn btn-warning link" data-route="edit" data-id="${movie._id}" href="#">Edit</a>`;
    let deleteButton = `<a class="btn btn-danger link" data-route="delete" data-id="${movie._id}" href="#">Delete</a>`;
    let likeButton = `<a class="btn btn-primary link" data-route="like" data-id="${movie._id}" href="#">Like</a>`;

    let buttons = [];
    let isOwner = auth.getUserId() === movie._ownerId;
    if(isOwner){
        buttons.push(deleteButton, editButton);
    }
    if(!hasLiked && !isOwner){
        buttons.push(likeButton);
    }

    let buttonsSection = buttons.join('\n');

    let elem = `<div class="row bg-light text-dark">
    <h1>Movie title: ${movie.title}</h1>

    <div class="col-md-8">
        <img class="img-thumbnail" src="${movie.img}"
             alt="Movie">
    </div>
    <div class="col-md-4 text-center">
        <h3 class="my-3 ">Movie Description</h3>
        <p>${movie.description}</p>
        ${buttonsSection}
        
        <span class="enrolled-span">Liked: ${likes}</span>
    </div>
</div>`;

return elem;
}

let movieDetailsPage = {
    initialize,
    getView,
    like,
    deleteMovie
}

export default movieDetailsPage;
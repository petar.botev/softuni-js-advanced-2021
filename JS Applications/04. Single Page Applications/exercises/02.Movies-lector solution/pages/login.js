import auth from "./helpers/authService.js";

let section = undefined;

function initialize(domElement) {
  section = domElement;
  let form = section.querySelector('#login-form');
  form.addEventListener('submit', loginUser);
};

async function getView() {
  return section;
}

async function loginUser(e){
  e.preventDefault();
  let form = e.target;
  let formData = new FormData(form);

  let user = {
    email: formData.get('email'),
    password: formData.get('password')
  }
  
  form.reset();
  await auth.login(user);
}

let loginPage = {
  initialize,
  getView,
  
};

export default loginPage;

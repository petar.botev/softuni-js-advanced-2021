import viewFinder from "../viewFinder.js";
import auth from "./helpers/authService.js";
import { jsonRequest } from "./helpers/httpService.js";

let section = undefined;

function initialize(domElement){
    section = domElement;

    let form = section.querySelector('#register-form');
    form.addEventListener('submit', registerUser);
}

async function registerUser(e){
    e.preventDefault();
    let form = e.target;
    let formData = new FormData(form);
  
    let email = formData.get('email');
    let password = formData.get('password');
    let repeatPassword = formData.get('repeatPassword');

    if(email === '' || 
    password === '' ||
    password.length < 6 ||
    password !== repeatPassword){
        alert('Fields mus not be empty and password must be match.');
    }

    let user = {
      email: formData.get('email'),
      password: formData.get('password')
    }
  
  
    form.reset();
    auth.register(user);
  }

async function getView(){
    return section;
}

let registerPage = {
  initialize,
    getView
}

export default registerPage;
import viewFinder from "../viewFinder.js";
import movieService from "./services/movieService.js";

let section = undefined;

function initialize(domElement){
    section = domElement;
    let form = section.querySelector('form');
    form.addEventListener('submit', addMovie);
}

async function getView(){
    return section;
}

async function addMovie(e) {
    e.preventDefault();
    try {
        let form = e.target;
        let formData = new FormData(form);
        let newMovie = {
            title:formData.get('title'),
            description: formData.get('description'),
            img: formData.get('imageUrl')
        }

        await movieService.createMovie(newMovie);
        form.reset();
        viewFinder.navigateTo('home');
    } catch (err) {
        console.error(err);
        alert(err);
    }
}

let addMoviePage = {
    initialize,
    getView,
    addMovie
}

export default addMoviePage;
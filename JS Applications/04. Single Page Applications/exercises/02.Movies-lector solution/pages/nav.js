import auth from "./helpers/authService";

let section = undefined;

function initialize(domElement){
    section = domElement;
    if(auth.isLoggedIn()) {
        this.loginUser();
    } else {
        this.logoutUser();
    }

}

function logoutUser() {
    let userWelcome = section.querySelector('#user-welcome');
    userWelcome.textContext = 'Welcome, guest';
    let userLinks = [...section.querySelectorAll('.user')];
    userLinks.forEach(el => el.classList.add('hidden'));
    let guestLinks = [...section.querySelectorAll('.guest')];
    guestLinks.forEach(el => el.classList.remove('hidden'));
}

function loginUser() {
    let userWelcome = section.querySelector('#user-welcome');
    userWelcome.textContext = `Welcome, ${auth.getUsername()}`;
    let userLinks = [...section.querySelectorAll('.user')];
    userLinks.forEach(el => el.classList.add('hidden'));
    let guestLinks = [...section.querySelectorAll('.guest')];
    guestLinks.forEach(el => el.classList.remove('hidden'));
}

async function getView(){
    return section;
}

let navPage = {
    initialize,
    getView,
    logoutUser,
    loginUser
}

export default navPage;
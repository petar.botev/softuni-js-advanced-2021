import viewFinder from "../../viewFinder.js";
import { jsonRequest } from "./httpService.js";

export function setAuthToken(token){
    localStorage.setItem('authToken', token);
}

export function getAuthToken(token){
    return localStorage.getItem('authToken');
}

function getUserId(){
    return localStorage.getItem('userId');
}

function setUserId(userId){
    return localStorage.setItem('userId', userId);
}

function getUsername(){
    return localStorage.getItem('username');
}

function setUsername(userId){
    return localStorage.setItem('username', userId);
}

export function isLoggedIn() {
    return localStorage.getItem('authToken') !== null && localStorage.getItem('authToken') !== undefined;
}

async function login(user){
    let url = 'http://localhost:3030/users/login';
  let result = await jsonRequest(url, 'Post', user);
  setAuthToken(result.accessToken);
  setUserId(result._id);
  setUsername(result.email);
  nav.loginUser();
  viewFinder.navigateTo('home');
}

async function register(user){
    let url = 'http://localhost:3030/users/register';
    let result = await jsonRequest(url, 'Post', user);
    setAuthToken(result.accessToken);
    setUserId(result._id);
    setUsername(result.email);
    nav.loginUser();
    viewFinder.navigateTo('home');
}

async function logout(){
    let result = await jsonRequest(
        'http://localhost:3030/users/logout', 'Get', undefined, true, true);
    localStorage.clear();
    nav.logoutUser();
    return viewFinder.redirectTo('login');
}

let auth = {
    setAuthToken,
    getAuthToken,
    isLoggedIn,
    logout,
    getUserId,
    setUserId,
    getUsername,
    setUsername,
    login,
    register
};

export default auth;

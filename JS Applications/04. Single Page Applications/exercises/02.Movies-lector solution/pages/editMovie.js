import movieService from "./services/movieService.js";

let section = undefined;

function initialize(domElement){
    section = domElement;
    let form = section.querySelector('form');
    form.addEventListener('submit', editMovie);

}

async function editMovie(e){
    e.preventDefault();
    try {
        let form = e.target;
        let formData = new FormData(form);
        let id = form.formData.id;

        let editedMovie = {
            title:formData.get('title'),
            description: formData.get('description'),
            img: formData.get('imageUrl')
        }

        let updateResult = await movieService.editMovie(editedMovie, id);
        //await movieService.getMovie(id);
        form.reset();
        viewFinder.navigateTo('movie-details', id);
    } catch (err) {
        console.error(err);
        alert(err);
    }

}
async function getView(){
    try {

        let movie = await movieService.getMovie(id);
        let titleInput = section.querySelector('input[name="title"]');
        let descriptionTextarea = section.querySelector('input[name="descriptionTextarea"]');
        let imageInput = section.querySelector('input[name="imageUrl"]');

        let form = section.querySelector('form');
        form.dataset.id = id;

        titleInput.value = movie.title;
        descriptionTextarea.value = movie.descriptionTextarea;
        imageInput.value = movie.img;
        return section;
    } catch (error) {

    }
    return section;

}

let editMoviePage = {
    initialize,
    getView
}

export default editMoviePage;
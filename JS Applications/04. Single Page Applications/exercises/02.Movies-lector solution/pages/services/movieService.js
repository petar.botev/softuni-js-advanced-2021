import { jsonRequest } from "../helpers/httpService.js";

let baseUrl = 'http://localhost:3030';

async function createMovie(newMovie){
    let createResult = await jsonRequest(
        `${baseUrl}/data/movies`, 'Post', newMovie, true);
    return createResult;
}
async function editMovie(editedMovi, id){
    let editedResult = await jsonRequest(
        `${baseUrl}/data/movies`, 'Put', editedMovie, true);
    return editedResult;
}

async function getMovie(movieId){
    let movie = await jsonRequest(
        `${baseUrl}/data/movies/${movieId}`);
    return editedResult;
}

async function getAllMovies(movieId){
    let url = `${baseUrl}/data/movies`;
    let movies = await jsonRequest(url);
    return movies;
}

async function deleteMovie(movieId){
    let deleteResult = await jsonRequest(
        `${baseUrl}/data/movies/${movieId}`, 'Delete', undefined, true);
        return deleteResult;
}

let movieService = {
    createMovie,
    editMovie,
    getMovie,
    getAllMovies,
    deleteMovie
}

export default movieService;
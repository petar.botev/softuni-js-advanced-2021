import addMoviePage from "./pages/addMovie.js";
import editMoviePage from "./pages/editMovie.js";
import auth from "./pages/helpers/authService.js";
import homePage from "./pages/homePage.js";
import loginPage from "./pages/login.js";
import movieDetailsPage from "./pages/movieDetails.js";
import registerPage from "./pages/register.js";

let views = {
    'home': async() => await homePage.getView(),
    'login': async() => await loginPage.getView(),
    'register': async() => await registerPage.getView(),
    'logout': async() => await auth.logout(),
    'movie-details': async(id) => await movieDetailsPage.getView(id),
    'like': async(id) => await movieDetailsPage.like(id),
    'add-movie': async() => await addMoviePage.getView(),
    'edit-movie': async() => await editMoviePage.getView(id),
    'delete': async() => await movieDetailsPage.deleteMovie(id)
};

let navLinkSelector = undefined;
let navigationCallback = undefined;

function initialize(allLinkElements, linkSelector, callback){

    allLinkElements.forEach(a => a.addEventListener('click', changeViewHandler));
    navLinkSelector = linkSelector;
    navigationCallback = callback;
}

async function changeViewHandler(e){
    let element = e.target.matches(navLinkSelector) 
        ? e.target 
        : e.target.closest(navLinkSelector);
      
    let route = element.dataset.route;
    let id = element.dataset.id;
    navigateTo(route, id);
}

async function navigateTo(route, id){
    if(views.hasOwnProperty(route)){
        let view = await views[route](id);
        navigationCallback(view);
    }
}

async function redirectTo(route){
    if(views.hasOwnProperty(route)){
        let view = views[route]();
        return view;
    }
}

let viewFinder = {
    initialize,
    navigateTo,
    changeViewHandler,
    redirectTo
};

export default viewFinder;
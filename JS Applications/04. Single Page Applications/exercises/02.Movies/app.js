import addMoviePage from "./pages/addMovie.js";
import editMoviePage from "./pages/editMovie.js";
import homePage from "./pages/homePage.js";
import loginPage from "./pages/login.js";
import { logout } from "./pages/logout.js";
import movieDetailPage from "./pages/movieDetails.js";
import registerPage from "./pages/register.js";

let appElement = document.querySelector('#container');

async function setup() {
    
    console.log(document.querySelector('.container'));
    appElement.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));

    //document.querySelector('#home-page').classList.remove('hidden');
    
    loginPage.setupSection(appElement.querySelector('#form-login'));
    registerPage.setupSection(appElement.querySelector('#form-sign-up'));
    homePage.setupSection(appElement.querySelector('#home-page'));
    addMoviePage.setupSection(appElement.querySelector('#add-movie'));
    movieDetailPage.setupSection(appElement.querySelector('#movie-example'));
    editMoviePage.setupSection(appElement.querySelector('#edit-movie'));

    if(localStorage.token == null){

        document.getElementById('welcome-email').style.display = 'none';
        document.getElementById('logout-nav').style.display = 'none';
        document.getElementById('register-nav').style.display = 'block';
        document.getElementById('login-nav').style.display = 'block';
        
        let homePageView = await homePage.getView();
        homePageView.classList.remove('hidden');
    }
    else {
        let welcomeEmailElement = document.getElementById('welcome-email');
        welcomeEmailElement.style.display = 'block';
        welcomeEmailElement.textContent = `Welcome, ${localStorage.email}`;

        document.getElementById('logout-nav').style.display = 'block';
        document.getElementById('register-nav').style.display = 'none';
        document.getElementById('login-nav').style.display = 'none';
        
        let homePageView = await homePage.getView();
        homePageView.classList.remove('hidden');
    }

}

document.querySelector('#movie-home-button').addEventListener('click', async (e) => {
    appElement.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));
    let homePageView = await homePage.getView();
    homePageView.classList.remove('hidden');
});

document.querySelector('#login-nav').addEventListener('click', async () => {
    appElement.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));
    let loginView = await loginPage.getView();
    loginView.classList.remove('hidden');
});

document.querySelector('#register-nav').addEventListener('click', async () => {
    appElement.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));
    let registerView = await registerPage.getView();
    registerView.classList.remove('hidden');
});

document.querySelector('#logout-nav').addEventListener('click', logout);

setup();    
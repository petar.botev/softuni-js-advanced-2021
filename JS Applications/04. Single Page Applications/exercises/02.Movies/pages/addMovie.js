let section = undefined;

function setupSection(domElement){
    section = domElement;

}

async function getView(){
    return section;
}

let addMoviePage = {
    setupSection,
    getView
}

export default addMoviePage;
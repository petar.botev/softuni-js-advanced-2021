let section = undefined;

function setupSection(domElement){
    section = domElement;

}

async function getView(){
    return section;
}

let movieDetailPage = {
    setupSection,
    getView
}

export default movieDetailPage;
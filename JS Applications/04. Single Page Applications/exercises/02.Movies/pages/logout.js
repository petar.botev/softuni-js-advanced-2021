import homePage from "./homePage.js";

export async function logout(e){
    e.preventDefault();

    let logoutUrl = 'http://localhost:3030/users/logout';

    fetch(logoutUrl, {
        method: "GET",
        headers: {
            "X-Authorization": localStorage.token
        }
    });

    localStorage.clear();

    document.getElementById('register-nav').style.display = 'inline-block';
    document.getElementById('login-nav').style.display = 'inline-block';

    let welcomeEmailElement = document.getElementById('welcome-email');
    welcomeEmailElement.style.display = 'none';
    //welcomeEmailElement.textContent = `Welcome, ${localStorage.email}`;
    
    document.getElementById('logout-nav').style.display = 'none';

    //document.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));
    let homePageView = await homePage.getView();
    homePageView.classList.remove('hidden');
}
import homePage from "./homePage.js";

let section = undefined;

function setupSection(domElement) {
  section = domElement;

  let form = section.querySelector('form');
  form.addEventListener('submit', loginQuery);
}

async function loginQuery(e) {
    e.preventDefault();
    let url = 'http://localhost:3030/users/login';
    console.log(e.target);
    
    let formData = new FormData(e.target);
    let email = formData.get('email');
    let password = formData.get('password');

    let obj = {
        email: email,
        password: password
    }
    
    let loginPostPromis = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj)
    });

    let loginResponce = await loginPostPromis.json();
    localStorage.setItem('email', loginResponce.email);
    localStorage.setItem('token', loginResponce.accessToken);

    document.getElementById('register-nav').style.display = 'none';
    document.getElementById('login-nav').style.display = 'none';

    let welcomeEmailElement = document.getElementById('welcome-email');
    welcomeEmailElement.style.display = 'block';
    welcomeEmailElement.textContent = `Welcome, ${localStorage.email}`;
    
    document.getElementById('logout-nav').style.display = 'block';

    document.querySelectorAll('#container > section').forEach(x => x.setAttribute('class', 'hidden'));
    let homePageView = await homePage.getView();
    homePageView.classList.remove('hidden');
}

async function getView() {
  return section;
}

let loginPage = {
  setupSection,
  getView,
  
};

export default loginPage;

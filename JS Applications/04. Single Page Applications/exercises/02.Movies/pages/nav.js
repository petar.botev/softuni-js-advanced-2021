let section = undefined;

function setupSection(domElement){
    section = domElement;

}

async function getView(){
    return section;
}

let navPage = {
    setupSection,
    getView
}

export default navPage;